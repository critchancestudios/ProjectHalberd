#ifndef _HDFS_H_
#define _HDFS_H_

#include "disc.h"

#define HDFS_OK         0
#define HDFS_NODISC     1
#define HDFS_INVALID    2
#define HDFS_BADHDR     3
#define HDFS_BADCRC     4
#define HDFS_NOTFOUND   5
#define HDFS_NOTDIR     6
#define HDFS_NOTFILE    7

typedef struct {
    char publisher[32];
    char gamename[32];
} DiscInfo;

int hdfs_init();
int hdfs_getinfo(DiscInfo* info);
int hdfs_findfile(const char* path, int* dataStart, int* dataLength);

#endif