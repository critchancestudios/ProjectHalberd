#ifndef _MEMC_H_
#define _MEMC_H_

#define MC_SECTOR_SIZE  512
#define MC_MAX_SECTORS  4096
#define NUM_MEMCARDS    2

extern int memc_present(int mc);
extern void memc_read(int mc, int lba, int len, void* buffer);
extern void memc_write(int mc, int lba, int len, const void* buffer);

#endif