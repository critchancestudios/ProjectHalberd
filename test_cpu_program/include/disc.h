#ifndef _DISC_H_
#define _DISC_H_

#define DVD_SECTOR_SIZE 2048

extern int disc_present();
extern int disc_opentray();
extern int disc_closetray();
extern void disc_read(int lba, int len, void* buffer);

#endif