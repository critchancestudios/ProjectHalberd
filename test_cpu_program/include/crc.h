#ifndef _CRC_H_
#define _CRC_H_

unsigned int crc32(unsigned int crc, const unsigned char* buf, int len);

#endif