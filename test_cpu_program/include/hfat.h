#ifndef _HFAT_H_
#define _HFAT_H_

#include <stdint.h>

#include "memc.h"

#define MC_CHECK_OK         0
#define MC_CHECK_NOFS       1
#define MC_CHECK_CORRUPT    2
#define MC_INVALID         -1

#define MC_META_SIZE        1600    // char[32] gamename, char[32] desc, u16[256] iconPalette, u8[32 * 32] icon

typedef struct {
    char filename[20];
    uint32_t timestamp;
    uint16_t dataStart;
    uint16_t dataLength;
    uint32_t metaOffset;
} HFAT_DtEntry;

int hfat_valid(int mc);
int hfat_check(int mc);
int hfat_format(int mc);

int hfat_find_file(int mc, const char* filename);
int hfat_delete_file(int mc, int dirEnt);
int hfat_create_file(int mc, const char* filename, uint32_t len, uint32_t metaOffset);

int hfat_getinfo(int mc, int dirEnt, HFAT_DtEntry* info);
int hfat_next_sector(int mc, int cur_sector);

#endif