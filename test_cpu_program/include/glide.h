#ifndef __GLIDE_H__
#define __GLIDE_H__

#define GLIDE_NUM_TMU   2
#define GR_FOG_TABLE_SIZE 64

#define FXFALSE         0
#define FXTRUE          1

#include "gpu.h"

typedef unsigned char   FxU8;
typedef unsigned short  FxU16;
typedef unsigned long   FxU32;
typedef char            FxI8;
typedef short           FxI16;
typedef long            FxI32;
typedef int             FxBool;

typedef FxI32 GrChipID_t;
#define GR_FBI          0x0
#define GR_TMU0         0x1
#define GR_TMU1         0x2

typedef FxI32 GrCombineFunction_t;
#define GR_COMBINE_FUNCTION_ZERO                                      GPU_COMBINE_FUNC_ZERO
#define GR_COMBINE_FUNCTION_NONE                                      GPU_COMBINE_FUNC_ZERO
#define GR_COMBINE_FUNCTION_LOCAL                                     GPU_COMBINE_FUNC_LOCAL
#define GR_COMBINE_FUNCTION_LOCAL_ALPHA                               GPU_COMBINE_FUNC_LOCAL_ALPHA
#define GR_COMBINE_FUNCTION_SCALE_OTHER                               GPU_COMBINE_FUNC_SCALE_OTHER
#define GR_COMBINE_FUNCTION_BLEND_OTHER                               GPU_COMBINE_FUNC_BLEND_OTHER
#define GR_COMBINE_FUNCTION_SCALE_OTHER_ADD_LOCAL                     GPU_COMBINE_FUNC_SCALE_OTHER_ADD_LOCAL
#define GR_COMBINE_FUNCTION_SCALE_OTHER_ADD_LOCAL_ALPHA               GPU_COMBINE_FUNC_SCALE_OTHER_ADD_LOCAL_ALPHA
#define GR_COMBINE_FUNCTION_SCALE_OTHER_MINUS_LOCAL                   GPU_COMBINE_FUNC_SCALE_OTHER_MINUS_LOCAL
#define GR_COMBINE_FUNCTION_SCALE_OTHER_MINUS_LOCAL_ADD_LOCAL         GPU_COMBINE_FUNC_SCALE_OTHER_MINUS_LOCAL_ADD_LOCAL
#define GR_COMBINE_FUNCTION_BLEND                                     GPU_COMBINE_FUNC_BLEND
#define GR_COMBINE_FUNCTION_SCALE_OTHER_MINUS_LOCAL_ADD_LOCAL_ALPHA   GPU_COMBINE_FUNC_SCALE_OTHER_MINUS_LOCAL_ADD_LOCAL_ALPHA
#define GR_COMBINE_FUNCTION_SCALE_MINUS_LOCAL_ADD_LOCAL               GPU_COMBINE_FUNC_SCALE_MINUS_LOCAL_ADD_LOCAL
#define GR_COMBINE_FUNCTION_BLEND_LOCAL                               GPU_COMBINE_FUNC_BLEND_LOCAL
#define GR_COMBINE_FUNCTION_SCALE_MINUS_LOCAL_ADD_LOCAL_ALPHA         GPU_COMBINE_FUNC_SCALE_MINUS_LOCAL_ADD_LOCAL_ALPHA

typedef FxI32 GrCombineFactor_t;
#define GR_COMBINE_FACTOR_ZERO                                        GPU_COMBINE_FACTOR_ZERO
#define GR_COMBINE_FACTOR_NONE                                        GPU_COMBINE_FACTOR_ZERO
#define GR_COMBINE_FACTOR_LOCAL                                       GPU_COMBINE_FACTOR_LOCAL
#define GR_COMBINE_FACTOR_OTHER_ALPHA                                 GPU_COMBINE_FACTOR_OTHER_ALPHA
#define GR_COMBINE_FACTOR_LOCAL_ALPHA                                 GPU_COMBINE_FACTOR_LOCAL_ALPHA
#define GR_COMBINE_FACTOR_ONE                                         GPU_COMBINE_FACTOR_ONE
#define GR_COMBINE_FACTOR_ONE_MINUS_LOCAL                             GPU_COMBINE_FACTOR_ONE_MINUS_LOCAL
#define GR_COMBINE_FACTOR_ONE_MINUS_OTHER_ALPHA                       GPU_COMBINE_FACTOR_ONE_MINUS_OTHER_ALPHA
#define GR_COMBINE_FACTOR_ONE_MINUS_LOCAL_ALPHA                       GPU_COMBINE_FACTOR_ONE_MINUS_LOCAL_ALPHA

typedef FxI32 GrAlphaBlendFnc_t;
#define GR_BLEND_ZERO                                                 GPU_BLEND_ZERO
#define GR_BLEND_SRC_ALPHA                                            GPU_BLEND_ARG0_A
#define GR_BLEND_SRC_COLOR                                            GPU_BLEND_ARG0_RGB
#define GR_BLEND_DST_COLOR                                            GPU_BLEND_ARG1_RGB
#define GR_BLEND_DST_ALPHA                                            GPU_BLEND_ARG1_A
#define GR_BLEND_ONE                                                  GPU_BLEND_ONE
#define GR_BLEND_ONE_MINUS_SRC_ALPHA                                  GPU_BLEND_ARG0_INV_A
#define GR_BLEND_ONE_MINUS_SRC_COLOR                                  GPU_BLEND_ARG0_INV_RGB
#define GR_BLEND_ONE_MINUS_DST_COLOR                                  GPU_BLEND_ARG1_INV_RGB 
#define GR_BLEND_ONE_MINUS_DST_ALPHA                                  GPU_BLEND_ARG1_INV_A

typedef FxI32 GrCmpFnc_t;
#define GR_CMP_NEVER                                                  GPU_COMPARE_NEVER
#define GR_CMP_LESS                                                   GPU_COMPARE_LESS
#define GR_CMP_EQUAL                                                  GPU_COMPARE_EQUAL
#define GR_CMP_LEQUAL                                                 GPU_COMPARE_LEQUAL
#define GR_CMP_GREATER                                                GPU_COMPARE_GREATER
#define GR_CMP_NOTEQUAL                                               GPU_COMPARE_NOTEQUAL
#define GR_CMP_GEQUAL                                                 GPU_COMPARE_GEQUAL
#define GR_CMP_ALWAYS                                                 GPU_COMPARE_ALWAYS

typedef FxI32 GrCullMode_t;
#define GR_CULL_DISABLE                 0x0
#define GR_CULL_NEGATIVE                0x1
#define GR_CULL_POSITIVE                0x2

typedef FxI32 GrDitherMode_t;
#define GR_DITHER_DISABLE               0x0
#define GR_DITHER_2x2                   0x1

typedef FxI32 GrFogMode_t;
#define GR_FOG_DISABLE                  0x0
#define GR_FOG_ENABLE                   0x1

typedef FxI32 GrLOD_t;
#define GR_LOD_256                      0x0
#define GR_LOD_128                      0x1
#define GR_LOD_64                       0x2
#define GR_LOD_32                       0x3
#define GR_LOD_16                       0x4
#define GR_LOD_8                        0x5
#define GR_LOD_4                        0x6
#define GR_LOD_2                        0x7
#define GR_LOD_1                        0x8

typedef FxI32 GrTextureClampMode_t;
#define GR_TEXTURECLAMP_WRAP            0x0
#define GR_TEXTURECLAMP_CLAMP           0x1

typedef FxI32 GrTextureFilterMode_t;
#define GR_TEXTUREFILTER_POINT_SAMPLED  0x0
#define GR_TEXTUREFILTER_BILINEAR       0x1

typedef FxI32 GrTextureFormat_t;
#define GR_TEXFMT_RGB_565               0x0
#define GR_TEXFMT_ARGB_4444             0x1
#define GR_TEXFMT_YIQ_422               0x2
#define GR_TEXFMT_AYIQ_8422             0x3
#define GR_TEXFMT_P_8                   0x4 /* 8-bit palette */
#define GR_TEXFMT_AP_88                 0x5 /* 8-bit alpha 8-bit palette */
#define GR_TEXFMT_INTENSITY_8           0x6 /* (0..0xFF) intensity */
#define GR_TEXFMT_ALPHA_INTENSITY_88    0x7

typedef FxU32 GrTexTable_t;
#define GR_TEXTABLE_NCC0                0x0
#define GR_TEXTABLE_NCC1                0x1
#define GR_TEXTABLE_PALETTE             0x2

typedef FxU32 GrNCCTable_t;
#define GR_NCCTABLE_NCC0                0x0
#define GR_NCCTABLE_NCC1                0x1

typedef FxU32 GrColor_t;
typedef FxU8  GrAlpha_t;
typedef FxU32 GrMipMapId_t;
typedef FxU8  GrFog_t;

typedef GpuVertex_t GrVertex;

typedef struct {
    GrLOD_t           smallLod;
    GrLOD_t           largeLod;
    GrTextureFormat_t format;
    void              *data;
} GrTexInfo;

void grGlideInit(FxI32 screenMode);
void grGlideShutdown( void );
void grBufferSwap();
void grBufferClear(FxU32 color, float depth);
FxU32 grSstScreenWidth( void );
FxU32 grSstScreenHeight( void );
void grDrawPolygon( int nverts, const int ilist[], const GrVertex vlist[] );
void grDrawPolygonVertexList( int nverts, const GrVertex vlist[] );
void grDrawPoint( const GrVertex *pt );
void grDrawLine( const GrVertex *v1, const GrVertex *v2 );
void grDrawTriangle( const GrVertex *a, const GrVertex *b, const GrVertex *c );
void grAlphaBlendFunction( GrAlphaBlendFnc_t sf,   GrAlphaBlendFnc_t df );
void grAlphaCombine(
            GrCombineFunction_t function, GrCombineFactor_t factor,
            FxBool invert
            );
void grAlphaTestFunction( GrCmpFnc_t function );
void grAlphaTestReferenceValue( GrAlpha_t value );
void grClipWindow( FxU32 minx, FxU32 miny, FxU32 maxx, FxU32 maxy );
void grColorCombine(
            GrCombineFunction_t function, GrCombineFactor_t factor,
            FxBool invert );
void grColorMask( FxBool rgb );
void grCullMode( GrCullMode_t mode );
void grConstantColorValue( GrColor_t value );
void grConstantColorValue4( float a, float r, float g, float b );
void grDepthBiasLevel( float level );
void grDepthBufferFunction( GrCmpFnc_t function );
void grDepthMask( FxBool mask );
void grDisableAllEffects( void );
void grDitherMode( GrDitherMode_t mode );
void grFogColorValue( GrColor_t fogcolor );
void grFogMode( GrFogMode_t mode );
void grFogTable( const GrFog_t ft[] );
void grGammaCorrectionValue( float value );
FxU32 grTexCalcMemRequired(
            GrLOD_t lodmin, GrLOD_t lodmax,
            GrTextureFormat_t fmt);
FxU32 grTexTextureMemRequired(
            GrTexInfo *info   );
FxU32 grTexMinAddress( void );
FxU32 grTexMaxAddress( void );
void grTexNCCTable( GrChipID_t tmu, GrNCCTable_t table );
void grTexSource(
            GrChipID_t tmu, 
            FxU32      startAddress,
            GrTexInfo  *info );
void grTexClampMode(
            GrChipID_t tmu, 
            GrTextureClampMode_t s_clampmode,
            GrTextureClampMode_t t_clampmode
            );
void grTexCombine(
            GrChipID_t tmu, 
            GrCombineFunction_t rgb_function,
            GrCombineFactor_t rgb_factor, 
            GrCombineFunction_t alpha_function,
            GrCombineFactor_t alpha_factor,
            FxBool rgb_invert,
            FxBool alpha_invert
            );
void grTexFilterMode(
            GrChipID_t tmu, 
            GrTextureFilterMode_t minfilter_mode,
            GrTextureFilterMode_t magfilter_mode
            );
void grTexLodBiasValue( GrChipID_t tmu, float bias );
void grTexDownloadMipMap(
            GrChipID_t tmu, 
            FxU32      startAddress,
            GrTexInfo  *info );
void grTexDownloadMipMapLevel(
            GrChipID_t        tmu, 
            FxU32             startAddress,
            GrLOD_t           thisLod,
            GrLOD_t           largeLod,
            GrTextureFormat_t format,
            void              *data );
void grTexDownloadMipMapLevelPartial(
            GrChipID_t        tmu, 
            FxU32             startAddress,
            GrLOD_t           thisLod,
            GrLOD_t           largeLod,
            GrTextureFormat_t format,
            void              *data,
            int               start,
            int               end );
void grTexDownloadTable(
            GrChipID_t        tmu, 
            GrTexTable_t      type, 
            void              *data );
void grTexDownloadTablePartial(
            GrChipID_t   tmu, 
            GrTexTable_t type, 
            void         *data,
            int          start,
            int          end );

#endif