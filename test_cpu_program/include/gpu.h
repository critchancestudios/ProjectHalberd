#ifndef _GPU_H_
#define _GPU_H_

#define GPU_CMD_NOP             0
#define GPU_CMD_SWAPBUF         1
#define GPU_CMD_FILLRECT        2
#define GPU_CMD_DRAWPT          3
#define GPU_CMD_DRAWLINE        4
#define GPU_CMD_DRAWTRI         5

#define GPU_CHIP_FB             0
#define GPU_CHIP_TMU0           1
#define GPU_CHIP_TMU1           2

#define GPU_REG_FB_DM           0
#define GPU_REG_FB_CLIPX1X2     1
#define GPU_REG_FB_CLIPY1Y2     2
#define GPU_REG_FB_PXCONFIG     3
#define GPU_REG_FB_MODE         4
#define GPU_REG_FB_DEPTHBIAS    5
#define GPU_REG_FB_FOGTABLE     6
#define GPU_REG_FB_CLEARCOL     22
#define GPU_REG_FB_CLEARDEPTH   23
#define GPU_REG_FB_CONSTCOL     24
#define GPU_REG_FB_FOGCOL       25
#define GPU_REG_FB_GAMMA        26

#define GPU_REG_TMU_TEXADDR     0
#define GPU_REG_TMU_TLOD        1
#define GPU_REG_TMU_TMODE       2
#define GPU_REG_TMU_YIQTABLE    3
#define GPU_REG_TMU_PALTABLE    35

#define GPU_DM_320x240_VGA      0
#define GPU_DM_320x240_NTSC     1
#define GPU_DM_320x288_PAL      2
#define GPU_DM_640x480_VGA      3
#define GPU_DM_640x480_NTSC     4
#define GPU_DM_640x576_PAL      5

#define GPU_DM_DITHER           (1 << 3)

#define GPU_TEXFMT_RGB565       0
#define GPU_TEXFMT_ARGB4444     1
#define GPU_TEXFMT_YIQ422       2
#define GPU_TEXFMT_AYIQ8422     3
#define GPU_TEXFMT_P8           4
#define GPU_TEXFMT_AP88         5
#define GPU_TEXFMT_I8           6
#define GPU_TEXFMT_AI88         7

#define GPU_LOD_256         0
#define GPU_LOD_128         1
#define GPU_LOD_64          2
#define GPU_LOD_32          3
#define GPU_LOD_16          4
#define GPU_LOD_8           5
#define GPU_LOD_4           6
#define GPU_LOD_2           7
#define GPU_LOD_1           8

#define GPU_COMBINE_FUNC_ZERO                                       0
#define GPU_COMBINE_FUNC_LOCAL                                      1
#define GPU_COMBINE_FUNC_LOCAL_ALPHA                                2
#define GPU_COMBINE_FUNC_SCALE_OTHER                                3
#define GPU_COMBINE_FUNC_BLEND_OTHER                                COMBINE_FUNC_SCALE_OTHER
#define GPU_COMBINE_FUNC_SCALE_OTHER_ADD_LOCAL                      4
#define GPU_COMBINE_FUNC_SCALE_OTHER_ADD_LOCAL_ALPHA                5
#define GPU_COMBINE_FUNC_SCALE_OTHER_MINUS_LOCAL                    6
#define GPU_COMBINE_FUNC_SCALE_OTHER_MINUS_LOCAL_ADD_LOCAL          7
#define GPU_COMBINE_FUNC_BLEND                                      COMBINE_FUNC_SCALE_OTHER_MINUS_LOCAL_ADD_LOCAL
#define GPU_COMBINE_FUNC_SCALE_OTHER_MINUS_LOCAL_ADD_LOCAL_ALPHA    8
#define GPU_COMBINE_FUNC_SCALE_MINUS_LOCAL_ADD_LOCAL                9
#define GPU_COMBINE_FUNC_BLEND_LOCAL                                10
#define GPU_COMBINE_FUNC_SCALE_MINUS_LOCAL_ADD_LOCAL_ALPHA          11

#define GPU_COMBINE_FACTOR_ZERO                                     0
#define GPU_COMBINE_FACTOR_LOCAL                                    1
#define GPU_COMBINE_FACTOR_OTHER_ALPHA                              2
#define GPU_COMBINE_FACTOR_LOCAL_ALPHA                              3
#define GPU_COMBINE_FACTOR_ONE                                      4
#define GPU_COMBINE_FACTOR_ONE_MINUS_LOCAL                          5
#define GPU_COMBINE_FACTOR_ONE_MINUS_OTHER_ALPHA                    6
#define GPU_COMBINE_FACTOR_ONE_MINUS_LOCAL_ALPHA                    7

#define GPU_BLEND_ZERO              0
#define GPU_BLEND_ONE               1
#define GPU_BLEND_CONST_RGB         2
#define GPU_BLEND_CONST_A           3
#define GPU_BLEND_ARG0_RGB          4
#define GPU_BLEND_ARG0_A            5
#define GPU_BLEND_ARG1_RGB          6
#define GPU_BLEND_ARG1_A            7
#define GPU_BLEND_CONST_INV_RGB     8
#define GPU_BLEND_CONST_INV_A       9
#define GPU_BLEND_ARG0_INV_RGB      10
#define GPU_BLEND_ARG0_INV_A        11
#define GPU_BLEND_ARG1_INV_RGB      12
#define GPU_BLEND_ARG1_INV_A        13

#define GPU_COMPARE_NEVER           0
#define GPU_COMPARE_ALWAYS          1
#define GPU_COMPARE_LESS            2
#define GPU_COMPARE_LEQUAL          3
#define GPU_COMPARE_GREATER         4
#define GPU_COMPARE_GEQUAL          5
#define GPU_COMPARE_EQUAL           6
#define GPU_COMPARE_NOTEQUAL        7

#define GPU_FBDISPLAYMODE(res, dither)      ((res & 7) | (dither ? (1 << 3) : 0))
#define GPU_FBCLIP(x1, x2)                  ((x1 & 0xFFFF) | (x2 << 16))
#define GPU_FBPXCONFIG(ccfnRgb, ccfnA, ccfacRgb, ccfacA, ccinvRgb, ccinvA, blendSrc, blendDst, tmu0en, tmu1en, fogen) \
    ((ccfnRgb & 0xF) | ((ccfnA & 0xF) << 4) | ((ccfacRgb & 0x7) << 8) | ((ccfacA & 0x7) << 11) | ((ccinvRgb ? 1 : 0) << 14) | ((ccinvA ? 1 : 0) << 15) \
    | ((blendSrc & 0xF) << 16) | ((blendDst & 0xF) << 20) | ((tmu0en ? 1 : 0) << 24) | ((tmu1en ? 1 : 0) << 25) | ((fogen ? 1 : 0) << 26))
#define GPU_FBMODE(cmask, dmask, dfunc, afunc, aref) \
                                            ((cmask ? 1 : 0) | (dmask ? 2 : 0) | ((dfunc & 7) << 2) | ((afunc & 7) << 5) | ((aref & 0xFF) << 8))
#define GPU_COLOR(r, g, b, a)               ((r & 0xFF) | ((g & 0xFF) << 8) | ((b & 0xFF) << 16) | ((a & 0xFF) << 24))
#define GPU_TLOD(min, max, bias)            ((min & 15) | ((max & 15) << 4) | ((bias & 63) << 8))
#define GPU_TMODE(ccfnRgb, ccfnA, ccfacRgb, ccfacA, ccinvRgb, ccinvA, filter, clampS, clampT, fmt, nccTbl) \
    ((ccfnRgb & 0xF) | ((ccfnA & 0xF) << 4) | ((ccfacRgb & 0x7) << 8) | ((ccfacA & 0x7) << 11) | ((ccinvRgb ? 1 : 0) << 14) | ((ccinvA ? 1 : 0) << 15) \
    | ((filter ? 1 : 0) << 17) | ((clampS ? 1 : 0) << 18) | ((clampT ? 1 : 0) << 19) | ((fmt & 7) << 20) | ((nccTbl ? 1 : 0) << 23))

typedef struct {
    float sow;
    float tow;
} GpuTmuVertex_t;

typedef struct {
    float x;
    float y;
    float z;
    float oow;
    float r;
    float g;
    float b;
    float a;
    GpuTmuVertex_t tmu0;
    GpuTmuVertex_t tmu1;
} GpuVertex_t;

typedef struct {
    GpuVertex_t vtx;
} GpuCmdDrawPoint_t;

typedef struct {
    GpuVertex_t vtx[2];
} GpuCmdDrawLine_t;

typedef struct {
    int sign;
    GpuVertex_t vtx[3];
} GpuCmdDrawTriangle_t;

typedef struct {
    unsigned int packet_type;
    union {
        GpuCmdDrawPoint_t drawpt;
        GpuCmdDrawLine_t drawline;
        GpuCmdDrawTriangle_t drawtri;
    } param;
} GpuCmd_t;

extern unsigned int gpu_getreg(int chipsel, int addr);
extern void gpu_setreg(int chipsel, int addr, unsigned int val);
extern float gpu_getregf(int chipsel, int addr);
extern void gpu_setregf(int chipsel, int addr, float val);
extern void gpu_cmd(GpuCmd_t* cmd, int size);
extern void gpu_flushcmd();
extern void gpu_readmem(int chipsel, int addr, int len, void* buffer);
extern void gpu_writemem(int chipsel, int addr, int len, const void* buffer);

#endif