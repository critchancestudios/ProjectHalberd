#ifndef _GLIDEUTL_H_
#define _GLIDEUTL_H_

#include "glide.h"

typedef struct
{
  FxU32               width, height;
  int                 small_lod, large_lod;
  GrTextureFormat_t   format;
} Gu3dfHeader;

typedef struct
{
  FxU8  yRGB[16];
  FxI16 iRGB[4][3];
  FxI16 qRGB[4][3];
  FxU32 packed_data[12];
} GuNccTable;

typedef struct {
    FxU32 data[256];
} GuTexPalette;

typedef union {
    GuNccTable   nccTable;
    GuTexPalette palette;
} GuTexTable;

typedef struct
{
  Gu3dfHeader  header;
  GuTexTable   table;
  void        *data;
  FxU32        mem_required;    /* memory required for mip map in bytes. */
} Gu3dfInfo;

float guFogTableIndexToW(int i);
void guFogGenerateExp(GrFog_t fogtable[], float density);
void guFogGenerateExp2(GrFog_t fogtable[], float density);
void guFogGenerateLinear(GrFog_t fogtable[], float nearZ, float farZ);

FxBool gu3dfGetInfo(const char* filename, Gu3dfInfo* info);
FxBool gu3dfLoad(const char* filename, Gu3dfInfo* info);

#endif