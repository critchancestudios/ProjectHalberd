#ifndef _UART_H_
#define _UART_H_

extern void uart_putc(char c);
extern void uart_flush();

void uart_puts(const char* str);

#endif