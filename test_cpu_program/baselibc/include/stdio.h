/*
 * stdio.h
 */

#ifndef _STDIO_H
#define _STDIO_H

#include <klibc/extern.h>
#include <klibc/inline.h>
#include <stdarg.h>
#include <stddef.h>
#include <string.h>

#include <printf.h>

struct File;
typedef struct File FILE;

#ifndef EOF
# define EOF (-1)
#endif

#define SEEK_SET	0
#define SEEK_CUR	1
#define SEEK_END	2

extern FILE* const stdin;
extern FILE* const stdout;
extern FILE* const stderr;

FILE* fopen(const char* filename, const char* mode);
void fclose(FILE* stream);
int fflush(FILE* stream);
size_t fread(void* ptr, size_t size, size_t count, FILE* stream);
size_t fwrite(const void* buffer, size_t size, size_t count, FILE* stream);
int fseek(FILE* stream, long offset, int whence);
long ftell(FILE* stream);

__extern_inline int fputs(const char *s, FILE *f)
{
	return fwrite(s, 1, strlen(s), f);
}

__extern_inline int puts(const char *s)
{
	return fwrite(s, 1, strlen(s), stdout) + fwrite("\n", 1, 1, stdout);
}

__extern_inline int fputc(int c, FILE *f)
{
	unsigned char ch = c;
	return fwrite(&ch, 1, 1, f) == 1 ? ch : EOF;
}

__extern char *fgets(char *, int, FILE *);
__extern_inline int fgetc(FILE *f)
{
	unsigned char ch;
	return fread(&ch, 1, 1, f) == 1 ? ch : EOF;
}

#define putc(c,f)  fputc((c),(f))
#define putchar(c) fputc((c),stdout)
#define getc(f) fgetc(f)
#define getchar() fgetc(stdin)

__extern int sscanf(const char *, const char *, ...);
__extern int vsscanf(const char *, const char *, va_list);

#endif				/* _STDIO_H */
