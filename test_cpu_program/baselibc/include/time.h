#ifndef _TIME_H_
#define _TIME_H_

// sys clock resolution is in milliseconds
#define CLOCKS_PER_SEC 1000

typedef long clock_t;
typedef long time_t;

struct tm {
    int tm_sec;
    int tm_min;
    int tm_hour;
    int tm_mday;
    int tm_mon;
    int tm_year;
    int tm_wday;
    int tm_yday;
};

extern clock_t clock();
extern time_t time(time_t* timer);

struct tm* localtime(const time_t* timer);

#endif