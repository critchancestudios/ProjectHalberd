#ifndef _MATH_H_
#define _MATH_H_

#define HUGE_VAL __builtin_huge_val()
#define HUGE_VALF __builtin_huge_valf()

double acos(double);
double asin(double);
double atan(double);
double atan2(double, double);
double cos(double);
double sin(double);
double tan(double);
double cosh(double);
double sinh(double);
double tanh(double);
double exp(double);
double exp2(double);
double frexp(double, int*);
double ldexp(double, int);
double log(double);
double log2(double);
double log10(double);
double logb(double);
double modf(double, double*);
double pow(double, double);
double sqrt(double);
double ceil(double);
double fabs(double);
double floor(double);
double fmod(double, double);
double acosh(double);
double asinh(double);
double atanh(double);
double round(double);
double fmin(double, double);
double fmax(double, double);
double copysign(double, double);
double trunc(double);

float acosf(float);
float asinf(float);
float atanf(float);
float atan2f(float, float);
float cosf(float);
float sinf(float);
float tanf(float);
float coshf(float);
float sinhf(float);
float tanhf(float);
float expf(float);
float exp2f(float);
float frexpf(float, int*);
float ldexpf(float, int);
float logf(float);
float log2f(float);
float log10f(float);
float logbf(float);
float modff(float, float*);
float powf(float, float);
float sqrtf(float);
float ceilf(float);
float fabsf(float);
float floorf(float);
float fmodf(float, float);
float acoshf(float);
float asinhf(float);
float atanhf(float);
float roundf(float);
float fminf(float, float);
float fmaxf(float, float);
float copysignf(float, float);
float truncf(float);

#endif