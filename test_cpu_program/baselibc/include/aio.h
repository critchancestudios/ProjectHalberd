#ifndef _AIO_H_
#define _AIO_H_

#include <stdio.h>

#define AIO_STATE_INIT      0
#define AIO_STATE_OK        1
#define AIO_STATE_PENDING   2
#define AIO_STATE_ERR       3

int aio_query(int reqId);
int aio_complete(int reqId);
int aio_read(void* ptr, size_t size, FILE* stream);
int aio_write(const void* ptr, size_t size, FILE* stream);
int aio_checkmc(int slot);
int aio_formatmc(int slot);

#endif