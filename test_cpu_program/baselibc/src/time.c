#include "stddef.h"
#include "time.h"

#define LEAPOCH (946684800L + 86400*(31+29))

#define DAYS_PER_400Y (365*400 + 97)
#define DAYS_PER_100Y (365*100 + 24)
#define DAYS_PER_4Y   (365*4   + 1)

extern int sys_clock();
extern int sys_time();

static const char days_in_month[] = {31,30,31,30,31,31,30,31,30,31,31,29};

static struct tm lt;

clock_t clock() {
    return (clock_t)sys_clock();
}

time_t time(time_t* timer) {
    time_t t = (time_t)sys_time();
    if (timer != NULL) {
        *timer = t;
    }
    return t;
}

// adapted from MUSL: http://git.musl-libc.org/cgit/musl/tree/src/time/__secs_to_tm.c?h=v0.9.15
struct tm* localtime(const time_t* timer) {
    if (timer == NULL) return NULL;

    time_t t = *timer;

    long days, secs;
	int remdays, remsecs, remyears;
	int qc_cycles, c_cycles, q_cycles;
	int years, months;
	int wday, yday, leap;

    secs = t - LEAPOCH;
	days = secs / 86400;
	remsecs = secs % 86400;
	if (remsecs < 0) {
		remsecs += 86400;
		days--;
	}

    wday = (3+days)%7;
	if (wday < 0) wday += 7;

	qc_cycles = days / DAYS_PER_400Y;
	remdays = days % DAYS_PER_400Y;
	if (remdays < 0) {
		remdays += DAYS_PER_400Y;
		qc_cycles--;
	}

    c_cycles = remdays / DAYS_PER_100Y;
	if (c_cycles == 4) c_cycles--;
	remdays -= c_cycles * DAYS_PER_100Y;

	q_cycles = remdays / DAYS_PER_4Y;
	if (q_cycles == 25) q_cycles--;
	remdays -= q_cycles * DAYS_PER_4Y;

	remyears = remdays / 365;
	if (remyears == 4) remyears--;
	remdays -= remyears * 365;

    leap = !remyears && (q_cycles || !c_cycles);
	yday = remdays + 31 + 28 + leap;
	if (yday >= 365+leap) yday -= 365+leap;

	years = remyears + 4*q_cycles + 100*c_cycles + 400*qc_cycles;

	for (months=0; days_in_month[months] <= remdays; months++)
		remdays -= days_in_month[months];

	lt.tm_year = years + 100;
	lt.tm_mon = months + 2;
	if (lt.tm_mon >= 12) {
		lt.tm_mon -=12;
		lt.tm_year++;
	}
	lt.tm_mday = remdays + 1;
	lt.tm_wday = wday;
	lt.tm_yday = yday;

	lt.tm_hour = remsecs / 3600;
	lt.tm_min = remsecs / 60 % 60;
	lt.tm_sec = remsecs % 60;

    return &lt;
}