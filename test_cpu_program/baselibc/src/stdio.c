#include "stdio.h"
#include "errno.h"
#include "stdint.h"

#include "uart.h"
#include "disc.h"
#include "hdfs.h"

#define MAX_OPEN_FILEHANDLES 32

typedef size_t (*file_read_fn)(void* buffer, size_t size, size_t count, void* context);
typedef size_t (*file_write_fn)(const void* buffer, size_t size, size_t count, void* context);
typedef void (*file_flush_fn)(void* context);
typedef int (*file_seek_fn)(void* context, long offset, int origin);
typedef long (*file_tell_fn)(void* context);
typedef void (*file_close_fn)(void* context);

struct File
{
    int valid;
    void* context;
    file_read_fn read;
    file_write_fn write;
    file_flush_fn flush;
    file_seek_fn seek;
    file_tell_fn tell;
    file_close_fn close;
};

typedef struct {
    int dtStart;
    int dtLen;
    int curSec;
    int pos;
} DiscStreamCtx;

static FILE fd_table[MAX_OPEN_FILEHANDLES] = { 0 };
static DiscStreamCtx discstream_pool[MAX_OPEN_FILEHANDLES] = { 0 }; static int discstream_pool_ct = MAX_OPEN_FILEHANDLES;

static DiscStreamCtx* alloc_discstream() {
    if (discstream_pool_ct == 0) return NULL;
    return &discstream_pool[discstream_pool_ct--];
}

static void release_discstream(DiscStreamCtx* ctx) {
    memcpy(&discstream_pool[discstream_pool_ct++], ctx, sizeof(DiscStreamCtx));
}

static size_t stdout_read(void* buffer, size_t size, size_t count, void* context) {
    errno = EPERM;
    return 0;
}

static size_t stdout_write(const void* buffer, size_t size, size_t count, void* context) {
    int bytecount = size * count;
    const unsigned char* b = buffer;
    for (int i = 0; i < bytecount; i++) {
        uart_putc(*b++);
    }
    return count;
}

static void stdout_flush(void* context) {
    uart_flush();
}

static int stdout_seek(void* context, long offset, int origin) {
    errno = EPERM;
    return -1;
}

static long stdout_tell(void* context) {
    errno = EPERM;
    return -1;
}

static void stdout_close(void* context) {
}

static size_t dvdstream_read(void* buffer, size_t size, size_t count, void* context) {
    DiscStreamCtx* ctx = (DiscStreamCtx*)context;

    if (ctx->curSec == -1) {
        return 0;
    }

    uint8_t secbuff[DVD_SECTOR_SIZE];

    int total_bytes = size * count;
    int read_bytes = 0;

    int sec_end = ctx->dtStart + ctx->dtLen;

    while (total_bytes > 0) {
        int pos_in_sector = ctx->pos % DVD_SECTOR_SIZE;
        int remaining_in_sector = DVD_SECTOR_SIZE - pos_in_sector;
        int copy = remaining_in_sector < total_bytes ? remaining_in_sector : total_bytes;

        disc_read(ctx->curSec, DVD_SECTOR_SIZE, secbuff);
        memcpy(buffer, &secbuff[pos_in_sector], copy);

        buffer += copy;
        ctx->pos += copy;
        read_bytes += copy;
        total_bytes -= copy;

        int new_pos_in_sector = ctx->pos % DVD_SECTOR_SIZE;

        if (new_pos_in_sector <= pos_in_sector) {
            ctx->curSec++;
            if (ctx->curSec >= sec_end) break;
        }
    }

    return read_bytes / size;
}

static size_t dvdstream_write(const void* buffer, size_t size, size_t count, void* context) {
    errno = EPERM;
    return 0;
}

static void dvdstream_flush(void* context) {
}

static int dvdstream_seek(void* context, long offset, int origin) {
    DiscStreamCtx* ctx = (DiscStreamCtx*)context;

    if (origin == SEEK_SET) {
        ctx->pos = offset;
    }
    else if (origin == SEEK_CUR) {
        ctx->pos += offset;
    }
    else if (origin == SEEK_END) {
        ctx->pos = (ctx->dtLen * DVD_SECTOR_SIZE) + offset;
    }
    else {
        errno = EINVAL;
        return -1;
    }

    int secnum = ctx->pos / DVD_SECTOR_SIZE;

    if (secnum < 0) {
        errno = EINVAL;
        return -1;
    }
    else if (secnum >= ctx->dtLen) {
        // seeking beyond end of stream is technically supported, but reads and writes will fail
        secnum = -1;
        return 0;
    }

    // compute new sector
    ctx->curSec = ctx->dtStart + secnum;

    return 0;
}

static long dvdstream_tell(void* context) {
    DiscStreamCtx* ctx = (DiscStreamCtx*)context;
    return ctx->pos;
}

static void dvdstream_close(void* context) {
    release_discstream((DiscStreamCtx*)context);
}

static FILE __stdout = {
    .valid = 1,
    .context = NULL,
    .read = stdout_read,
    .write = stdout_write,
    .flush = stdout_flush,
    .seek = stdout_seek,
    .tell = stdout_tell,
    .close = stdout_close
};

FILE* const stdout = &__stdout;
FILE* const stderr = &__stdout;

static FILE* alloc_file() {
    for (int i = 0; i < MAX_OPEN_FILEHANDLES; i++) {
        if (!fd_table[i].valid) {
            return &fd_table[i];
        }
    }

    return NULL;
}

FILE* fopen(const char* filename, const char* mode) {
    FILE* f = alloc_file();
    if (f == NULL) {
        errno = ENFILE;
        return NULL;
    }

    if (strncmp(filename, "/dvd/", 5) == 0) {
        const char* path = filename + 5;
        int dataStart, dataLen;
        int result = hdfs_findfile(path, &dataStart, &dataLen);
        
        if (result == HDFS_OK) {
            DiscStreamCtx* ctx = alloc_discstream();
            ctx->dtStart = dataStart;
            ctx->dtLen = dataLen;
            ctx->curSec = dataStart;
            ctx->pos = 0;
            f->context = ctx;
            f->read = dvdstream_read;
            f->write = dvdstream_write;
            f->flush = dvdstream_flush;
            f->seek = dvdstream_seek;
            f->tell = dvdstream_tell;
            f->close = dvdstream_close;
            f->valid = 1;
            return f;
        }
        
        if (result == HDFS_NODISC) {
            errno = ENOMEDIUM;
        }
        else if (result == HDFS_INVALID) {
            errno = EINVAL;
        }
        else if (result == HDFS_NOTFOUND) {
            errno = ENOENT;
        }
        else if (result == HDFS_NOTDIR) {
            errno = ENOTDIR;
        }

        return NULL;
    }

    errno = ENODEV;
    return NULL;
}

void fclose(FILE* stream) {
    if (!stream->valid) {
        return;
    }

    stream->close(stream->context);
    stream->valid = 0;
}

int fflush(FILE* stream) {
    if (!stream->valid) {
        errno = EBADF;
        return -1;
    }

    stream->flush(stream->context);
    return 0;
}

size_t fread(void* buffer, size_t size, size_t count, FILE* stream) {
    if (!stream->valid) {
        errno = EBADF;
        return -1;
    }

    return stream->read(buffer, size, count, stream->context);
}

size_t fwrite(const void* buffer, size_t size, size_t count, FILE* stream) {
    if (!stream->valid) {
        errno = EBADF;
        return -1;
    }

    return stream->write(buffer, size, count, stream->context);
}

int fseek(FILE* stream, long offset, int whence) {
    if (!stream->valid) {
        errno = EBADF;
        return -1;
    }

    return stream->seek(stream->context, offset, whence);
}

long ftell(FILE* stream) {
    if (!stream->valid) {
        errno = EBADF;
        return -1;
    }

    return stream->tell(stream->context);
}