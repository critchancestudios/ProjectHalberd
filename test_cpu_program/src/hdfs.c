#include <stdio.h>

#include "hdfs.h"
#include "crc.h"

#define HDFS_HDR_CRC_BYTES  72
#define DT_ENTRIES          (DVD_SECTOR_SIZE / sizeof(HDFS_DtEntry)) 

typedef struct {
    char magic[4];
    char pub[32];
    char game[32];
    unsigned int rootDtSize;
    unsigned int hdrChecksum;
} HDFS_Header;

typedef struct {
    char filename[52];
    unsigned int flags;
    unsigned int dataStart;
    unsigned int dataLength;
} HDFS_DtEntry;

static HDFS_Header header = { 0 };
static int disc_ok = 0;

static HDFS_DtEntry dtbuf[DT_ENTRIES];

static int checkstr(const char* a, const char* b, int n) {
    for (int i = 0; i < n; i++) {
        if (*a != *b) {
            return 0;
        }

        if (*a == 0) {
            return 1;
        }

        a++;
        b++;
    }

    return 1;
}

// search the given directory table for a file with the given name
static int scan_dt(int dt, int dtlen, const char* filename, HDFS_DtEntry* info) {
    for (int i = 0; i < dtlen; i++) {
        int sector = dt + i;
        disc_read(sector, DVD_SECTOR_SIZE, &dtbuf);

        for (int j = 0; j < DT_ENTRIES; j++) {
            if (dtbuf[j].filename[0] == 0) {
                // hit the end of the dtable, early exit
                return -1;
            }
            else if (checkstr(dtbuf[j].filename, filename, 52)) {
                // found it
                if (info != 0) {
                    *info = dtbuf[j];
                }
                return (sector * DT_ENTRIES) + j;
            }
        }
    }

    return -1;
}

static char* get_path_elem(char** state, char* buf) {
    char* path = *state;
    if (*path == '/') path++;

    char* dst = buf;
    int len = 0;
    
    while (*path != '/' && *path != 0) {
        *dst++ = *path++;
        len++;
    }

    *state = path;
    *dst = 0;
    return len == 0 ? 0 : buf;
}

static int find_file(const char* path, HDFS_DtEntry* info) {
    int cur_dt, cur_dtlen;
    int isfile = 0;

    cur_dt = 1;
    cur_dtlen = header.rootDtSize;

    char* fp = path;
    char pathbuf[64];

    char* tok = get_path_elem(&fp, pathbuf);

    if (tok == 0) {
        return HDFS_NOTFOUND;
    }

    while (tok != 0) {
        if (isfile) {
            // path element is not a directory
            return HDFS_NOTDIR;
        }

        int ent = scan_dt(cur_dt, cur_dtlen, tok, info);
        if (ent == -1) {
            // couldn't find part of the path
            return HDFS_NOTFOUND;
        }

        isfile = (info->flags & 1) == 0;
        tok = get_path_elem(&fp, pathbuf);

        cur_dt = info->dataStart;
        cur_dtlen = info->dataLength;
    }

    return HDFS_OK;
}

int hdfs_init() {
    disc_ok = 0;

    // make sure disc is present
    if (!disc_present()) {
        return HDFS_NODISC;
    }

    // read header sector
    disc_read(0, sizeof(HDFS_Header), &header);

    // verify magic
    if (!checkstr(header.magic, "HDFS", 4)) {
        return HDFS_BADHDR;
    }

    // calculate & verify crc
    unsigned int hdr_crc = crc32(0, &header, HDFS_HDR_CRC_BYTES);

    if (hdr_crc != header.hdrChecksum) {
        return HDFS_BADCRC;
    }

    // valid game disc
    disc_ok = 1;
    return HDFS_OK;
}

int hdfs_getinfo(DiscInfo* info) {
    if (!disc_ok) {
        return HDFS_INVALID;
    }

    if (!disc_present()) {
        return HDFS_NODISC;
    }

    memcpy(info->publisher, header.pub, 32);
    memcpy(info->gamename, header.game, 32);
    return 1;
}

int hdfs_findfile(const char* path, int* dataStart, int* dataLength) {
    if (!disc_ok) {
        return HDFS_INVALID;
    }

    if (!disc_present()) {
        return HDFS_NODISC;
    }

    HDFS_DtEntry ent;
    int ret = find_file(path, &ent);
    if (dataStart != 0) {
        *dataStart = ent.dataStart;
    }
    if (dataLength != 0) {
        *dataLength = ent.dataLength;
    }
    
    return ret;
}