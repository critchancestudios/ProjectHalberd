#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

#include "glide.h"
#include "glideutl.h"

#include "hdfs.h"
#include "hfat.h"

void main() {
    // ensure disc FS is initialized - stdio will not do this automatically!
    if (hdfs_init() != HDFS_OK) {
        printf("Failed initializing disc FS\n");
        return;
    }

    // init memory card A
    if (hfat_check(0) != MC_CHECK_OK) {
        printf("Memcard check failed - formatting\n");
        hfat_format(0);
    }
    else {
        printf("Memcard check OK\n");
    }

    printf("Hello from ARM!\n");

    grGlideInit(GPU_DM_640x480_NTSC);

    // load lava.3df
    GrTexInfo texinfo;
    Gu3dfInfo texinfo_3df;
    if (gu3dfGetInfo("/dvd/subfolder/lava.3df", &texinfo_3df)) {
        printf("Format: %d\n", texinfo_3df.header.format);
        printf("Size: %d x %d\n", texinfo_3df.header.width, texinfo_3df.header.height);
        printf("LOD: %d -> %d\n", texinfo_3df.header.large_lod, texinfo_3df.header.small_lod);
        texinfo_3df.data = malloc(texinfo_3df.mem_required);
        if (gu3dfLoad("/dvd/subfolder/lava.3df", &texinfo_3df)) {
            grTexDownloadTable(GR_TMU0, GR_TEXTABLE_PALETTE, texinfo_3df.table.palette.data);

            texinfo.data = texinfo_3df.data;
            texinfo.format = texinfo_3df.header.format;
            texinfo.largeLod = texinfo_3df.header.large_lod;
            texinfo.smallLod = texinfo_3df.header.small_lod;

            grTexDownloadMipMap(GR_TMU0, 0, &texinfo);
        }
        else {
            printf("Failed reading texture data\n");
        }
    }
    else {
        printf("Failed reading texture info\n");
    }

    // fog
    GrFog_t fog[64];
    guFogGenerateLinear(fog, 1.0f, 5.0f);
    grFogTable(fog);
    grFogColorValue(0xAAAAAA);
    grFogMode(GR_FOG_ENABLE);

    // set texture source & tex params
    grTexSource(GR_TMU0, 0, &texinfo);
    grTexFilterMode(GR_TMU0, GR_TEXTUREFILTER_BILINEAR, GR_TEXTUREFILTER_BILINEAR);
    grTexClampMode(GR_TMU0, GR_TEXTURECLAMP_WRAP, GR_TEXTURECLAMP_WRAP);

    // set up color combine to multiply w/ TMU input
    grColorCombine(GR_COMBINE_FUNCTION_SCALE_OTHER, GR_COMBINE_FACTOR_LOCAL, FXFALSE);
    grAlphaCombine(GR_COMBINE_FUNCTION_SCALE_OTHER, GR_COMBINE_FACTOR_LOCAL, FXFALSE);

    while (1) {
        grBufferClear(0x4080FF, 1.0f);

        // draw a triangle
        GrVertex tri[3];
        tri[0].x = 0.0f;
        tri[0].y = -5.0f;
        tri[0].z = 10.0f;
        tri[0].oow = 0.1f;
        tri[0].r = 1.0f;
        tri[0].g = 1.0f;
        tri[0].b = 1.0f;
        tri[0].a = 1.0f;
        tri[0].tmu0.sow = 0.0f;
        tri[0].tmu0.tow = 0.0f;

        tri[1].x = 0.5f;
        tri[1].y = 0.5f;
        tri[1].z = 0.0f;
        tri[1].oow = 1.0f;
        tri[1].r = 1.0f;
        tri[1].g = 1.0f;
        tri[1].b = 1.0f;
        tri[1].a = 1.0f;
        tri[1].tmu0.sow = 0.0f;
        tri[1].tmu0.tow = 1.0f;

        tri[2].x = -0.5f;
        tri[2].y = 0.5f;
        tri[2].z = 0.0f;
        tri[2].oow = 1.0f;
        tri[2].r = 1.0f;
        tri[2].g = 1.0f;
        tri[2].b = 1.0f;
        tri[2].a = 1.0f;
        tri[2].tmu0.sow = 1.0f;
        tri[2].tmu0.tow = 1.0f;

        grDrawTriangle(&tri[0], &tri[1], &tri[2]);

        // swap buffers
        grBufferSwap();
    }

    grGlideShutdown();
}