#include <string.h>
#include <stdio.h>
#include <errno.h>

#include "hfat.h"
#include "crc.h"

// just a ultra simple flat FAT-like fs
// lots of inspiration taken from FAT12, along with PS2 and Gamecube schemes
// nothing really in the way of error recovery - "don't turn your console off" and all that :3

#define FAT_ENTRIES (MC_SECTOR_SIZE / 2)
#define DT_ENTRIES  (MC_SECTOR_SIZE / sizeof(HFAT_DtEntry)) 

#define HDR_MAGIC "HFAT"
#define HDR_CRC_BYTES 24

typedef struct {
    char oem[8];
    uint32_t serial;
    char magic[4];
    uint32_t fmtTime;
    uint16_t dtSectors;
    uint16_t fatSectors;
    uint32_t hdrChecksum;
    uint32_t dtChecksum;
    uint32_t fatChecksum;
} HFAT_Header;

typedef struct {
    int valid;
} MCState;

static MCState memcards[NUM_MEMCARDS] = { 0 };
static int secid[MC_MAX_SECTORS] = { 0 };

// calculate crc32 of contents of given sector range on disk
static uint32_t crc32_sectors(int slot, uint32_t crc, int sectorStart, int sectorCount) {
    char sectorbuf[MC_SECTOR_SIZE];

    for (int i = 0; i < sectorCount; i++) {
        memc_read(slot, sectorStart + i, MC_SECTOR_SIZE, sectorbuf);
        crc = crc32(crc, sectorbuf, MC_SECTOR_SIZE);
    }

    return crc;
}

// given a single sector of the FAT, look for and return offset of first unallocated entry
static int allocate_from_fat_sector(uint16_t* fat, int startIdx) {
    for (int i = startIdx; i < FAT_ENTRIES; i++) {
        if (fat[i] == 0) {
            return i;
        }
    }

    return -1;
}

// given a single sector of the DT, look for and return offset of first unallocated entry
static int allocate_from_dt_sector(HFAT_DtEntry* dt) {
    for (int i = 0; i < DT_ENTRIES; i++) {
        if (dt[i].filename[0] == 0) {
            return i;
        }
    }

    return -1;
}

// allocate single entry within DT
// returns index of entry within DT, or -1 if failed
static int allocate_dt_entry(int mc, const HFAT_Header* header) {
    // scan through DT looking for a free entry
    int dir_start = 1;

    HFAT_DtEntry dir_sector[DT_ENTRIES];

    for (int i = 0; i < header->dtSectors; i++) {
        // scan dt sector for a free entry
        memc_read(mc, dir_start + i, MC_SECTOR_SIZE, &dir_sector);

        int idx = allocate_from_dt_sector(dir_sector);
        if (idx != -1) {
            return idx + (i * DT_ENTRIES);
        }
    }

    return -1;
}

// allocate single data sector within FAT
// returns sector number, or -1 if failed
static int allocate_data_sector(int mc, const HFAT_Header* header, int startidx) {
    // scan through the FAT looking for a free sector
    int fat_start = 1 + header->dtSectors;
    int data_start = fat_start + header->fatSectors;

    uint16_t fat_sector[FAT_ENTRIES];

    int first_sec = startidx / FAT_ENTRIES;
    int fat_offs = startidx % FAT_ENTRIES;

    for (int i = first_sec; i < header->fatSectors; i++) {
        // scan FAT sector for a free entry
        memc_read(mc, fat_start + i, MC_SECTOR_SIZE, &fat_sector);

        int idx = allocate_from_fat_sector(fat_sector, fat_offs);
        fat_offs = 0;

        if (idx != -1) {
            return data_start + idx + (i * FAT_ENTRIES);
        }
    }

    return -1;
}

// allocate a number of data sectors within FAT, writing to sector number array
// returns number of sectors allocated
static int allocate_data_sectors(int mc, const HFAT_Header* header, int* sectors, int count) {
    int alloc_cnt = 0;
    int start_idx = 0;

    int fat_start = 1 + header->dtSectors;
    int data_start = fat_start + header->fatSectors;

    for (int i = 0; i < count; i++) {
        int sec = allocate_data_sector(mc, header, start_idx);
        if (sec == -1) break;

        sectors[i] = sec;
        alloc_cnt++;

        // start search at next fat entry so it doesn't keep flagging the same entry (which we haven't updated yet)
        // if this would go past end of FAT, just end our search instead
        start_idx = (sec - data_start) + 1;

        if (start_idx >= header->fatSectors) {
            break;
        }
    }

    return alloc_cnt;
}

int hfat_valid(int mc) {
    if (mc < 0 || mc >= NUM_MEMCARDS) return 0;
    return memcards[mc].valid;
}

int hfat_check(int mc) {
    if (mc < 0 || mc >= NUM_MEMCARDS) return MC_INVALID;
    
    HFAT_Header header;
    memc_read(mc, 0, sizeof(HFAT_Header), &header);

    memcards[mc].valid = 0;

    if (strncmp(header.magic, HDR_MAGIC, 4)) {
        return MC_CHECK_NOFS;
    }

    // compute header CRC32
    uint32_t hdr_crc = crc32(0, &header, HDR_CRC_BYTES);

    if (hdr_crc != header.hdrChecksum) {
        return MC_CHECK_CORRUPT;
    }

    // compute crc for DT
    int dt_start = 1;
    uint32_t dt_crc = crc32_sectors(mc, 0, dt_start, header.dtSectors);

    if (dt_crc != header.dtChecksum) {
        return MC_CHECK_CORRUPT;
    }

    // compute crc for FAT
    int fat_start = 1 + header.dtSectors;
    uint32_t fat_crc = crc32_sectors(mc, 0, fat_start, header.fatSectors);

    if (fat_crc != header.fatChecksum) {
        return MC_CHECK_CORRUPT;
    }

    // filesystem ok!
    memcards[mc].valid = 1;
    return MC_CHECK_OK;
}

int hfat_format(int mc) {
    if (mc < 0 || mc >= NUM_MEMCARDS) return -1;

    // format a 2MiB memory card
    char buff[MC_SECTOR_SIZE];
    memset(buff, 0, MC_SECTOR_SIZE);

    HFAT_Header header;

    // preserve existing OEM+serial
    memc_read(mc, 0, 12, &header);

    // overwrite other fields
    memcpy(&header.magic, HDR_MAGIC, 4);
    header.fmtTime = (uint32_t)time(NULL);
    header.dtSectors = 16;
    header.fatSectors = 16;
    header.hdrChecksum = crc32(0, &header, HDR_CRC_BYTES);
    header.fatChecksum = 0;
    header.dtChecksum = 0;

    for (int i = 0; i < header.dtSectors; i++) {
        header.dtChecksum = crc32(header.dtChecksum, buff, MC_SECTOR_SIZE);
    }

    for (int i = 0; i < header.fatSectors; i++) {
        header.fatChecksum = crc32(header.fatChecksum, buff, MC_SECTOR_SIZE);
    }

    memc_write(mc, 0, sizeof(HFAT_Header), &header);

    int dt_start = 1;
    int fat_start = dt_start + header.dtSectors;
    int data_start = fat_start + header.fatSectors;

    // write dt & fat sectors
    for (int i = 0; i < header.dtSectors; i++) {
        memc_write(mc, dt_start + i, MC_SECTOR_SIZE, buff);
    }

    for (int i = 0; i < header.fatSectors; i++) {
        memc_write(mc, fat_start + i, MC_SECTOR_SIZE, buff);
    }

    // write remaining data sectors
    for (int i = data_start; i < 4096; i++) {
        memc_write(mc, i, MC_SECTOR_SIZE, buff);
    }

    memcards[mc].valid = 1;
    return 0;
}

int hfat_find_file(int mc, const char* filename) {
    if (mc < 0 || mc >= NUM_MEMCARDS) return -1;
    if (!memcards[mc].valid) return -1;
    
    HFAT_Header header;
    memc_read(mc, 0, sizeof(HFAT_Header), &header);

    int dir_start = 1;

    HFAT_DtEntry dir_sector[DT_ENTRIES];

    for (int i = 0; i < header.dtSectors; i++) {
        memc_read(mc, dir_start + i, MC_SECTOR_SIZE, &dir_sector);

        for (int j = 0; j < DT_ENTRIES; j++) {
            if (strncmp(dir_sector[j].filename, filename, 20) == 0) {
                // found it
                return j + (i * DT_ENTRIES);
            }
        }
    }

    return -1;
}

int hfat_delete_file(int mc, int dirEnt) {
    if (mc < 0 || mc >= NUM_MEMCARDS) return -1;
    if (!memcards[mc].valid) return -1;
    
    HFAT_Header header;
    memc_read(mc, 0, sizeof(HFAT_Header), &header);

    int dt_start = 1;
    int fat_start = 1 + header.dtSectors;

    int dt_sec = 1 + (dirEnt / DT_ENTRIES);
    int dt_offs = dirEnt % DT_ENTRIES;

    int prev_fat = -1;

    uint8_t secbuf[MC_SECTOR_SIZE];
    uint16_t* fatbuf = (uint16_t*)secbuf;
    HFAT_DtEntry* dtbuf = (HFAT_DtEntry*)secbuf;

    // erase entry from dt
    memc_read(mc, dt_sec, MC_SECTOR_SIZE, secbuf);
    HFAT_DtEntry ent = dtbuf[dt_offs];
    memset(&dtbuf[dt_offs], 0, sizeof(HFAT_DtEntry));
    memc_write(mc, dt_sec, MC_SECTOR_SIZE, secbuf);

    // clear out FAT chain
    int cur_sec = ent.dataStart;
    for (int i = 0; i < ent.dataLength; i++) {
        // convert sector number into offset within FAT
        int data_start = fat_start + header.fatSectors;
        int fat_idx = cur_sec - data_start;

        int fat_sec = fat_start + (fat_idx / FAT_ENTRIES);
        int fat_offs = fat_idx % FAT_ENTRIES;

        if (fat_sec != prev_fat) {
            if (prev_fat != -1) {
                // flush sector
                memc_write(mc, prev_fat, MC_SECTOR_SIZE, secbuf);
            }
            prev_fat = fat_sec;
            memc_write(mc, fat_sec, MC_SECTOR_SIZE, secbuf);
        }

        uint16_t link = fatbuf[fat_offs];
        fatbuf[fat_offs] = 0;

        if (link == 0xFFFF) {
            if (i < ent.dataLength - 1) {
                printf("HFAT ERROR: Encountered unexpected end-of-chain while erasing file. FAT may be corrupt\n");
            }
            return -1;
        }
        else if (link == 0) {
            printf("HFAT ERROR: Encountered unallocated block while erasing file. FAT may be corrupt\n");
            return -1;
        }
        else {
            cur_sec = link;
        }
    }

    if (prev_fat != -1) {
        // flush sector
        memc_write(mc, prev_fat, MC_SECTOR_SIZE, secbuf);
    }

    // finally, recompute checksums for dirtable & fat sectors
    header.dtChecksum = crc32_sectors(mc, 0, dt_start, header.dtSectors);
    header.fatChecksum = crc32_sectors(mc, 0, fat_start, header.fatSectors);

    memc_write(mc, 0, sizeof(HFAT_Header), &header);

    return 0;
}

int hfat_create_file(int mc, const char* filename, uint32_t len, uint32_t metaOffset) {
    if (mc < 0 || mc >= NUM_MEMCARDS) return -1;
    if (!memcards[mc].valid) return -1;

    // ensure file does not already exist
    if (hfat_find_file(mc, filename) != -1) {
        errno = EEXIST;
        return -1;
    }
    
    HFAT_Header header;
    memc_read(mc, 0, sizeof(HFAT_Header), &header);

    int dt_start = 1;
    int fat_start = dt_start + header.dtSectors;

    uint8_t secbuf[MC_SECTOR_SIZE];

    int prev_fat = -1;
    uint16_t* fatbuf = (uint16_t*)secbuf;
    HFAT_DtEntry* dtbuf = (HFAT_DtEntry*)secbuf;

    // allocate entry in dt
    int dir_ent = allocate_dt_entry(mc, &header);
    if (dir_ent == -1) {
        errno = ENOSPC;
        return -1;
    }

    // allocate sectors for file data
    if (allocate_data_sectors(mc, &header, secid, len) != len) {
        errno = ENOSPC;
        return -1;
    }

    // link up fat sectors
    for (int i = 0; i < len; i++) {
        // convert sector number into offset within FAT
        int fat_start = 1 + header.dtSectors;
        int data_start = fat_start + header.fatSectors;
        int fat_idx = secid[i] - data_start;

        int fat_sec = fat_start + (fat_idx / FAT_ENTRIES);
        int fat_offs = fat_idx % FAT_ENTRIES;

        // 0xFFFF indicates last entry of chain
        uint16_t link = 0xFFFF;
        if (i < len - 1) {
            link = (uint16_t)secid[i + 1];
        }

        if (fat_sec != prev_fat) {
            if (prev_fat != -1) {
                // flush old fat sector to disk
                memc_write(mc, prev_fat, MC_SECTOR_SIZE, fatbuf);
            }
            // read in new fat sector
            prev_fat = fat_sec;
            memc_write(mc, fat_sec, MC_SECTOR_SIZE, fatbuf);
        }

        fatbuf[fat_offs] = link;
    }

    uint16_t file_head = secid[0];

    // flush fat sector to disk
    if (prev_fat != -1) {
        memc_write(mc, prev_fat, MC_SECTOR_SIZE, fatbuf);
    }

    // record new file info
    HFAT_DtEntry file_info;
    strncpy(file_info.filename, filename, 20);
    file_info.dataLength = len;
    file_info.metaOffset = metaOffset;
    file_info.dataStart = file_head;
    file_info.timestamp = (uint32_t)time(NULL);

    // read in dt sector, write new entry, & flush to disk
    int dtsec = dt_start + (dir_ent / DT_ENTRIES);
    memc_read(mc, dtsec, MC_SECTOR_SIZE, dtbuf);
    memcpy(&dtbuf[dir_ent % DT_ENTRIES], &file_info, sizeof(HFAT_DtEntry));
    memc_write(mc, dtsec, MC_SECTOR_SIZE, dtbuf);

    // finally, recompute checksums for dirtable & fat sectors
    header.dtChecksum = crc32_sectors(mc, 0, dt_start, header.dtSectors);
    header.fatChecksum = crc32_sectors(mc, 0, fat_start, header.fatSectors);

    memc_write(mc, 0, sizeof(HFAT_Header), &header);

    // return index into directory table
    return dir_ent;
}

int hfat_getinfo(int mc, int dirEnt, HFAT_DtEntry* info) {
    if (mc < 0 || mc >= NUM_MEMCARDS) return -1;
    if (!memcards[mc].valid) return -1;
    
    HFAT_Header header;
    memc_read(mc, 0, sizeof(HFAT_Header), &header);

    int dt_start = 1;
    int fat_start = 1 + header.dtSectors;

    int dt_sec = 1 + (dirEnt / DT_ENTRIES);
    int dt_offs = dirEnt % DT_ENTRIES;

    int prev_fat = -1;

    uint8_t secbuf[MC_SECTOR_SIZE];
    uint16_t* fatbuf = (uint16_t*)secbuf;
    HFAT_DtEntry* dtbuf = (HFAT_DtEntry*)secbuf;

    // get entry from dt
    memc_read(mc, dt_sec, MC_SECTOR_SIZE, secbuf);
    memcpy(info, &dtbuf[dt_offs], sizeof(HFAT_DtEntry));

    return 0;
}

int hfat_next_sector(int mc, int cur_sector) {
    if (mc < 0 || mc >= NUM_MEMCARDS) return -1;
    if (!memcards[mc].valid) return -1;
    
    HFAT_Header header;
    memc_read(mc, 0, sizeof(HFAT_Header), &header);
    
    // convert sector number into offset within FAT
    int fat_start = 1 + header.dtSectors;
    int data_start = fat_start + header.fatSectors;
    int fat_idx = cur_sector - data_start;

    int fat_sec = fat_start + (fat_idx / FAT_ENTRIES);
    int fat_offs = fat_idx % FAT_ENTRIES;

    // look up value in FAT
    uint16_t fatbuf[FAT_ENTRIES];
    memc_read(mc, fat_sec, MC_SECTOR_SIZE, fatbuf);

    uint16_t link = fatbuf[fat_offs];

    if (link == 0xFFFF) {
        return -1;
    }
    else if (link == 0) {
        // uh oh.
        printf("HFAT ERROR: Encountered unallocated block while traversing file chain. FAT may be corrupt\n");
        return -1;
    }
    else {
        return link;
    }
}