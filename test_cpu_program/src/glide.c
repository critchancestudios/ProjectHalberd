#include "glide.h"

#define TMU_MEM (8 * 1024 * 1024)

static int screenMode = 0;
static int dither = 0;
static int screenW = 0, screenH = 0;
static int clipX1, clipY1, clipX2, clipY2;
static int cc_rgbFunc = GPU_COMBINE_FUNC_LOCAL, cc_rgbFac = GPU_COMBINE_FACTOR_ONE, cc_aFunc = GPU_COMBINE_FUNC_LOCAL, cc_aFac = GPU_COMBINE_FACTOR_ONE;
static int cc_rgbInvert = 0, cc_aInvert = 0;
static int cc_blendSrc = GPU_BLEND_ONE, cc_blendDst = GPU_BLEND_ZERO;
static int tmu0en = 1, tmu1en = 1;
static int colormask = 1, depthmask = 0;
static int depthfunc = GPU_COMPARE_ALWAYS;
static int afunc = GPU_COMPARE_ALWAYS;
static int aref = 0;
static int fogEn = 0;
static int triSign = 1;

static int tmu_baseAddr[GLIDE_NUM_TMU] = { 0 };
static int tmu_format[GLIDE_NUM_TMU] = { 0 };
static int tmu_filter[GLIDE_NUM_TMU] = { 0 };
static int tmu_clampS[GLIDE_NUM_TMU] = { 0 };
static int tmu_clampT[GLIDE_NUM_TMU] = { 0 };
static int tmu_nccTbl[GLIDE_NUM_TMU] = { 0 };
static int tmu_minLod[GLIDE_NUM_TMU] = { 0 };
static int tmu_maxLod[GLIDE_NUM_TMU] = { 0 };
static int tmu_bias[GLIDE_NUM_TMU] = { 0 };
static int tmu_cc_rgbFn[GLIDE_NUM_TMU] = { GPU_COMBINE_FUNC_LOCAL, GPU_COMBINE_FUNC_LOCAL };
static int tmu_cc_rgbFc[GLIDE_NUM_TMU] = { GPU_COMBINE_FACTOR_ONE, GPU_COMBINE_FACTOR_ONE };
static int tmu_cc_rgbInv[GLIDE_NUM_TMU] = { 0 };
static int tmu_cc_aFn[GLIDE_NUM_TMU] = { GPU_COMBINE_FUNC_LOCAL, GPU_COMBINE_FUNC_LOCAL };
static int tmu_cc_aFc[GLIDE_NUM_TMU] = { GPU_COMBINE_FACTOR_ONE, GPU_COMBINE_FACTOR_ONE };
static int tmu_cc_aInv[GLIDE_NUM_TMU] = { 0 };

#define SET_DM gpu_setreg(GPU_CHIP_FB, GPU_REG_FB_DM, GPU_FBDISPLAYMODE(screenMode, dither))
#define SET_CLIP gpu_setreg(GPU_CHIP_FB, GPU_REG_FB_CLIPX1X2, GPU_FBCLIP(clipX1, clipX2)); gpu_setreg(GPU_CHIP_FB, GPU_REG_FB_CLIPY1Y2, GPU_FBCLIP(clipY1, clipY2))
#define SET_PXCONFIG gpu_setreg(GPU_CHIP_FB, GPU_REG_FB_PXCONFIG, GPU_FBPXCONFIG(cc_rgbFunc, cc_aFunc, cc_rgbFac, cc_aFac, cc_rgbInvert, cc_aInvert, cc_blendSrc, cc_blendDst, tmu0en, tmu1en, fogEn))
#define SET_FBMODE gpu_setreg(GPU_CHIP_FB, GPU_REG_FB_MODE, GPU_FBMODE(colormask, depthmask, depthfunc, afunc, aref))
#define SET_TLOD(tmu) gpu_setreg(GPU_CHIP_TMU0 + tmu, GPU_REG_TMU_TLOD, GPU_TLOD(tmu_minLod[tmu], tmu_maxLod[tmu], tmu_bias[tmu]))
#define SET_TMODE(tmu) gpu_setreg(GPU_CHIP_TMU0 + tmu, GPU_REG_TMU_TMODE, GPU_TMODE(tmu_cc_rgbFn[tmu], tmu_cc_aFn[tmu], tmu_cc_rgbFc[tmu], tmu_cc_aFc[tmu], \
    tmu_cc_rgbInv[tmu], tmu_cc_aInv[tmu], tmu_filter[tmu], tmu_clampS[tmu], tmu_clampT[tmu], tmu_format[tmu], tmu_nccTbl[tmu]))

static int bytes_per_pixel(GrTextureFormat_t format) {
    switch (format) {
        case GR_TEXFMT_RGB_565:
        case GR_TEXFMT_ARGB_4444:
        case GR_TEXFMT_ALPHA_INTENSITY_88:
        case GR_TEXFMT_AP_88:
        case GR_TEXFMT_AYIQ_8422:
            return 2;
        case GR_TEXFMT_YIQ_422:
        case GR_TEXFMT_INTENSITY_8:
        case GR_TEXFMT_P_8:
            return 1;
    }

    // invalid/unknown texture format
    return 0;
}

void grGlideInit(FxI32 res) {
    screenMode = res;

    switch(res) {
        case GPU_DM_320x240_VGA:
        case GPU_DM_320x240_NTSC:
            screenW = 320;
            screenH = 240;
            break;
        case GPU_DM_320x288_PAL:
            screenW = 320;
            screenH = 288;
            break;
        case GPU_DM_640x480_VGA:
        case GPU_DM_640x480_NTSC:
            screenW = 640;
            screenH = 480;
            break;
        case GPU_DM_640x576_PAL:
            screenW = 640;
            screenH = 576;
            break;
    }

    clipX1 = 0;
    clipX2 = screenW;
    clipY1 = 0;
    clipY2 = screenH;

    SET_DM;
    SET_CLIP;
    SET_PXCONFIG;
    SET_FBMODE;

    grGammaCorrectionValue(1.0f);
}

void grGlideShutdown( void ) {
}

void grBufferSwap() {
    GpuCmd_t cmd;
    cmd.packet_type = GPU_CMD_SWAPBUF;
    gpu_cmd(&cmd, sizeof(cmd.packet_type));

    gpu_flushcmd();
}

void grBufferClear(FxU32 color, float depth) {
    gpu_setreg(GPU_CHIP_FB, GPU_REG_FB_CLEARCOL, color);
    gpu_setregf(GPU_CHIP_FB, GPU_REG_FB_CLEARDEPTH, depth);

    GpuCmd_t cmd;
    cmd.packet_type = GPU_CMD_FILLRECT;
    gpu_cmd(&cmd, sizeof(cmd.packet_type));
}

FxU32 grSstScreenWidth( void ) {
    return screenW;
}

FxU32 grSstScreenHeight( void ) {
    return screenH;
}

void grDrawPolygon( int nverts, const int ilist[], const GrVertex vlist[] ) {
    for (int i = 2; i < nverts; i++) {
        grDrawTriangle(&vlist[ilist[0]], &vlist[ilist[i - 1]], &vlist[ilist[i]]);
    }
}

void grDrawPolygonVertexList( int nverts, const GrVertex vlist[] ) {
    for (int i = 2; i < nverts; i++) {
        grDrawTriangle(&vlist[0], &vlist[i - 1], &vlist[i]);
    }
}

void grDrawPoint( const GrVertex *pt ) {
    GpuCmd_t cmd;
    cmd.packet_type = GPU_CMD_DRAWPT;
    cmd.param.drawpt.vtx = *pt;
    gpu_cmd(&cmd, sizeof(GpuCmd_t));
}

void grDrawLine( const GrVertex *v1, const GrVertex *v2 ) {
    GpuCmd_t cmd;
    cmd.packet_type = GPU_CMD_DRAWLINE;
    cmd.param.drawline.vtx[0] = *v1;
    cmd.param.drawline.vtx[1] = *v2;
    gpu_cmd(&cmd, sizeof(GpuCmd_t));
}

void grDrawTriangle( const GrVertex *a, const GrVertex *b, const GrVertex *c ) {
    GpuCmd_t cmd;
    cmd.packet_type = GPU_CMD_DRAWTRI;
    cmd.param.drawtri.sign = triSign;
    cmd.param.drawtri.vtx[0] = *a;
    cmd.param.drawtri.vtx[1] = *b;
    cmd.param.drawtri.vtx[2] = *c;
    gpu_cmd(&cmd, sizeof(GpuCmd_t));
}

void grAlphaBlendFunction( GrAlphaBlendFnc_t sf,   GrAlphaBlendFnc_t df ) {
    cc_blendSrc = sf;
    cc_blendDst = df;
    SET_PXCONFIG;
}

void grAlphaCombine(
            GrCombineFunction_t function, GrCombineFactor_t factor,
            FxBool invert
            ) {
    cc_aFunc = function;
    cc_aFac = factor;
    cc_aInvert = invert;
    SET_PXCONFIG;
}

void grAlphaTestFunction( GrCmpFnc_t function ) {
    afunc = function;
    SET_FBMODE;
}

void grAlphaTestReferenceValue( GrAlpha_t value ) {
    aref = value;
    SET_FBMODE;
}

void grClipWindow( FxU32 minx, FxU32 miny, FxU32 maxx, FxU32 maxy ) {
    clipX1 = minx;
    clipX2 = maxx;
    clipY1 = miny;
    clipY2 = maxy;
    SET_CLIP;
}

void grColorCombine(
            GrCombineFunction_t function, GrCombineFactor_t factor,
            FxBool invert ) {
    cc_rgbFunc = function;
    cc_rgbFac = factor;
    cc_rgbInvert = invert;

    SET_PXCONFIG;
}

void grColorMask( FxBool rgb ) {
    colormask = rgb;
    SET_FBMODE;
}

void grCullMode( GrCullMode_t mode ) {
    switch (mode) {
        case GR_CULL_DISABLE:
            triSign = 0;
            break;
        case GR_CULL_POSITIVE:
            triSign = 1;
            break;
        case GR_CULL_NEGATIVE:
            triSign = -1;
            break;
    }
}

void grConstantColorValue( GrColor_t value ) {
    gpu_setreg(GPU_CHIP_FB, GPU_REG_FB_CONSTCOL, value);
}

void grConstantColorValue4( float a, float r, float g, float b ) {
    FxU32 r2 = (FxU8)(r * 255.0f);
    FxU32 g2 = (FxU8)(g * 255.0f);
    FxU32 b2 = (FxU8)(b * 255.0f);
    FxU32 a2 = (FxU8)(a * 255.0f);
    gpu_setreg(GPU_CHIP_FB, GPU_REG_FB_CONSTCOL, r2 | (g2 << 8) | (b2 << 16) | (a2 << 24));
}

void grDepthBiasLevel( float level ) {
    gpu_setregf(GPU_CHIP_FB, GPU_REG_FB_DEPTHBIAS, level);
}

void grDepthBufferFunction( GrCmpFnc_t function ) {
    depthfunc = function;
    SET_FBMODE;
}

void grDepthMask( FxBool mask ) {
    depthmask = mask;
    SET_FBMODE;
}

void grDisableAllEffects( void ) {
    // reset color combine, blending, fog enable, depth function, etc...

    cc_rgbFunc = GPU_COMBINE_FUNC_LOCAL; cc_rgbFac = GPU_COMBINE_FACTOR_ONE; cc_aFunc = GPU_COMBINE_FUNC_LOCAL; cc_aFac = GPU_COMBINE_FACTOR_ONE;
    cc_rgbInvert = 0; cc_aInvert = 0;
    cc_blendSrc = GPU_BLEND_ONE; cc_blendDst = GPU_BLEND_ZERO;
    tmu0en = 1; tmu1en = 1;
    colormask = 1; depthmask = 0;
    depthfunc = GPU_COMPARE_ALWAYS;
    afunc = GPU_COMPARE_ALWAYS;
    aref = 0;
    fogEn = 0;

    SET_PXCONFIG;
    SET_FBMODE;
}

void grDitherMode( GrDitherMode_t mode ) {
    dither = mode;
    SET_DM;
}

void grFogColorValue( GrColor_t fogcolor ) {
    gpu_setreg(GPU_CHIP_FB, GPU_REG_FB_FOGCOL, fogcolor);
}

void grFogMode( GrFogMode_t mode ) {
    fogEn = mode;
    SET_PXCONFIG;
}

void grFogTable( const GrFog_t ft[] ) {
    unsigned int* ftptr = (unsigned int*)&ft[0];
    for (int i = 0; i < 16; i++) {
        gpu_setreg(GPU_CHIP_FB, GPU_REG_FB_FOGTABLE + i, ftptr[i]);
    }
}

void grGammaCorrectionValue( float value ) {
    gpu_setregf(GPU_CHIP_FB, GPU_REG_FB_GAMMA, value);
}

FxU32 grTexCalcMemRequired(
            GrLOD_t lodmin, GrLOD_t lodmax,
            GrTextureFormat_t fmt) {
    int size = 0;
    int pixelSize = bytes_per_pixel(fmt);
    for (int mip = lodmin; mip <= lodmax; mip++) {
        int dim = 256 >> mip;
        size += pixelSize * dim * dim;
    }
    return size;
}

FxU32 grTexTextureMemRequired(
            GrTexInfo *info   ) {
    return grTexCalcMemRequired(info->smallLod, info->largeLod, info->format);
}

FxU32 grTexMinAddress( void ) {
    return 0;
}

FxU32 grTexMaxAddress( void ) {
    return TMU_MEM;
}

void grTexNCCTable( GrChipID_t tmu, GrNCCTable_t table ) {
    if (tmu == GR_FBI) return;
    int tmuidx = tmu == GR_TMU1 ? 1 : 0;

    tmu_nccTbl[tmuidx] = table;
    SET_TMODE(tmuidx);
}

void grTexSource(
            GrChipID_t tmu, 
            FxU32      startAddress,
            GrTexInfo  *info ) {
    if (tmu == GR_FBI) return;
    int tmuidx = tmu == GR_TMU1 ? 1 : 0;

    tmu_baseAddr[tmuidx] = startAddress;
    tmu_minLod[tmuidx] = info->largeLod;
    tmu_maxLod[tmuidx] = info->smallLod;
    tmu_format[tmuidx] = info->format;

    SET_TLOD(tmuidx);
    SET_TMODE(tmuidx);
}

void grTexClampMode(
            GrChipID_t tmu, 
            GrTextureClampMode_t s_clampmode,
            GrTextureClampMode_t t_clampmode
            ) {
    if (tmu == GR_FBI) return;
    int tmuidx = tmu == GR_TMU1 ? 1 : 0;

    tmu_clampS[tmuidx] = s_clampmode == GR_TEXTURECLAMP_CLAMP;
    tmu_clampT[tmuidx] = t_clampmode == GR_TEXTURECLAMP_CLAMP;

    SET_TMODE(tmuidx);
}

void grTexCombine(
            GrChipID_t tmu, 
            GrCombineFunction_t rgb_function,
            GrCombineFactor_t rgb_factor, 
            GrCombineFunction_t alpha_function,
            GrCombineFactor_t alpha_factor,
            FxBool rgb_invert,
            FxBool alpha_invert
            ) {
    if (tmu == GR_FBI) return;
    int tmuidx = tmu == GR_TMU1 ? 1 : 0;

    tmu_cc_rgbFn[tmuidx] = rgb_function;
    tmu_cc_rgbFc[tmuidx] = rgb_factor;
    tmu_cc_aFn[tmuidx] = alpha_function;
    tmu_cc_aFc[tmuidx] = alpha_factor;
    tmu_cc_rgbInv[tmuidx] = rgb_invert;
    tmu_cc_aInv[tmuidx] = alpha_invert;

    SET_TMODE(tmuidx);
}

void grTexFilterMode(
            GrChipID_t            tmu, 
            GrTextureFilterMode_t minfilter_mode,
            GrTextureFilterMode_t magfilter_mode
            ) {
    if (tmu == GR_FBI) return;
    int tmuidx = tmu == GR_TMU1 ? 1 : 0;

    tmu_filter[tmuidx] = magfilter_mode == GR_TEXTUREFILTER_BILINEAR;
    
    SET_TMODE(tmuidx);
}

void grTexLodBiasValue( GrChipID_t tmu, float bias ) {
    if (tmu == GR_FBI) return;
    int tmuidx = tmu == GR_TMU1 ? 1 : 0;

    // convert bias to Q4.2
    int qbias = (int)(bias * (1 << 2));

    if (qbias < -32) qbias = -32;
    else if (qbias > 31) qbias = 31;

    tmu_bias[tmuidx] = qbias;

    SET_TLOD(tmuidx);
}

void grTexDownloadMipMap(
            GrChipID_t tmu, 
            FxU32      startAddress,
            GrTexInfo  *info ) {
    int size = grTexCalcMemRequired(info->largeLod, info->smallLod, info->format);
    if (tmu != GR_FBI) {
        gpu_writemem(tmu, startAddress, size, info->data);
    }
}

void grTexDownloadMipMapLevel(
            GrChipID_t        tmu, 
            FxU32             startAddress,
            GrLOD_t           thisLod,
            GrLOD_t           largeLod,
            GrTextureFormat_t format,
            void              *data ) {
    if (tmu == GR_FBI) return;

    // calculate offset to given LOD level in this chain
    int offset = 0;
    for (int i = largeLod; i < thisLod; i++) {
        offset += grTexCalcMemRequired(i, i, format);
    }

    // calculate size of single LOD we are updating & write
    int mipsize = grTexCalcMemRequired(thisLod, thisLod, format);
    gpu_writemem(tmu, startAddress + offset, mipsize, data);
}

void grTexDownloadMipMapLevelPartial(
            GrChipID_t        tmu, 
            FxU32             startAddress,
            GrLOD_t           thisLod,
            GrLOD_t           largeLod,
            GrTextureFormat_t format,
            void              *data,
            int               start,
            int               end ) {
    if (tmu == GR_FBI) return;

    // calculate offset to given LOD level in this chain
    int offset = 0;
    for (int i = largeLod; i < thisLod; i++) {
        offset += grTexCalcMemRequired(i, i, format);
    }

    // calculate size of a single row
    int dim = 256 >> thisLod;
    int rowsize = dim * bytes_per_pixel(format);
    int numrows = (end - start) + 1;

    int datalen = numrows * rowsize;
    offset += start * rowsize;

    // write memory
    gpu_writemem(tmu, startAddress + offset, datalen, data);
}

void grTexDownloadTable(
            GrChipID_t   tmu, 
            GrTexTable_t type, 
            void         *data ) {
    if (tmu == GR_FBI) return;

    unsigned int* dataptr = data;
    if (type == GR_TEXTABLE_NCC0) {
        for (int i = 0; i < 16; i++) {
            gpu_setreg(tmu, GPU_REG_TMU_YIQTABLE + i, dataptr[i]);
        }
    }
    else if (type == GR_TEXTABLE_NCC1) {
        for (int i = 0; i < 16; i++) {
            gpu_setreg(tmu, GPU_REG_TMU_YIQTABLE + 16 + i, dataptr[i]);
        }
    }
    else if (type == GR_TEXTABLE_PALETTE) {
        for (int i = 0; i < 256; i++) {
            gpu_setreg(tmu, GPU_REG_TMU_PALTABLE + i, dataptr[i]);
        }
    }
}

void grTexDownloadTablePartial(
            GrChipID_t   tmu, 
            GrTexTable_t type, 
            void         *data,
            int          start,
            int          end ) {
    if (tmu == GR_FBI) return;

    // partial uploads of NCC tables are not supported
    // (official Glide API had the same limitation)
    if (type != GR_TEXTABLE_PALETTE) return;

    // invalid range
    if (start < 0 || end > 256) return;

    unsigned int* dataptr = data;
    for (int i = start; i <= end; i++) {
        gpu_setreg(tmu, GPU_REG_TMU_PALTABLE + i, dataptr[i]);
    }
}