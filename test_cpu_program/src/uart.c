#include "uart.h"

void uart_puts(const char* str) {
    while (*str != 0) {
        uart_putc(*str++);
    }
}

// needed by printf implementation
void _putchar(char character) {
    uart_putc(character);
}