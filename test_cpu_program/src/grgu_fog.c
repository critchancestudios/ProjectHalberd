#include <math.h>

#include "glideutl.h"

// calculated as 2^(i/4)
static float tableIndexToW[GR_FOG_TABLE_SIZE] = 
{
    1.0f,       1.189207115002721f,     1.4142135623730951f,     1.681792830507429f,
    2.0f,       2.378414230005442f,     2.8284271247461903f,     3.363585661014858f,
    4.0f,       4.756828460010884f,     5.656854249492381f,      6.727171322029716f,
    8.0f,       9.513656920021768f,     11.313708498984761f,     13.454342644059432f,
    16.0f,      19.027313840043536f,    22.627416997969522f,     26.908685288118864f,
    32.0f,      38.05462768008707f,     45.254833995939045f,     53.81737057623773f,
    64.0f,      76.10925536017415f,     90.50966799187809f,      107.63474115247546f,
    128.0f,     152.2185107203483f,     181.01933598375618f,     215.2694823049509f,
    256.0f,     304.4370214406966f,     362.03867196751236f,     430.5389646099018f,
    512.0f,     608.8740428813932f,     724.0773439350247f,      861.0779292198037f,
    1024.0f,    1217.7480857627863f,    1448.1546878700494f,     1722.1558584396073f,
    2048.0f,    2435.4961715255727f,    2896.309375740099f,      3444.3117168792146f,
    4096.0f,    4870.992343051145f,     5792.618751480198f,      6888.623433758429f,
    8192.0f,    9741.98468610229f,      11585.237502960395f,     13777.246867516858f,
    16384.0f,   19483.96937220458f,     23170.47500592079f,      27554.493735033717f,
    32768.0f,   38967.93874440916f,     46340.95001184158f,      55108.98747006743f
};

float guFogTableIndexToW(int i) {
    if (i < 0) i = 0;
    else if (i >= GR_FOG_TABLE_SIZE) i = GR_FOG_TABLE_SIZE - 1;

    return tableIndexToW[i];
}

void guFogGenerateExp(GrFog_t fogtable[], float density) {
    float dp = density * guFogTableIndexToW(GR_FOG_TABLE_SIZE - 1);
    float scale = 1.0f / (1.0f - expf(-dp));

    for (int i = 0; i < GR_FOG_TABLE_SIZE; i++) {
        dp = density * guFogTableIndexToW(i);
        float f = (1.0f - expf(-dp)) * scale;

        if (f > 1.0f) f = 1.0f;
        else if (f < 0.0f) f = 0.0f;

        fogtable[i] = (GrFog_t)(f * 255.0f);
    }
}

void guFogGenerateExp2(GrFog_t fogtable[], float density) {
    float dp = density * guFogTableIndexToW(GR_FOG_TABLE_SIZE - 1);
    float scale = 1.0f / (1.0f - expf(-dp * dp));

    for (int i = 0; i < GR_FOG_TABLE_SIZE; i++) {
        dp = density * guFogTableIndexToW(i);
        float f = (1.0f - expf(-dp * dp)) * scale;

        if (f > 1.0f) f = 1.0f;
        else if (f < 0.0f) f = 0.0f;

        fogtable[i] = (GrFog_t)(f * 255.0f);
    }
}

void guFogGenerateLinear(GrFog_t fogtable[], float nearZ, float farZ) {
    for (int i = 0; i < GR_FOG_TABLE_SIZE; i++) {
        float world_w = tableIndexToW[i];

        float f = (world_w - nearZ) / (farZ - nearZ);
        if (f > 1.0f) f = 1.0f;
        else if (f < 0.0f) f = 0.0f;

        fogtable[i] = (GrFog_t)(f * 255.0f);
    }
}