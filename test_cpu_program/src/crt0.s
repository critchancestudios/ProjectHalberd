.text

// entry point
.global _start
_start:
    // init libc
    bl libc_malloc_init_heap
    bl libc_init_error_tables

    // execute main & exit
    bl main
    svc #0

// void sys_heapinfo(int* heapstart, int* heaplen);
.global sys_heapinfo
sys_heapinfo:
    svc #1
    bx lr

// int sys_clock();
.global sys_clock
sys_clock:
    svc #2
    bx lr

// int sys_time();
.global sys_time
sys_time:
    svc #3
    bx lr

// int gpu_getreg(int chipsel, int addr);
.global gpu_getreg
gpu_getreg:
    svc #4
    bx lr

// void gpu_setreg(int chipsel, int addr, int val);
.global gpu_setreg
gpu_setreg:
    svc #5
    bx lr

// float gpu_getregf(int chipsel, int addr);
.global gpu_getregf
gpu_getregf:
    svc #6
    bx lr

// void gpu_setregf(int chipsel, int addr, float val);
.global gpu_setregf
gpu_setregf:
    svc #7
    bx lr

// void gpu_cmd(GpuCmd_t* cmd, int size);
.global gpu_cmd
gpu_cmd:
    svc #8
    bx lr

// void gpu_flushcmd();
.global gpu_flushcmd
gpu_flushcmd:
    svc #9
    bx lr

// void gpu_readmem(int chipsel, int addr, int len, void* buffer);
.global gpu_readmem
gpu_readmem:
    svc #10
    bx lr

// void gpu_writemem(int chipsel, int addr, int len, const void* buffer);
.global gpu_writemem
gpu_writemem:
    svc #11
    bx lr

// int disc_present();
.global disc_present
disc_present:
    svc #12
    bx lr

// int disc_opentray();
.global disc_opentray
disc_opentray:
    svc #13
    bx lr

// int disc_closetray();
.global disc_closetray
disc_closetray:
    svc #14
    bx lr

// void disc_read(int lba, int len, void* buffer);
.global disc_read
disc_read:
    svc #15
    bx lr

// void uart_putc(char c);
.global uart_putc
uart_putc:
    svc #16
    bx lr

// void uart_flush();
.global uart_flush
uart_flush:
    svc #17
    bx lr

// int memc_present(int mc);
.global memc_present
memc_present:
    svc #18
    bx lr

// void memc_read(int mc, int lba, int len, void* buffer);
.global memc_read
memc_read:
    svc #19
    bx lr

// void memc_write(int mc, int lba, int len, const void* buffer);
.global memc_write
memc_write:
    svc #19
    bx lr