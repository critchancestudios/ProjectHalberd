include(FindPackageHandleStandardArgs)

find_library(UNICORN_LIBRARY NAMES unicorn)
find_path(UNICORN_INCLUDE_DIR NAMES unicorn.h)

find_package_handle_standard_args(Unicorn REQUIRED_VARS UNICORN_LIBRARY)

if (UNICORN_FOUND)
  mark_as_advanced(UNICORN_INCLUDE_DIR)
  mark_as_advanced(UNICORN_LIBRARY)
endif()

if (UNICORN_FOUND AND NOT TARGET Unicorn::Unicorn)
  add_library(Unicorn::Unicorn SHARED IMPORTED)
  set_property(TARGET Unicorn::Unicorn PROPERTY IMPORTED_LOCATION ${UNICORN_LIBRARY})
  target_include_directories(Unicorn::Unicorn INTERFACE ${UNICORN_INCLUDE_DIR})
endif()
