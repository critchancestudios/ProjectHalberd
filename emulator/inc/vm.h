#pragma once

#include "hdfs.h"

typedef struct HalberdRuntime HalberdRuntime;

HalberdRuntime *halberd_create();
void halberd_destroy(HalberdRuntime *rt);

int halberd_loadelf(HalberdRuntime *rt, void *elf);
int halberd_loadElfFromDisc(HalberdRuntime *rt, const char* path);
int halberd_start(HalberdRuntime *rt);

void halberd_loadMemcard(HalberdRuntime *rt, const char* path, int slot);
void halberd_loadDisc(HalberdRuntime *rt, const char* path);

Disc* halberd_getDisc(HalberdRuntime *rt);