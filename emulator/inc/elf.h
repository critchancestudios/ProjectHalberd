#pragma once

#include <stdint.h>
#include <unicorn/unicorn.h>

int elf_load(void *elf, void *mem, int mem_size, int stack_addr, int stack_size, uc_engine* engine, int* entryPoint, int* heapStart, int* heapLength);