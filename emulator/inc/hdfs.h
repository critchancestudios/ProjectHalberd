#pragma once

#include <stddef.h>
#include <stdint.h>

#include "stream.h"

#define DVD_SECTOR_SIZE     2048
#define DVD_MAX_SECTORS     2298496

typedef struct {
    char publisher[32];
    char gamename[32];
} DiscInfo;

typedef struct _Disc Disc;

Disc* init_disc(Stream* fs);
void destroy_disc(Disc* disc);

int disc_find_file(Disc* disc, const char* path, int* dataStart, int* dataLength);
void disc_read_sector(Disc* disc, int sector, void* buffer, size_t size);
DiscInfo disc_info(Disc* disc);

Stream* disc_open(Disc* disc, const char* filename, const char* mode);