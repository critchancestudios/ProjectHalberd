#pragma once

#include "stream.h"

void *loadfile(const char *path, size_t* len);
void *loadAllStream(Stream* stream);