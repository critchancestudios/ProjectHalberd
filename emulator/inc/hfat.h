#pragma once

#include <stream.h>

#define MC_CHECK_OK         0
#define MC_CHECK_NOFS       1
#define MC_CHECK_CORRUPT    2

#define MC_SECTOR_SIZE  512
#define META_SIZE       1600    // char[32] gamename, char[32] desc, u16[256] iconPalette, u8[32 * 32] icon

typedef struct _Memcard Memcard;

typedef struct {
    char filename[20];
    uint32_t timestamp;
    uint16_t dataStart;
    uint16_t dataLength;
    uint32_t metaOffset;
} HFAT_DtEntry;

uint32_t gen_serial();

Memcard* init_memcard(Stream* fs);
void destroy_memcard(Memcard* mc);

int memcard_valid(Memcard* mc);
int check_memcard(Memcard* mc);
void format_memcard(Memcard* mc);

int memcard_find_file(Memcard* mc, const char* filename);
void memcard_delete_file(Memcard* mc, int dirEnt);
int memcard_create_file(Memcard* mc, const char* filename, uint32_t length, uint32_t metaOffset);

void memcard_getinfo(Memcard* mc, int dirEnt, HFAT_DtEntry* info);
int memcard_next_sector(Memcard* mc, int cur_sector);
void memcard_read_sector(Memcard* mc, int sector, void* buffer, size_t size);
void memcard_write_sector(Memcard* mc, int sector, const void* buffer, size_t size);

void memcard_flush(Memcard* mc);

Stream* memcard_open(Memcard* mc, const char* filename, const char* mode);
Stream* memcard_create(Memcard* mc, const char* filename, int sectors, int metaOffset);