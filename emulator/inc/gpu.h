#pragma once

#include <stdint.h>

#include "gpuconfig.h"

#define GPU_CMD_NOP         0
#define GPU_CMD_SWAPBUF     1
#define GPU_CMD_FILLRECT    2
#define GPU_CMD_DRAWPT      3
#define GPU_CMD_DRAWLINE    4
#define GPU_CMD_DRAWTRI     5

#define GPU_CHIP_FB         0
#define GPU_CHIP_TMU0       1
#define GPU_CHIP_TMU1       2

#define GPU_REG_FB_DM           0
#define GPU_REG_FB_CLIPX1X2     1
#define GPU_REG_FB_CLIPY1Y2     2
#define GPU_REG_FB_PXCONFIG     3
#define GPU_REG_FB_MODE         4
#define GPU_REG_FB_DEPTHBIAS    5
#define GPU_REG_FB_FOGTABLE     6
#define GPU_REG_FB_CLEARCOL     22
#define GPU_REG_FB_CLEARDEPTH   23
#define GPU_REG_FB_CONSTCOL     24
#define GPU_REG_FB_FOGCOLOR     25
#define GPU_REG_FB_GAMMA        26

#define GPU_REG_TMU_TEXADDR     0
#define GPU_REG_TMU_TLOD        1
#define GPU_REG_TMU_TMODE       2
#define GPU_REG_TMU_NCCTABLE0   3
#define GPU_REG_TMU_NCCTABLE1   19
#define GPU_REG_TMU_PALTABLE    35

#define GPU_DM_320x240_VGA  0
#define GPU_DM_320x240_NTSC 1
#define GPU_DM_320x288_PAL  2
#define GPU_DM_640x480_VGA  3
#define GPU_DM_640x480_NTSC 4
#define GPU_DM_640x576_PAL  5

#define GPU_DM_DITHER       (1 << 3)

#define GPU_TEXFMT_RGB565   0
#define GPU_TEXFMT_ARGB4444 1
#define GPU_TEXFMT_YIQ422   2
#define GPU_TEXFMT_AYIQ8422 3
#define GPU_TEXFMT_P8       4
#define GPU_TEXFMT_AP88     5
#define GPU_TEXFMT_L8       6
#define GPU_TEXFMT_AL88     7

#define GPU_LOD_256         0
#define GPU_LOD_128         1
#define GPU_LOD_64          2
#define GPU_LOD_32          3
#define GPU_LOD_16          4
#define GPU_LOD_8           5
#define GPU_LOD_4           6
#define GPU_LOD_2           7
#define GPU_LOD_1           8

#define GPU_DM_GETRES(v)    (v & 7)
#define GPU_DM_GETDITHER(v) (v & 8)

#define GPU_TLOD_MIN(v)     (v & 15)
#define GPU_TLOD_MAX(v)     ((v >> 4) & 15)
#define GPU_TLOD_BIAS(v)    ((v >> 8) & 63)

#define GPU_TMODE_CCFNRGB(v)    (v & 15)
#define GPU_TMODE_CCFNA(v)      ((v >> 4) & 15)
#define GPU_TMODE_CCFCRGB(v)    ((v >> 8) & 7)
#define GPU_TMODE_CCFCA(v)      ((v >> 11) & 7)
#define GPU_TMODE_CCRGBINV(v)   ((v >> 14) & 1)
#define GPU_TMODE_CCAINV(v)     ((v >> 15) & 1)
#define GPU_TMODE_MINF(v)       ((v >> 16) & 1)
#define GPU_TMODE_MAGF(v)       ((v >> 17) & 1)
#define GPU_TMODE_CLAMPS(v)     ((v >> 18) & 1)
#define GPU_TMODE_CLAMPT(v)     ((v >> 19) & 1)
#define GPU_TMODE_TEXFMT(v)     ((v >> 20) & 7)
#define GPU_TMODE_NCCTBL(v)     ((v >> 23) & 1)

#define GPU_COMBINE_FUNC_ZERO                                       0
#define GPU_COMBINE_FUNC_LOCAL                                      1
#define GPU_COMBINE_FUNC_LOCAL_ALPHA                                2
#define GPU_COMBINE_FUNC_SCALE_OTHER                                3
#define GPU_COMBINE_FUNC_BLEND_OTHER                                COMBINE_FUNC_SCALE_OTHER
#define GPU_COMBINE_FUNC_SCALE_OTHER_ADD_LOCAL                      4
#define GPU_COMBINE_FUNC_SCALE_OTHER_ADD_LOCAL_ALPHA                5
#define GPU_COMBINE_FUNC_SCALE_OTHER_MINUS_LOCAL                    6
#define GPU_COMBINE_FUNC_SCALE_OTHER_MINUS_LOCAL_ADD_LOCAL          7
#define GPU_COMBINE_FUNC_BLEND                                      COMBINE_FUNC_SCALE_OTHER_MINUS_LOCAL_ADD_LOCAL
#define GPU_COMBINE_FUNC_SCALE_OTHER_MINUS_LOCAL_ADD_LOCAL_ALPHA    8
#define GPU_COMBINE_FUNC_SCALE_MINUS_LOCAL_ADD_LOCAL                9
#define GPU_COMBINE_FUNC_BLEND_LOCAL                                10
#define GPU_COMBINE_FUNC_SCALE_MINUS_LOCAL_ADD_LOCAL_ALPHA          11

#define GPU_COMBINE_FACTOR_ZERO                                     0
#define GPU_COMBINE_FACTOR_LOCAL                                    1
#define GPU_COMBINE_FACTOR_OTHER_ALPHA                              2
#define GPU_COMBINE_FACTOR_LOCAL_ALPHA                              3
#define GPU_COMBINE_FACTOR_ONE                                      4
#define GPU_COMBINE_FACTOR_ONE_MINUS_LOCAL                          5
#define GPU_COMBINE_FACTOR_ONE_MINUS_OTHER_ALPHA                    6
#define GPU_COMBINE_FACTOR_ONE_MINUS_LOCAL_ALPHA                    7

#define GPU_BLEND_ZERO              0
#define GPU_BLEND_ONE               1
#define GPU_BLEND_CONST_RGB         2
#define GPU_BLEND_CONST_A           3
#define GPU_BLEND_ARG0_RGB          4
#define GPU_BLEND_ARG0_A            5
#define GPU_BLEND_ARG1_RGB          6
#define GPU_BLEND_ARG1_A            7
#define GPU_BLEND_CONST_INV_RGB     8
#define GPU_BLEND_CONST_INV_A       9
#define GPU_BLEND_ARG0_INV_RGB      10
#define GPU_BLEND_ARG0_INV_A        11
#define GPU_BLEND_ARG1_INV_RGB      12
#define GPU_BLEND_ARG1_INV_A        13

#define GPU_PXCFG_CCFNRGB(v)        (v & 15)
#define GPU_PXCFG_CCFNA(v)          ((v >> 4) & 15)
#define GPU_PXCFG_CCFCRGB(v)        ((v >> 8) & 7)
#define GPU_PXCFG_CCFCA(v)          ((v >> 11) & 7)
#define GPU_PXCFG_CCRGBINV(v)       ((v >> 14) & 1)
#define GPU_PXCFG_CCAINV(v)         ((v >> 15) & 1)
#define GPU_PXCFG_BLENDSRC(v)       ((v >> 16) & 15)
#define GPU_PXCFG_BLENDDST(v)       ((v >> 20) & 15)
#define GPU_PXCFG_TMU0EN(v)         ((v >> 24) & 1)
#define GPU_PXCFG_TMU1EN(v)         ((v >> 25) & 1)
#define GPU_PXCFG_FOGEN(v)          ((v >> 26) & 1)

#define GPU_COMPARE_NEVER           0
#define GPU_COMPARE_ALWAYS          1
#define GPU_COMPARE_LESS            2
#define GPU_COMPARE_LEQUAL          3
#define GPU_COMPARE_GREATER         4
#define GPU_COMPARE_GEQUAL          5
#define GPU_COMPARE_EQUAL           6
#define GPU_COMPARE_NOTEQUAL        7

#define GPU_FB_MODE_COLORMASK(v)    (v & 1)
#define GPU_FB_MODE_DEPTHMASK(v)    (v & 2)
#define GPU_FB_MODE_DEPTHFUNC(v)    ((v >> 2) & 7)
#define GPU_FB_MODE_ALPHAFUNC(v)    ((v >> 5) & 7)
#define GPU_FB_MODE_ALPHAREF(v)     ((v >> 8) & 0xFF)

typedef struct _Gpu Gpu;

typedef struct {
    float sow;
    float tow;
} GpuTmuVertex_t;

typedef struct {
    float x;
    float y;
    float z;
    float oow;
    float r;
    float g;
    float b;
    float a;
    GpuTmuVertex_t tmu[GPU_NUM_TMU];
} GpuVertex_t;

typedef struct {
    GpuVertex_t vtx;
} GpuCmdDrawPoint_t;

typedef struct {
    GpuVertex_t vtx[2];
} GpuCmdDrawLine_t;

typedef struct {
    int sign;
    GpuVertex_t vtx[3];
} GpuCmdDrawTriangle_t;

typedef struct {
    uint32_t packet_type;
    union {
        GpuCmdDrawPoint_t drawpt;
        GpuCmdDrawLine_t drawline;
        GpuCmdDrawTriangle_t drawtri;
    } param;
} GpuCmd_t;

Gpu* gpu_create();
void gpu_destroy(Gpu* gpu);

uint32_t gpu_getreg(Gpu* gpu, int chipsel, int addr);
void gpu_setreg(Gpu* gpu, int chipsel, int addr, uint32_t val);
float gpu_getregf(Gpu* gpu, int chipsel, int addr);
void gpu_setregf(Gpu* gpu, int chipsel, int addr, float val);
void gpu_handlecmd(Gpu* gpu, GpuCmd_t* packet);
void gpu_flushcmd(Gpu* gpu);
void gpu_readmem(Gpu* gpu, int chipsel, int addr, int len, void* buffer);
void gpu_writemem(Gpu* gpu, int chipsel, int addr, int len, const void* buffer);