#pragma once

// size of a single memory page (4KiB)
#define PAGE_SIZE (1024 * 4)

// the minimum start address
// ELF files should not map any segments below this address
#define MIN_ELF_ADDR 0x10000

// max size of main ram (32MiB)
#define MEM_SIZE 32 * 1024 * 1024

// stack segment (32KiB) is allocated just before minimum address defined above
// note: avoid mapping page 0, to help catch null pointers (access will trigger segfault)
#define STACK_SIZE 32768
#define STACK_ADDR (MIN_ELF_ADDR - STACK_SIZE)