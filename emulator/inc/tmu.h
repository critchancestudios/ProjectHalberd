#ifndef _TMU_H_
#define _TMU_H_

typedef struct _Tmu Tmu;

typedef struct {
    uint8_t y[16];      // 16 possible values of Y
    int16_t a[4][3];    //  4 possible values of A
    int16_t b[4][3];    //  4 possible values of B
} NCCTable;

typedef struct {
    uint32_t rgb[256];
} PalTable;

Tmu* tmu_create();
void tmu_destroy(Tmu* tmu);

NCCTable* tmu_getncctable(Tmu* tmu, int which);
PalTable* tmu_getpaltable(Tmu* tmu);
void tmu_dirtyncctable(Tmu* tmu);
void tmu_dirtypaltable(Tmu* tmu);
void tmu_setaddr(Tmu* tmu, int baseaddr);
void tmu_setlod(Tmu* tmu, int minlod, int maxlod, float bias);
void tmu_setfmt(Tmu* tmu, int fmt);
void tmu_setclamp(Tmu* tmu, int clampS, int clampT);
void tmu_setfilter(Tmu* tmu, int minfilter, int magfilter);
void tmu_setNcc(Tmu* tmu, int ncc);
GLuint tmu_preparetex(Tmu* tmu);
void tmu_read(Tmu* tmu, void* buffer, int addr, int len);
void tmu_write(Tmu* tmu, const void* buffer, int addr, int len);

#endif