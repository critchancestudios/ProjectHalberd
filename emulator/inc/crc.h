#pragma once

#include <stdint.h>
#include <stddef.h>

uint32_t crc32(uint32_t crc, const unsigned char* buf, size_t len);