#pragma once

#include <stdint.h>
#include <stddef.h>

typedef size_t (*stream_read_fn)(void* buffer, size_t size, size_t count, void* context);
typedef size_t (*stream_write_fn)(const void* buffer, size_t size, size_t count, void* context);
typedef void (*stream_flush_fn)(void* context);
typedef int (*stream_seek_fn)(void* context, long offset, int origin);
typedef long (*stream_tell_fn)(void* context);
typedef void (*stream_close_fn)(void* context);

typedef struct _stream Stream;

Stream* stream_create(void* context, stream_read_fn read, stream_write_fn write, stream_flush_fn flush, stream_seek_fn seek, stream_tell_fn tell, stream_close_fn close);
void stream_destroy(Stream* stream);

int stream_lock(Stream* stream);
int stream_unlock(Stream* stream);

size_t stream_read(void* buffer, size_t size, size_t count, Stream* stream);
size_t stream_write(const void* buffer, size_t size, size_t count, Stream* stream);
void stream_flush(Stream* stream);
int stream_seek(Stream* stream, long offset, int origin);
long stream_tell(Stream* stream);

Stream* stream_fopen(const char* filepath, const char* mode);