#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "elf.h"
#include "mem.h"

typedef uint16_t    Elf32_Half; // Unsigned half int
typedef uint32_t    Elf32_Off;  // Unsigned offset
typedef uint32_t    Elf32_Addr; // Unsigned address
typedef uint32_t    Elf32_Word; // Unsigned int
typedef int32_t     Elf32_Sword; // Signed int

#define ELF_NIDENT  16

enum Elf_Ident {
	EI_MAG0		    = 0, // 0x7F
	EI_MAG1		    = 1, // 'E'
	EI_MAG2		    = 2, // 'L'
	EI_MAG3		    = 3, // 'F'
	EI_CLASS	    = 4, // Architecture (32/64)
	EI_DATA		    = 5, // Byte Order
	EI_VERSION	    = 6, // ELF Version
	EI_OSABI	    = 7, // OS Specific
	EI_ABIVERSION	= 8, // OS Specific
	EI_PAD		    = 9  // Padding
};

#define ELFMAG0	    0x7F // e_ident[EI_MAG0]
#define ELFMAG1	    'E'  // e_ident[EI_MAG1]
#define ELFMAG2	    'L'  // e_ident[EI_MAG2]
#define ELFMAG3	    'F'  // e_ident[EI_MAG3]
 
#define ELFDATA2LSB	(1)  // Little Endian
#define ELFCLASS32	(1)  // 32-bit Architecture

enum Elf_Type {
	ET_NONE		= 0, // Unknown Type
	ET_REL		= 1, // Relocatable File
	ET_EXEC		= 2  // Executable File
};

#define EM_ARM          (40)    // ARM Machine Type
#define EV_CURRENT	    (1)     // ELF Current Version

typedef struct {
	uint8_t		e_ident[ELF_NIDENT];
	Elf32_Half	e_type;
	Elf32_Half	e_machine;
	Elf32_Word	e_version;
	Elf32_Addr	e_entry;
	Elf32_Off	e_phoff;
	Elf32_Off	e_shoff;
	Elf32_Word	e_flags;
	Elf32_Half	e_ehsize;
	Elf32_Half	e_phentsize;
	Elf32_Half	e_phnum;
	Elf32_Half	e_shentsize;
	Elf32_Half	e_shnum;
	Elf32_Half	e_shstrndx;
} Elf32_Ehdr;

typedef struct {
	Elf32_Word	sh_name;
	Elf32_Word	sh_type;
	Elf32_Word	sh_flags;
	Elf32_Addr	sh_addr;
	Elf32_Off	sh_offset;
	Elf32_Word	sh_size;
	Elf32_Word	sh_link;
	Elf32_Word	sh_info;
	Elf32_Word	sh_addralign;
	Elf32_Word	sh_entsize;
} Elf32_Shdr;

typedef struct {
    Elf32_Word  p_type;
    Elf32_Off   p_offset;
    Elf32_Addr  p_vaddr;
    Elf32_Addr  p_paddr;
    Elf32_Word  p_filesz;
    Elf32_Word  p_memsz;
    Elf32_Word  p_flags;
    Elf32_Word  p_align;
} Elf32_Phdr;

enum PHDR_Types {
    PT_NULL         = 0,
    PT_LOAD         = 1,
    PT_DYNAMIC      = 2,
    PT_INTERP       = 3
};

enum PHDR_Flags {
    PF_X            = 1,
    PF_W            = 2,
    PF_R            = 4,
};

#define SHN_UNDEF	(0x00)      // Undefined/Not present
#define SHN_ABS     (0xfff1)    // Absolute value

enum ShT_Types {
	SHT_NULL	    = 0,   // Null section
	SHT_PROGBITS	= 1,   // Program information
	SHT_SYMTAB	    = 2,   // Symbol table
	SHT_STRTAB	    = 3,   // String table
	SHT_RELA	    = 4,   // Relocation (w/ addend)
	SHT_NOBITS	    = 8,   // Not present in file
	SHT_REL		    = 9,   // Relocation (no addend)
};
 
enum ShT_Attributes {
	SHF_WRITE	= 0x01, // Writable section
	SHF_ALLOC	= 0x02, // Exists in memory
	SHF_EXEC	= 0x02  // Contains executable instructions
};

typedef struct {
	Elf32_Word		st_name;
	Elf32_Addr		st_value;
	Elf32_Word		st_size;
	uint8_t			st_info;
	uint8_t			st_other;
	Elf32_Half		st_shndx;
} Elf32_Sym;

typedef struct {
    void *base;
    void *next;
    int remaining;
} Arena;

static int
elf_check_file(Elf32_Ehdr *hdr)
{
    if (!hdr) return 0;

    if (hdr->e_ident[EI_MAG0] != ELFMAG0 || hdr->e_ident[EI_MAG1] != ELFMAG1 ||
        hdr->e_ident[EI_MAG2] != ELFMAG2 || hdr->e_ident[EI_MAG3] != ELFMAG3) {
        printf("Bad ELF: Invalid magic\n");
        return 0;
    }

    if (hdr->e_ident[EI_CLASS] != ELFCLASS32) {
        printf("Unsupported ELF file class\n");
        return 0;
    }

    if (hdr->e_ident[EI_DATA] != ELFDATA2LSB) {
        printf("Unsupported ELF byte order\n");
        return 0;
    }

    if (hdr->e_machine != EM_ARM) {
        printf("Unsupported ELF machine type\n");
        return 0;
    }

    if (hdr->e_ident[EI_VERSION] != EV_CURRENT) {
        printf("Unsupported ELF version\n");
        return 0;
    }

    if (hdr->e_type != ET_REL && hdr->e_type != ET_EXEC) {
        printf("Unsupported ELF file type\n");
        return 0;
    }

    return 1;
}

static Elf32_Shdr *elf_sheader(Elf32_Ehdr *hdr) {
    void *ptr = hdr;
    ptr += hdr->e_shoff;
    return (Elf32_Shdr *)ptr;
}

static Elf32_Phdr *elf_pheader(Elf32_Ehdr *hdr) {
    void *ptr = hdr;
    ptr += hdr->e_phoff;
    return (Elf32_Phdr *)ptr;
}
 
static Elf32_Shdr *elf_section(Elf32_Ehdr *hdr, int idx) {
	return &elf_sheader(hdr)[idx];
}

static char *elf_str_table(Elf32_Ehdr *hdr) {
	if(hdr->e_shstrndx == SHN_UNDEF) return NULL;
	return (char *)hdr + elf_section(hdr, hdr->e_shstrndx)->sh_offset;
}
 
static char *elf_lookup_string(Elf32_Ehdr *hdr, int offset) {
	char *strtab = elf_str_table(hdr);
	if(strtab == NULL) return NULL;
	return strtab + offset;
}

static void arena_align(Arena* arena, int align) {
    if (align <= 1) return;

    int offs = arena->remaining % align;
    arena->next += offs;
    arena->remaining -= offs;

    assert((arena->remaining % align) == 0);
}

static int round_down(int number, int multiple) {
    if (multiple == 0) return number;

    int remainder = number % multiple;
    if (remainder == 0) return number;

    return number - remainder;
}

static int round_up(int number, int multiple) {
    if (multiple == 0) return number;

    int remainder = number % multiple;
    if (remainder == 0) return number;

    return number + (multiple - remainder);
}

static void* arena_alloc(Arena* arena, int size) {
    // round up to the next page size multiple (allocations must be multiples of page size)
    size = round_up(size, PAGE_SIZE);

    if (arena->remaining < size) {
        return NULL;
    }

    void *ptr = arena->next;
    arena->next += size;
    arena->remaining -= size;

    return ptr;
}

static int elf_load_segments(Elf32_Ehdr *elf_header, Arena* arena, uc_engine* engine, int* heapstart) {
    // iterate program tables and allocate segments
    Elf32_Phdr *phdr = elf_pheader(elf_header);

    *heapstart = MIN_ELF_ADDR;

    for (int i = 0; i < elf_header->e_phnum; i++) {
        Elf32_Phdr *pr = &phdr[i];

        if (pr->p_type == PT_LOAD) {
            if (pr->p_vaddr < MIN_ELF_ADDR) {
                printf("Failed mapping segment (cannot map segments below %x)\n", MIN_ELF_ADDR);
                return 0;
            }

            int size = round_up(pr->p_memsz + pr->p_offset, PAGE_SIZE);

            // allocate memory for segment
            arena_align(arena, pr->p_align);
            void *mem = arena_alloc(arena, size);

            if (mem == NULL) {
                printf("\tFailed allocating memory for segment\n");
                return 0;
            }

            // segments need to be page-aligned, but vaddr may not be
            int map_addr = pr->p_vaddr - pr->p_offset;

            void *src = ((void *)elf_header) + pr->p_offset;
            memcpy(mem + pr->p_offset, src, pr->p_filesz);

            if (pr->p_filesz < pr->p_memsz) {
                memset(mem + pr->p_offset + pr->p_filesz, 0, pr->p_memsz - pr->p_filesz);
            }

            uc_prot perm = UC_PROT_NONE;

            if (pr->p_flags & PF_X) {
                perm |= UC_PROT_EXEC;
            }

            if (pr->p_flags & PF_R) {
                perm |= UC_PROT_READ;
            }

            if (pr->p_flags & PF_W) {
                perm |= UC_PROT_WRITE;
            }

            int map_end = map_addr + size;
            if (map_end > *heapstart) {
                *heapstart = map_end;
            }

            // map memory segment
            int err = uc_mem_map_ptr(engine, map_addr, size, perm, mem);
            if (err != UC_ERR_OK) {
                printf("Failed mapping memory segment\n");
                return 0;
            }
        }
    }

    return 1;
}

static int elf_get_entry(Elf32_Ehdr* elf_header) {
    for (int i = 0; i < elf_header->e_shnum; i++) {
        Elf32_Shdr *sec = elf_section(elf_header, i);

        if (sec->sh_type == SHT_SYMTAB) {
            // iterate symbol table, look for _start entry point
            int entries = sec->sh_size / sec->sh_entsize;

            void *symptr = ((void *)elf_header) + sec->sh_offset;
            Elf32_Sym *table = (Elf32_Sym *)symptr;

            for (int j = 1; j < entries; j++) {
                Elf32_Sym *symbol = &table[j];
                Elf32_Shdr *strtab = elf_section(elf_header, sec->sh_link);
                const char *name = (const char *)elf_header + strtab->sh_offset + symbol->st_name;

                if (strcmp(name, "_start") == 0) {
                    return symbol->st_value;
                }
            }
        }
    }

    return -1;
}

int elf_load(void *elf_data, void *dst_mem, int dst_mem_size, int stack_addr, int stack_size, uc_engine* engine, int* entryPoint, int* heapStart, int* heaplen) {
    Elf32_Ehdr *elf_header = (Elf32_Ehdr *)elf_data;
    if (!elf_check_file(elf_header)) {
        return 0;
    }

    Arena arena = {
        .base = dst_mem,
        .next = dst_mem,
        .remaining = dst_mem_size
    };

    if (!elf_load_segments(elf_header, &arena, engine, heapStart)) {
        return 0;
    }

    // allocate stack memory
    arena_align(&arena, 4);
    void *stackptr = arena_alloc(&arena, stack_size);

    if (stackptr == NULL) {
        printf("Failed allocating stack\n");
        return 0;
    }

    // map stack segment
    if (uc_mem_map_ptr(engine, stack_addr, stack_size, UC_PROT_READ | UC_PROT_WRITE, stackptr) != UC_ERR_OK) {
        printf("Failed mapping stack segment\n");
        return 0;
    }

    // finally: find maximum allocated segment & map remaining memory after it as "heap"
    printf("Mapping heap at %x (%d MiB)\n", *heapStart, arena.remaining / (1024 * 1024));
    *heaplen = arena.remaining;
    if (uc_mem_map_ptr(engine, *heapStart, arena.remaining, UC_PROT_READ | UC_PROT_WRITE, arena.next) != UC_ERR_OK) {
        printf("Failed mapping heap memory\n");
        return 0;
    }

    // set stack pointer
    int sp = stack_addr + stack_size;
    uc_reg_write(engine, UC_ARM_REG_SP, &sp);

    // get entry point
    *entryPoint = elf_get_entry(elf_header);

    if (*entryPoint == -1) {
        printf("Could not find entry point _start\n");
        return 0;
    }

    return 1;
}