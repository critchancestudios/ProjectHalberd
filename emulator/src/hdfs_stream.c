#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

#include "hdfs.h"

typedef struct {
    Disc* disc;
    int dtStart;
    int dtLen;
    int curSec;
    int pos;
} DiscStreamCtx;

static size_t disc_stream_read(void* buffer, size_t size, size_t count, void* context) {
    DiscStreamCtx* ctx = (DiscStreamCtx*)context;

    if (ctx->curSec == -1) {
        return 0;
    }

    uint8_t secbuff[DVD_SECTOR_SIZE];

    int total_bytes = size * count;
    int read_bytes = 0;

    int sec_end = ctx->dtStart + ctx->dtLen;

    while (total_bytes > 0) {
        int pos_in_sector = ctx->pos % DVD_SECTOR_SIZE;
        int remaining_in_sector = DVD_SECTOR_SIZE - pos_in_sector;
        int copy = remaining_in_sector < total_bytes ? remaining_in_sector : total_bytes;

        disc_read_sector(ctx->disc, ctx->curSec, secbuff, DVD_SECTOR_SIZE);
        memcpy(buffer, &secbuff[pos_in_sector], copy);

        buffer += copy;
        ctx->pos += copy;
        read_bytes += copy;
        total_bytes -= copy;

        ctx->curSec++;
        if (ctx->curSec >= sec_end) break;
    }

    return read_bytes / size;
}

static size_t disc_stream_write(const void* buffer, size_t size, size_t count, void* context) {
    errno = EPERM;
    return 0;
}

static void disc_stream_flush(void* context) {
}

static int disc_stream_seek(void* context, long offset, int origin) {
    DiscStreamCtx* ctx = (DiscStreamCtx*)context;

    if (origin == SEEK_SET) {
        ctx->pos = offset;
    }
    else if (origin == SEEK_CUR) {
        ctx->pos += offset;
    }
    else if (origin == SEEK_END) {
        ctx->pos = (ctx->dtLen * DVD_SECTOR_SIZE) + offset;
    }
    else {
        errno = EINVAL;
        return -1;
    }

    int secnum = ctx->pos / DVD_SECTOR_SIZE;

    if (secnum < 0) {
        errno = EINVAL;
        return -1;
    }
    else if (secnum >= ctx->dtLen) {
        // seeking beyond end of stream is technically supported, but reads and writes will fail
        secnum = -1;
        return 0;
    }

    // compute new sector
    ctx->curSec = ctx->dtStart + secnum;

    return 0;
}

static long disc_stream_tell(void* context) {
    DiscStreamCtx* ctx = (DiscStreamCtx*)context;
    return ctx->pos;
}

static void disc_stream_close(void* context) {
    free(context);
}

Stream* disc_open(Disc* disc, const char* filename, const char* mode) {
    // r and rb are supported modes

    if (strcmp(mode, "r") && strcmp(mode, "rb")) {
        errno = EINVAL;
        return NULL;
    }

    // try and locate file on disc
    int dtStart, dtLen;
    int ent = disc_find_file(disc, filename, &dtStart, &dtLen);
    if (ent == -1) {
        errno = ENOENT;
        return NULL;
    }

    // found!

    // init context
    DiscStreamCtx* ctx = malloc(sizeof(DiscStreamCtx));
    if (ctx == NULL) {
        errno = ENOMEM;
        return NULL;
    }

    ctx->disc = disc;
    ctx->dtStart = dtStart;
    ctx->dtLen = dtLen;
    ctx->curSec = dtStart;
    ctx->pos = 0;

    // create stream
    Stream* stream = stream_create(ctx,
        disc_stream_read,
        disc_stream_write,
        disc_stream_flush,
        disc_stream_seek,
        disc_stream_tell,
        disc_stream_close);

    if (stream == NULL) {
        free(ctx);
        errno = ENOMEM;
        return NULL;
    }

    return stream;
}