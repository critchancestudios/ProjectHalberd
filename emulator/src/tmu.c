#include <stdint.h>
#include <stddef.h>
#include <string.h>

#include <GL/gl.h>

#include "tmu.h"
#include "gpu.h"
#include "gpuconfig.h"

#define TEXCACHE_LEN 256

typedef struct _TexCacheEntry TexCacheEntry;

struct _TexCacheEntry {
    int allocated;
    int dirty;
    int fmt;
    int minlod;
    int maxlod;
    uint32_t addr;
    GLuint handle;
};

struct _Tmu {
    uint8_t mem[GPU_TMU_MEM];
    TexCacheEntry tcache[TEXCACHE_LEN];
    TexCacheEntry* activeTex;
    int baseAddr;
    int minLod;
    int maxLod;
    int fmt;
    float bias;
    int clampS;
    int clampT;
    int nccTable;
    int minFilter;
    int magFilter;
    NCCTable ncc0;
    NCCTable ncc1;
    PalTable pal;
    uint32_t convBuf[256 * 256];
};

static uint32_t hash(uint32_t x) {
    x = ((x >> 16) ^ x) * 0x45d9f3b;
    x = ((x >> 16) ^ x) * 0x45d9f3b;
    x = (x >> 16) ^ x;
    return x;
}

static TexCacheEntry* get_tex(Tmu* tmu, int addr, int fmt, int minlod, int maxlod) {
    TexCacheEntry* entry = &tmu->tcache[hash(addr)];
    if (entry->addr != addr || entry->fmt != fmt || entry->minlod != minlod || entry->maxlod != maxlod) {
        if (entry->allocated) {
            glDeleteTextures(1, &entry->handle);
            entry->allocated = 0;
        }
        entry->addr = addr;
        entry->fmt = fmt;
        entry->minlod = minlod;
        entry->maxlod = maxlod;
        entry->dirty = 1;
    }

    return entry;
}

static int bytes_per_pixel(int fmt) {
    switch (fmt) {
        case GPU_TEXFMT_RGB565: return 2;
        case GPU_TEXFMT_ARGB4444: return 2;
        case GPU_TEXFMT_YIQ422: return 1;
        case GPU_TEXFMT_AYIQ8422: return 2;
        case GPU_TEXFMT_P8: return 1;
        case GPU_TEXFMT_AP88: return 2;
        case GPU_TEXFMT_L8: return 1;
        case GPU_TEXFMT_AL88: return 2;
    }

    // invalid texture format
    return -1;
}

static int mipsize(int fmt, int lod) {
    int pixelsize = bytes_per_pixel(fmt);
    int dim = 256 >> lod;
    return dim * dim * pixelsize;
}

static int calcsize(int fmt, int minlod, int maxlod) {
    // note: aspect ratio setting not supported (square textures only)
    int pixelsize = bytes_per_pixel(fmt);

    int size = 0;
    for (int i = minlod; i <= maxlod; i++) {
        int dim = 256 >> i;
        size += dim * dim * pixelsize;
    }

    return size;
}

static void convert_yiq422_to_rgba(NCCTable* ncc, uint8_t* src, uint32_t* dst, int num) {
    for (int i = 0; i < num; i++) {
        uint8_t px = src[i];
        uint8_t y = (px >> 4) & 0xF;
        uint8_t i = (px >> 2) & 0x3;
        uint8_t q = px & 0x3;

        y = ncc->y[y];

        int ar = ncc->a[i][0];
        int ag = ncc->a[i][1];
        int ab = ncc->a[i][2];

        int br = ncc->b[q][0];
        int bg = ncc->b[q][1];
        int bb = ncc->b[q][2];

        int r = y + ar + br;
        int g = y + ag + bg;
        int b = y + ab + bb;

        r = (r < 0 ? 0 : (r > 255 ? 255 : r));
        g = (g < 0 ? 0 : (g > 255 ? 255 : g));
        b = (b < 0 ? 0 : (b > 255 ? 255 : b));

        dst[i] = b | (g << 8) | (r << 16) | (0xFF << 24);
    }
}

static void convert_ayiq8422_to_rgba(NCCTable* ncc, uint16_t* src, uint32_t* dst, int num) {
    for (int i = 0; i < num; i++) {
        uint16_t px = src[i];
        uint16_t a = (px >> 8) & 0xFF;
        uint16_t y = (px >> 4) & 0xF;
        uint16_t i = (px >> 2) & 0x3;
        uint16_t q = px & 0x3;

        y = ncc->y[y];

        int ar = ncc->a[i][0];
        int ag = ncc->a[i][1];
        int ab = ncc->a[i][2];

        int br = ncc->b[q][0];
        int bg = ncc->b[q][1];
        int bb = ncc->b[q][2];

        int r = y + ar + br;
        int g = y + ag + bg;
        int b = y + ab + bb;

        r = (r < 0 ? 0 : (r > 255 ? 255 : r));
        g = (g < 0 ? 0 : (g > 255 ? 255 : g));
        b = (b < 0 ? 0 : (b > 255 ? 255 : b));

        dst[i] = b | (g << 8) | (r << 16) | (a << 24);
    }
}

static void convert_p8_to_rgba(PalTable* pal, uint8_t* src, uint32_t* dst, int num) {
    for (int i = 0; i < num; i++) {
        uint8_t wtf = src[i];
        dst[i] = pal->rgb[src[i]] | 0xFF000000;
    }
}

static void convert_ap88_to_rgba(PalTable* pal, uint16_t* src, uint32_t* dst, int num) {
    for (int i = 0; i < num; i++) {
        uint8_t p = src[i] & 0xFF;
        uint8_t a = src[i] >> 8;
        dst[i] = (pal->rgb[p] & 0x00FFFFFF) | (a << 24);
    }
}

static void convert_l8_to_rgba(PalTable* pal, uint8_t* src, uint32_t* dst, int num) {
    for (int i = 0; i < num; i++) {
        uint32_t l = src[i];
        dst[i] = l | (l << 8) | (l << 16) | 0xFF000000;
    }
}

static void convert_al88_to_rgba(PalTable* pal, uint16_t* src, uint32_t* dst, int num) {
    for (int i = 0; i < num; i++) {
        uint32_t l = src[i] & 0xFF;
        uint32_t a = (src[i] >> 8) & 0xFF;
        dst[i] = l | (l << 8) | (l << 16) | (a << 24);
    }
}

static void upload_tex_direct(Tmu* tmu, TexCacheEntry* entry, GLenum format, GLenum type, int pxSize) {
    int dim = 256 >> entry->minlod;
    int lodcount = (entry->maxlod - entry->minlod) + 1;

    void* ptr = &tmu->mem[entry->addr];

    for (int i = 0; i < lodcount; i++) {
        int mipdim = dim >> i;
        int pxcount = mipdim * mipdim;

        glTexSubImage2D(GL_TEXTURE_2D, i, 0, 0, mipdim, mipdim, format, type, ptr);
        ptr += pxcount * pxSize;
    }
}

static void upload_tex_yiq422(Tmu* tmu, TexCacheEntry* entry) {
    int dim = 256 >> entry->minlod;
    int lodcount = (entry->maxlod - entry->minlod) + 1;

    NCCTable* table = tmu->nccTable ? &tmu->ncc1 : &tmu->ncc0;
    void* ptr = &tmu->mem[entry->addr];

    uint8_t* srcptr = ptr;

    for (int i = 0; i < lodcount; i++) {
        int mipdim = dim >> i;
        int pxcount = mipdim * mipdim;

        convert_yiq422_to_rgba(table, srcptr, tmu->convBuf, pxcount);
        glTexSubImage2D(GL_TEXTURE_2D, i, 0, 0, mipdim, mipdim, GL_RGBA, GL_UNSIGNED_BYTE, tmu->convBuf);

        srcptr += pxcount;
    }
}

static void upload_tex_ayiq8422(Tmu* tmu, TexCacheEntry* entry) {
    int dim = 256 >> entry->minlod;
    int lodcount = (entry->maxlod - entry->minlod) + 1;

    NCCTable* table = tmu->nccTable ? &tmu->ncc1 : &tmu->ncc0;
    void* ptr = &tmu->mem[entry->addr];

    uint16_t* srcptr = ptr;

    for (int i = 0; i < lodcount; i++) {
        int mipdim = dim >> i;
        int pxcount = mipdim * mipdim;

        convert_ayiq8422_to_rgba(table, srcptr, tmu->convBuf, pxcount);
        glTexSubImage2D(GL_TEXTURE_2D, i, 0, 0, mipdim, mipdim, GL_RGBA, GL_UNSIGNED_BYTE, tmu->convBuf);

        srcptr += pxcount;
    }
}

static void upload_tex_p8(Tmu* tmu, TexCacheEntry* entry) {
    int dim = 256 >> entry->minlod;
    int lodcount = (entry->maxlod - entry->minlod) + 1;

    PalTable* table = &tmu->pal;
    void* ptr = &tmu->mem[entry->addr];

    uint8_t* srcptr = ptr;

    for (int i = 0; i < lodcount; i++) {
        int mipdim = dim >> i;
        int pxcount = mipdim * mipdim;

        convert_p8_to_rgba(table, srcptr, tmu->convBuf, pxcount);
        glTexSubImage2D(GL_TEXTURE_2D, i, 0, 0, mipdim, mipdim, GL_RGBA, GL_UNSIGNED_BYTE, tmu->convBuf);

        srcptr += pxcount;
    }
}

static void upload_tex_ap88(Tmu* tmu, TexCacheEntry* entry) {
    int dim = 256 >> entry->minlod;
    int lodcount = (entry->maxlod - entry->minlod) + 1;

    PalTable* table = &tmu->pal;
    void* ptr = &tmu->mem[entry->addr];

    uint16_t* srcptr = ptr;

    for (int i = 0; i < lodcount; i++) {
        int mipdim = dim >> i;
        int pxcount = mipdim * mipdim;

        convert_ap88_to_rgba(table, srcptr, tmu->convBuf, pxcount);
        glTexSubImage2D(GL_TEXTURE_2D, i, 0, 0, mipdim, mipdim, GL_RGBA, GL_UNSIGNED_BYTE, tmu->convBuf);

        srcptr += pxcount;
    }
}

static void upload_tex_l8(Tmu* tmu, TexCacheEntry* entry) {
    int dim = 256 >> entry->minlod;
    int lodcount = (entry->maxlod - entry->minlod) + 1;

    PalTable* table = &tmu->pal;
    void* ptr = &tmu->mem[entry->addr];

    uint8_t* srcptr = ptr;

    for (int i = 0; i < lodcount; i++) {
        int mipdim = dim >> i;
        int pxcount = mipdim * mipdim;

        convert_l8_to_rgba(table, srcptr, tmu->convBuf, pxcount);
        glTexSubImage2D(GL_TEXTURE_2D, i, 0, 0, mipdim, mipdim, GL_RGBA, GL_UNSIGNED_BYTE, tmu->convBuf);

        srcptr += pxcount;
    }
}

static void upload_tex_al88(Tmu* tmu, TexCacheEntry* entry) {
    int dim = 256 >> entry->minlod;
    int lodcount = (entry->maxlod - entry->minlod) + 1;

    PalTable* table = &tmu->pal;
    void* ptr = &tmu->mem[entry->addr];

    uint16_t* srcptr = ptr;

    for (int i = 0; i < lodcount; i++) {
        int mipdim = dim >> i;
        int pxcount = mipdim * mipdim;

        convert_al88_to_rgba(table, srcptr, tmu->convBuf, pxcount);
        glTexSubImage2D(GL_TEXTURE_2D, i, 0, 0, mipdim, mipdim, GL_RGBA, GL_UNSIGNED_BYTE, tmu->convBuf);

        srcptr += pxcount;
    }
}

static void upload_tex(Tmu* tmu, TexCacheEntry* entry) {
    int dim = 256 >> entry->minlod;
    int lodcount = (entry->maxlod - entry->minlod) + 1;

    NCCTable* table = tmu->nccTable ? &tmu->ncc1 : &tmu->ncc0;
    void* ptr = &tmu->mem[entry->addr];

    GLenum internalFormat, format, type;

    switch(entry->fmt) {
        case GPU_TEXFMT_RGB565: {
            internalFormat = GL_RGB;
            format = GL_RGB;
            type = GL_UNSIGNED_SHORT_5_6_5;
            break;
        }
        case GPU_TEXFMT_ARGB4444: {
            internalFormat = GL_RGBA;
            format = GL_RGBA;
            type = GL_UNSIGNED_SHORT_4_4_4_4;
            break;
        }
        // converted to RGBA
        case GPU_TEXFMT_YIQ422: {
            internalFormat = GL_RGBA8;
            format = GL_RGBA;
            type = GL_UNSIGNED_BYTE;
            break;
        }
        // converted to RGBA
        case GPU_TEXFMT_AYIQ8422: {
            internalFormat = GL_RGBA8;
            format = GL_RGBA;
            type = GL_UNSIGNED_BYTE;
            break;
        }
        // converted to RGBA
        case GPU_TEXFMT_P8: {
            internalFormat = GL_RGBA8;
            format = GL_RGBA;
            type = GL_UNSIGNED_BYTE;
            break;
        }
        // converted to RGBA
        case GPU_TEXFMT_AP88: {
            internalFormat = GL_RGBA8;
            format = GL_RGBA;
            type = GL_UNSIGNED_BYTE;
            break;
        }
        // converted to RGBA
        // these two *should* be natively supported, but GL_LUMINANCE seems broken at least on my laptop w/ NVidia. driver bug?
        case GPU_TEXFMT_L8: {
            internalFormat = GL_RGBA8;
            format = GL_RGBA;
            type = GL_UNSIGNED_BYTE;
            break;
        }
        case GPU_TEXFMT_AL88: {
            internalFormat = GL_RGBA8;
            format = GL_RGBA;
            type = GL_UNSIGNED_BYTE;
            break;
        }
    }

    if (!entry->allocated) {
        entry->allocated = 1;
        glGenTextures(1, &entry->handle);
        glBindTexture(GL_TEXTURE_2D, entry->handle);
        for (int i = 0; i < lodcount; i++) {
            glTexImage2D(GL_TEXTURE_2D, i, internalFormat, dim >> i, dim >> i, 0, format, type, NULL);
        }
    }

    glBindTexture(GL_TEXTURE_2D, entry->handle);

    switch(entry->fmt) {
        case GPU_TEXFMT_RGB565: {
            upload_tex_direct(tmu, entry, format, type, 2);
            break;
        }
        case GPU_TEXFMT_ARGB4444: {
            upload_tex_direct(tmu, entry, format, type, 2);
            break;
        }
        case GPU_TEXFMT_YIQ422: {
            upload_tex_yiq422(tmu, entry);
            break;
        }
        case GPU_TEXFMT_AYIQ8422: {
            upload_tex_ayiq8422(tmu, entry);
            break;
        }
        case GPU_TEXFMT_P8: {
            upload_tex_p8(tmu, entry);
            break;
        }
        case GPU_TEXFMT_AP88: {
            upload_tex_ap88(tmu, entry);
            break;
        }
        case GPU_TEXFMT_L8: {
            upload_tex_l8(tmu, entry);
            break;
        }
        case GPU_TEXFMT_AL88: {
            upload_tex_al88(tmu, entry);
            break;
        }
    }
}

Tmu* tmu_create(int mem) {
    Tmu* tmu = malloc(sizeof(Tmu));
    if (tmu == NULL) {
        return NULL;
    }

    memset(tmu, 0, sizeof(Tmu));

    return tmu;
}

void tmu_destroy(Tmu* tmu) {
    for (int i = 0; i < TEXCACHE_LEN; i++) {
        if (tmu->tcache[i].allocated) {
            glDeleteTextures(1, &tmu->tcache[i].handle);
        }
    }
    free(tmu);
}

NCCTable* tmu_getncctable(Tmu* tmu, int which) {
    if (which == 0) return &tmu->ncc0;
    else if (which == 1) return &tmu->ncc1;

    return NULL;
}

PalTable* tmu_getpaltable(Tmu* tmu) {
    return &tmu->pal;
}

void tmu_dirtyncctable(Tmu* tmu) {
    // any NCC textures are marked dirty
    for (int i = 0; i < TEXCACHE_LEN; i++) {
        if (tmu->tcache[i].allocated && (tmu->tcache[i].fmt == GPU_TEXFMT_YIQ422 || tmu->tcache[i].fmt == GPU_TEXFMT_AYIQ8422)) {
            tmu->tcache[i].dirty = 1;
        }
    }
}

void tmu_dirtypaltable(Tmu* tmu) {
    // any paletted textures are marked dirty
    for (int i = 0; i < TEXCACHE_LEN; i++) {
        if (tmu->tcache[i].allocated && (tmu->tcache[i].fmt == GPU_TEXFMT_P8 || tmu->tcache[i].fmt == GPU_TEXFMT_AP88)) {
            tmu->tcache[i].dirty = 1;
        }
    }
}

GLuint tmu_preparetex(Tmu* tmu) {
    int texsize = calcsize(tmu->fmt, tmu->minLod, tmu->maxLod);

    TexCacheEntry* entry = get_tex(tmu, tmu->baseAddr, tmu->fmt, tmu->minLod, tmu->maxLod);
    if (entry->dirty) {
        entry->dirty = 0;
        upload_tex(tmu, entry);
    }
    tmu->activeTex = entry;

    glBindTexture(GL_TEXTURE_2D, entry->handle);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, tmu->clampS ? GL_CLAMP_TO_EDGE : GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, tmu->clampT ? GL_CLAMP_TO_EDGE : GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, tmu->minFilter ? GL_LINEAR : GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, tmu->magFilter ? GL_LINEAR : GL_NEAREST);

    return entry->handle;
}

void tmu_read(Tmu* tmu, void* buffer, int addr, int len) {
    memcpy(buffer, &tmu->mem[addr], len);
}

void tmu_write(Tmu* tmu, const void* buffer, int addr, int len) {
    memcpy(&tmu->mem[addr], buffer, len);

    uint8_t wtf[4];
    memcpy(wtf, buffer, 4);

    int writeend = addr + len;

    // invalidate any textures which overlap this region of memory
    for (int i = 0; i < TEXCACHE_LEN; i++) {
        if (!tmu->tcache[i].allocated || tmu->tcache[i].dirty) continue;

        int texsize = calcsize(tmu->tcache[i].fmt, tmu->tcache[i].minlod, tmu->tcache[i].maxlod);
        int texend = tmu->tcache[i].addr + texsize;
        if (addr <= texend && tmu->tcache[i].addr <= writeend) {
            tmu->tcache[i].dirty = 1;
        }
    }
}

void tmu_setaddr(Tmu* tmu, int baseaddr) {
    tmu->baseAddr = baseaddr;
}

void tmu_setlod(Tmu* tmu, int minlod, int maxlod, float bias) {
    if (minlod > 8) minlod = 8;
    if (maxlod > 8) maxlod = 8;
    if (maxlod < minlod) maxlod = minlod;

    tmu->minLod = minlod;
    tmu->maxLod = maxlod;
    tmu->bias = bias;
}

void tmu_setfmt(Tmu* tmu, int fmt) {
    tmu->fmt = fmt;
}

void tmu_setclamp(Tmu* tmu, int clampS, int clampT) {
    tmu->clampS = clampS;
    tmu->clampT = clampT;
}

void tmu_setNcc(Tmu* tmu, int ncc) {
    tmu->nccTable = ncc;
    tmu_dirtyncctable(tmu);
}

void tmu_setfilter(Tmu* tmu, int minfilter, int magfilter) {
    tmu->minFilter = minfilter;
    tmu->magFilter = magfilter;
}