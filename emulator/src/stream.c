#include <stdlib.h>
#include <stdio.h>

#include <SDL.h>

#include "stream.h"

struct _stream {
    SDL_mutex* lock;
    void* context;
    stream_read_fn read;
    stream_write_fn write;
    stream_flush_fn flush;
    stream_seek_fn seek;
    stream_tell_fn tell;
    stream_close_fn close;
};

Stream* stream_create(void* context, stream_read_fn read, stream_write_fn write, stream_flush_fn flush, stream_seek_fn seek, stream_tell_fn tell, stream_close_fn close) {
    Stream* stream = (Stream*)malloc(sizeof(Stream));
    if (stream == NULL) return NULL;

    stream->lock = SDL_CreateMutex();
    stream->context = context;
    stream->read = read;
    stream->write = write;
    stream->flush = flush;
    stream->seek = seek;
    stream->tell = tell;
    stream->close = close;

    return stream;
}

void stream_destroy(Stream* stream) {
    SDL_DestroyMutex(stream->lock);
    stream->close(stream->context);
    free(stream);
}

int stream_lock(Stream* stream) {
    SDL_LockMutex(stream->lock);
}

int stream_unlock(Stream* stream) {
    SDL_UnlockMutex(stream->lock);
}

size_t stream_read(void* buffer, size_t size, size_t count, Stream* stream) {
    return stream->read(buffer, size, count, stream->context);
}

size_t stream_write(const void* buffer, size_t size, size_t count, Stream* stream) {
    return stream->write(buffer, size, count, stream->context);
}

void stream_flush(Stream* stream) {
    stream->flush(stream->context);
}

int stream_seek(Stream* stream, long offset, int origin) {
    return stream->seek(stream->context, offset, origin);
}

long stream_tell(Stream* stream) {
    return stream->tell(stream->context);
}

static size_t stream_file_read(void* buffer, size_t size, size_t count, void* context) {
    return fread(buffer, size, count, (FILE*)context);
}

static size_t stream_file_write(const void* buffer, size_t size, size_t count, void* context) {
    return fwrite(buffer, size, count, (FILE*)context);
}

static void stream_file_flush(void* context) {
    fflush((FILE*)context);
}

static int stream_file_seek(void* context, long offset, int origin) {
    return fseek((FILE*)context, offset, origin);
}

static long stream_file_tell(void* context) {
    return ftell((FILE*)context);
}

static void stream_file_close(void* context) {
    fclose((FILE*)context);
}

Stream* stream_fopen(const char* filepath, const char* mode) {
    FILE* file = fopen(filepath, mode);
    if (file == NULL) return NULL;

    return stream_create(file, stream_file_read, stream_file_write, stream_file_flush, stream_file_seek, stream_file_tell, stream_file_close);
}