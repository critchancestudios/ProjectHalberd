#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>

#include "hdfs.h"
#include "crc.h"

#define HDFS_MAGIC              "HDFS"
#define HDFS_HDR_CRC_BYTES      72
#define DT_ENTRIES              (DVD_SECTOR_SIZE / sizeof(HDFS_DtEntry)) 

typedef struct {
    char filename[52];
    uint32_t flags;
    uint32_t dataStart;
    uint32_t dataLength;
} HDFS_DtEntry;

typedef struct {
    char magic[4];
    char pub[32];
    char game[32];
    uint32_t rootDtSize;
    uint32_t hdrChecksum;
} HDFS_Header;

struct _Disc {
    HDFS_Header header;
    Stream* stream;
};

static HDFS_DtEntry dtbuf[DT_ENTRIES];

// read data from a sector into memory
static void read_sector(Stream* fs, int sector, void* buffer, size_t size) {
    if (sector < 0) sector += INT32_MAX;
    sector %= DVD_MAX_SECTORS;

    stream_lock(fs);
    stream_seek(fs, sector * DVD_SECTOR_SIZE, SEEK_SET);
    stream_read(buffer, size, 1, fs);
    stream_unlock(fs);
}

// write data to a sector on disk
static void write_sector(Stream* fs, int sector, const void* buffer, size_t size) {
    if (sector < 0) sector += INT32_MAX;
    sector %= DVD_MAX_SECTORS;

    stream_lock(fs);
    stream_seek(fs, sector * DVD_SECTOR_SIZE, SEEK_SET);
    stream_write(buffer, size, 1, fs);
    stream_unlock(fs);
}

// search the given directory table for a file with the given name
static int scan_dt(Disc* disc, int dt, int dtlen, const char* filename, HDFS_DtEntry* info) {
    for (int i = 0; i < dtlen; i++) {
        int sector = dt + i;
        read_sector(disc->stream, sector, &dtbuf, DVD_SECTOR_SIZE);

        for (int j = 0; j < DT_ENTRIES; j++) {
            if (dtbuf[j].filename[0] == 0) {
                // hit the end of the dtable, early exit
                return -1;
            }
            else if (strncmp(dtbuf[j].filename, filename, 52) == 0) {
                // found it
                if (info != NULL) {
                    *info = dtbuf[j];
                }
                return (sector * DT_ENTRIES) + j;
            }
        }
    }

    return -1;
}

// search for the given file on disc, relative to the root directory
static int find_file(Disc* disc, const char* filepath, HDFS_DtEntry* info) {
    int cur_dt, cur_dtlen;
    int isfile = 0;

    cur_dt = 1;
    cur_dtlen = disc->header.rootDtSize;

    // dup filepath first
    char* fp = malloc(strlen(filepath));
    if (fp == NULL) return -1;
    strcpy(fp, filepath);

    // recursively search each path element
    char* tok_state = fp;
    char* tok = __strtok_r(tok_state, "/", &tok_state);

    if (tok == NULL) {
        free(fp);
        return 0;
    }

    while ((tok != NULL)) {
        if (isfile) {
            // path element is not a directory
            free(fp);
            return 0;
        }

        int ent = scan_dt(disc, cur_dt, cur_dtlen, tok, info);
        if (ent == -1) {
            // couldn't find part of the path
            free(fp);
            return 0;
        }

        isfile = (info->flags & 1) == 0;
        tok = __strtok_r(tok_state, "/", &tok_state);

        cur_dt = info->dataStart;
        cur_dtlen = info->dataLength;
    }

    free(fp);
    return 1;
}

Disc* init_disc(Stream* fs) {
    // read header sector
    HDFS_Header header;
    read_sector(fs, 0, &header, sizeof(HDFS_Header));

    // verify magic
    if (strncmp(header.magic, HDFS_MAGIC, 4)) {
        printf("Bad disc header (not a Halberd disc image)\n");
        return NULL;
    }

    // calc crc of header
    uint32_t hdr_crc = crc32(0, &header, HDFS_HDR_CRC_BYTES);

    // verify crc
    if (hdr_crc != header.hdrChecksum) {
        printf("Bad disc header (invalid checksum)\n");
        return NULL;
    }

    // valid game disc

    Disc* d = malloc(sizeof(Disc));
    if (d == NULL) {
        printf("Out of memory\n");
        return NULL;
    }

    d->stream = fs;
    d->header = header;
 
    return d;
}

void destroy_disc(Disc* disc) {
    // close stream
    stream_destroy(disc->stream);
    free(disc);
}

int disc_find_file(Disc* disc, const char* path, int* dataStart, int* dataLength) {
    HDFS_DtEntry ent;
    int ret = find_file(disc, path, &ent);
    if (dataStart != NULL) {
        *dataStart = ent.dataStart;
    }
    if (dataLength != NULL) {
        *dataLength = ent.dataLength;
    }
    return ret;
}

void disc_read_sector(Disc* disc, int sector, void* buffer, size_t size) {
    read_sector(disc->stream, sector, buffer, size);
}

DiscInfo disc_info(Disc* disc) {
    DiscInfo d;
    memcpy(&d.publisher, disc->header.pub, 32);
    memcpy(&d.gamename, disc->header.game, 32);
    return d;
}