#include <stdio.h>
#include <assert.h>

#include <unicorn/unicorn.h>
#include <SDL.h>

#include "vm.h"
#include "hdfs.h"
#include "util.h"

int main(int argc, char** argv) {
    // init SDL (we need this for GetPrefPath)
    // note that further calls to Init will simply be ignored, so let's just init everything upfront
    SDL_Init(SDL_INIT_EVERYTHING);

    char* saveLoc = SDL_GetPrefPath("CritChanceStudios", "ProjectHalberd");

    // init VM
    HalberdRuntime *rt = halberd_create();

    // mount memory cards
    if (saveLoc != NULL) {
        char pathbuf[1024];
        sprintf(pathbuf, "%s%s", saveLoc, "mca.sav");
        halberd_loadMemcard(rt, pathbuf, 0);
        sprintf(pathbuf, "%s%s", saveLoc, "mcb.sav");
        halberd_loadMemcard(rt, pathbuf, 1);
    }
    else {
        printf("Failed creating save directory (no memory cards loaded)\n");
    }

    // load disc
    halberd_loadDisc(rt, "test.iso");

    // load ELF
    size_t elflen;
    void* elf = loadfile("test.elf", &elflen);
    if (elf == NULL) {
        printf("Failed reading ELF\n");
        return -1;
    }

    if (!halberd_loadelf(rt, elf)) {
        printf("Failed loading ELF\n");
        return -1;
    }

    printf("Starting executable\n");

    int err = halberd_start(rt);
    if (err) {
        printf("Error executing: %s\n", uc_strerror(err));
        return err;
    }

    printf("Finished.\n");
    halberd_destroy(rt);

    SDL_Quit();
    return 0;
}