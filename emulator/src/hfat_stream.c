#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

#include "hfat.h"

typedef struct {
    int rdEnable;
    int wrEnable;
    Memcard* mc;
    HFAT_DtEntry ent;
    int curSec;
    int pos;
} McStreamCtx;

static size_t memcard_stream_read(void* buffer, size_t size, size_t count, void* context) {
    McStreamCtx* ctx = (McStreamCtx*)context;

    if (!ctx->rdEnable) {
        errno = EPERM;
        return 0;
    }

    if (ctx->curSec == -1) {
        return 0;
    }

    uint8_t secbuff[MC_SECTOR_SIZE];

    int total_bytes = size * count;
    int read_bytes = 0;

    while (total_bytes > 0) {
        int pos_in_sector = ctx->pos % MC_SECTOR_SIZE;
        int remaining_in_sector = MC_SECTOR_SIZE - pos_in_sector;
        int copy = remaining_in_sector < total_bytes ? remaining_in_sector : total_bytes;

        memcard_read_sector(ctx->mc, ctx->curSec, secbuff, MC_SECTOR_SIZE);
        memcpy(buffer, &secbuff[pos_in_sector], copy);

        buffer += copy;
        ctx->pos += copy;
        read_bytes += copy;
        total_bytes -= copy;

        ctx->curSec = memcard_next_sector(ctx->mc, ctx->curSec);
        if (ctx->curSec == -1) break;
    }

    return read_bytes / size;
}

static size_t memcard_stream_write(const void* buffer, size_t size, size_t count, void* context) {
    McStreamCtx* ctx = (McStreamCtx*)context;

    if (!ctx->wrEnable) {
        errno = EPERM;
        return 0;
    }

    if (ctx->curSec == -1) {
        // todo: should this be an error?
        return 0;
    }

    uint8_t secbuff[MC_SECTOR_SIZE];

    int total_bytes = size * count;
    int write_bytes = 0;

    while (total_bytes > 0) {
        int pos_in_sector = ctx->pos % MC_SECTOR_SIZE;
        int remaining_in_sector = MC_SECTOR_SIZE - pos_in_sector;
        int copy = remaining_in_sector < total_bytes ? remaining_in_sector : total_bytes;

        memcard_read_sector(ctx->mc, ctx->curSec, secbuff, MC_SECTOR_SIZE);
        memcpy(&secbuff[pos_in_sector], buffer, copy);
        memcard_write_sector(ctx->mc, ctx->curSec, secbuff, MC_SECTOR_SIZE);

        buffer += copy;
        ctx->pos += copy;
        write_bytes += copy;
        total_bytes -= copy;

        ctx->curSec = memcard_next_sector(ctx->mc, ctx->curSec);
        if (ctx->curSec == -1) break;
    }

    return write_bytes / size;
}

static void memcard_stream_flush(void* context) {
    McStreamCtx* ctx = (McStreamCtx*)context;
    memcard_flush(ctx->mc);
}

static int memcard_stream_seek(void* context, long offset, int origin) {
    McStreamCtx* ctx = (McStreamCtx*)context;

    if (origin == SEEK_SET) {
        ctx->pos = offset;
    }
    else if (origin == SEEK_CUR) {
        ctx->pos += offset;
    }
    else if (origin == SEEK_END) {
        ctx->pos = (ctx->ent.dataLength * MC_SECTOR_SIZE) + offset;
    }
    else {
        errno = EINVAL;
        return -1;
    }

    int secnum = ctx->pos / MC_SECTOR_SIZE;

    if (secnum < 0) {
        errno = EINVAL;
        return -1;
    }
    else if (secnum >= ctx->ent.dataLength) {
        // seeking beyond end of stream is technically supported, but reads and writes will fail
        secnum = -1;
        return 0;
    }

    // compute new sector
    ctx->curSec = ctx->ent.dataStart;
    for (int i = 0; i < secnum; i++) {
        ctx->curSec = memcard_next_sector(ctx->mc, ctx->curSec);
        if (ctx->curSec == -1) break;
    }

    return 0;
}

static long memcard_stream_tell(void* context) {
    McStreamCtx* ctx = (McStreamCtx*)context;
    return ctx->pos;
}

static void memcard_stream_close(void* context) {
    free(context);
}

Stream* memcard_open(Memcard* mc, const char* filename, const char* mode) {
    // r, rb, w, and wb are supported modes
    // however, note that writing a file that hasn't been allocated is not supported

    int rdEnable = 0;
    int wrEnable = 0;

    if (strcmp(mode, "r") == 0 || strcmp(mode, "rb") == 0) {
        rdEnable = 1;
    }
    else if (strcmp(mode, "w") == 0 || strcmp(mode, "wb") == 0) {
        wrEnable = 1;
    }
    else {
        errno = EINVAL;
        return NULL;
    }

    // try and locate file on memory card
    int ent = memcard_find_file(mc, filename);
    if (ent == -1) {
        errno = ENOENT;
        return NULL;
    }

    // found!

    // init context
    McStreamCtx* ctx = malloc(sizeof(McStreamCtx));
    if (ctx == NULL) {
        errno = ENOMEM;
        return NULL;
    }

    memcard_getinfo(mc, ent, &ctx->ent);
    ctx->rdEnable = rdEnable;
    ctx->wrEnable = wrEnable;
    ctx->mc = mc;
    ctx->curSec = ctx->ent.dataStart;
    ctx->pos = 0;

    // create stream
    Stream* stream = stream_create(ctx,
        memcard_stream_read,
        memcard_stream_write,
        memcard_stream_flush,
        memcard_stream_seek,
        memcard_stream_tell,
        memcard_stream_close);

    if (stream == NULL) {
        free(ctx);
        errno = ENOMEM;
        return NULL;
    }

    return stream;
}

Stream* memcard_create(Memcard* mc, const char* filename, int sectors, int metaOffset) {
    // try and allocate memory card file
    int ent = memcard_create_file(mc, filename, sectors, metaOffset);
    if (ent == -1) {
        return NULL;
    }

    // init context
    McStreamCtx* ctx = malloc(sizeof(McStreamCtx));
    if (ctx == NULL) {
        errno = ENOMEM;
        return NULL;
    }

    memcard_getinfo(mc, ent, &ctx->ent);
    ctx->rdEnable = 0;
    ctx->wrEnable = 1;
    ctx->mc = mc;
    ctx->curSec = ctx->ent.dataStart;
    ctx->pos = 0;

    // create stream
    Stream* stream = stream_create(ctx,
        memcard_stream_read,
        memcard_stream_write,
        memcard_stream_flush,
        memcard_stream_seek,
        memcard_stream_tell,
        memcard_stream_close);

    if (stream == NULL) {
        free(ctx);
        errno = ENOMEM;
        return NULL;
    }

    return stream;
}