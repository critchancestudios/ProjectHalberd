#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <SDL.h>
#include <GL/gl.h>

#include "gpuconfig.h"

#include "gpu.h"
#include "util.h"
#include "tmu.h"

#define REG_MAX 512
#define MAX_VTXBUF 512 * 3

#define FETCH_GL_PROC(name) gpu->name = SDL_GL_GetProcAddress(#name); assert(gpu->name != NULL)

typedef struct {
    int rgbFunc;
    int rgbFactor;
    int aFunc;
    int aFactor;
    int rgbInvert;
    int aInvert;
} ColorCombineSettings;

typedef struct {
    GLint tmu_enable[GPU_NUM_TMU];
    GLint tmu_tex[GPU_NUM_TMU];
    GLint tmu_rgbFunc[GPU_NUM_TMU];
    GLint tmu_aFunc[GPU_NUM_TMU];
    GLint tmu_rgbFac[GPU_NUM_TMU];
    GLint tmu_aFac[GPU_NUM_TMU];
    GLint tmu_rgbInvert[GPU_NUM_TMU];
    GLint tmu_aInvert[GPU_NUM_TMU];

    GLint color_rgbFunc;
    GLint color_aFunc;
    GLint color_rgbFac;
    GLint color_aFac;
    GLint color_rgbInvert;
    GLint color_aInvert;

    GLint alpha_compare;
    GLint alpha_ref;

    GLint fog_lut;
    GLint fog_color;
    GLint fog_enable;

    GLint gamma;
} ShaderUniformData;

typedef void (*reghandler_fn)(Gpu* gpu, int chipsel, int addr, uint32_t val);

typedef union {
    uint32_t u;
    float f;
} u2f;

struct _Gpu {
    SDL_Window* win;
    SDL_GLContext gl;
    int resw, resh;
    int cx, cy, cw, ch;
    ColorCombineSettings fbcc;
    ColorCombineSettings tmucc[GPU_NUM_TMU];
    Tmu* tmu[GPU_NUM_TMU];
    int tmu_en[GPU_NUM_TMU];
    uint8_t fogtable[64];
    float fogcolor[3];
    int fogEn;
    float clearColor[3];
    float clearDepth;
    float gamma;
    int colorMask;
    int depthMask;
    int alphaCompare;
    float alphaRef;
    GLenum triSign;
    uint32_t reg[3][REG_MAX];
    reghandler_fn reghandler[3][REG_MAX];
    GpuVertex_t vtxbuf[MAX_VTXBUF];
    int vtxbuflen;
    GLenum vtxmode;
    GLuint vbo;
    GLuint vtxShader;
    GLuint fragShader;
    GLuint shaderProgram;
    GLint vtxPos;
    GLint vtxCol;
    GLint vtxTex[GPU_NUM_TMU];
    ShaderUniformData uniformData;
    GLint fogLUT;
    void (*glGenBuffers)(GLsizei n, GLuint* buffers);
    void (*glBindBuffer)(GLenum target, GLuint buffer);
    void (*glBufferData)(GLenum target, GLsizeiptr size, const void* data, GLenum usage);
    void (*glBufferSubData)(GLenum target, GLintptr offset, GLsizeiptr size, const void* data);
    void (*glDeleteBuffers)(GLsizei n, GLuint* buffers);
    void (*glVertexAttribPointer)(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, const void* pointer);
    void (*glEnableVertexAttribArray)(GLuint index);
    GLint (*glGetAttribLocation)(GLuint program, const GLchar* name);
    GLint (*glGetUniformLocation)(GLuint program, const GLchar* name);
    void (*glUniform1f)(GLint location, GLfloat value);
    void (*glUniform4f)(GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3);
    void (*glUniform1i)(GLint location, GLint value);
    GLuint (*glCreateShader)(GLenum shaderType);
    void (*glShaderSource)(GLuint shader, GLsizei count, const GLchar** string, const GLint* length);
    void (*glCompileShader)(GLuint shader);
    void (*glGetShaderiv)(GLuint shader, GLenum pname, GLint* params);
    void (*glGetShaderInfoLog)(GLuint shader, GLsizei maxlength, GLsizei* length, GLchar* infolog);
    GLuint (*glCreateProgram)();
    void (*glAttachShader)(GLuint program, GLuint shader);
    void (*glLinkProgram)(GLuint program);
    void (*glGetProgramiv)(GLuint program, GLenum pname, GLint* params);
    void (*glGetProgramInfoLog)(GLuint program, GLsizei maxlength, GLsizei* length, GLchar* infolog);
    void (*glUseProgram)(GLuint program);
    void (*glDeleteShader)(GLuint shader);
    void (*glDeleteProgram)(GLuint program);
};

static void validate_shader(Gpu* gpu, GLuint shader) {
    GLint success = 0;
    gpu->glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        GLint logSize = 0;
        gpu->glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logSize);
        char* errorlog = malloc(logSize);
        gpu->glGetShaderInfoLog(shader, logSize, &logSize, errorlog);
        printf("Failed compiling shader: %s\n", errorlog);
        fflush(stdout);
        free(errorlog);
        abort();
    }
}

static void validate_program(Gpu* gpu, GLuint program) {
    GLint success = 0;
    gpu->glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success) {
        GLint logSize = 0;
        gpu->glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logSize);
        char* errorlog = malloc(logSize);
        gpu->glGetProgramInfoLog(program, logSize, &logSize, errorlog);
        printf("Failed linking program: %s\n", errorlog);
        fflush(stdout);
        free(errorlog);
        abort();
    }
}

static void flush_draw(Gpu* gpu) {
    if (gpu->vtxbuflen == 0) return;

    // set up winding
    if (gpu->triSign == 0) {
        glDisable(GL_CULL_FACE);
    }
    else {
        glEnable(GL_CULL_FACE);

        if (gpu->triSign > 0) {
            glFrontFace(GL_CCW);
        }
        else {
            glFrontFace(GL_CW);
        }
    }

    gpu->glUseProgram(gpu->shaderProgram);

    // set uniforms
    for (int i = 0; i < GPU_NUM_TMU; i++) {
        gpu->glUniform1i(gpu->uniformData.tmu_enable[i], gpu->tmu_en[i]);
        if (gpu->tmu_en[i]) {
            gpu->glUniform1i(gpu->uniformData.tmu_tex[i], i);

            glActiveTexture(GL_TEXTURE0 + i);
            tmu_preparetex(gpu->tmu[i]);
        }
        gpu->glUniform1i(gpu->uniformData.tmu_rgbFunc[i], gpu->tmucc[i].rgbFunc);
        gpu->glUniform1i(gpu->uniformData.tmu_rgbFunc[i], gpu->tmucc[i].rgbFunc);
        gpu->glUniform1i(gpu->uniformData.tmu_rgbFac[i], gpu->tmucc[i].rgbFactor);
        gpu->glUniform1i(gpu->uniformData.tmu_aFunc[i], gpu->tmucc[i].aFunc);
        gpu->glUniform1i(gpu->uniformData.tmu_aFac[i], gpu->tmucc[i].aFactor);
        gpu->glUniform1i(gpu->uniformData.tmu_rgbInvert[i], gpu->tmucc[i].rgbInvert);
        gpu->glUniform1i(gpu->uniformData.tmu_aInvert[i], gpu->tmucc[i].aInvert);
    }

    gpu->glUniform1i(gpu->uniformData.color_rgbFunc,     gpu->fbcc.rgbFunc);
    gpu->glUniform1i(gpu->uniformData.color_rgbFac,      gpu->fbcc.rgbFactor);
    gpu->glUniform1i(gpu->uniformData.color_aFunc,       gpu->fbcc.aFunc);
    gpu->glUniform1i(gpu->uniformData.color_aFac,        gpu->fbcc.aFactor);
    gpu->glUniform1i(gpu->uniformData.color_rgbInvert,   gpu->fbcc.rgbInvert);
    gpu->glUniform1i(gpu->uniformData.color_aInvert,     gpu->fbcc.aInvert);

    gpu->glUniform1i(gpu->uniformData.alpha_compare,     gpu->alphaCompare);
    gpu->glUniform1f(gpu->uniformData.alpha_ref,         gpu->alphaRef);

    gpu->glUniform1i(gpu->uniformData.fog_enable,        gpu->fogEn);
    gpu->glUniform4f(gpu->uniformData.fog_color,         gpu->fogcolor[0], gpu->fogcolor[1], gpu->fogcolor[2], 1.0f);
    gpu->glUniform1i(gpu->uniformData.fog_lut,           2);

    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, gpu->fogLUT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    gpu->glUniform1f(gpu->uniformData.gamma,             gpu->gamma);

    gpu->glBindBuffer(GL_ARRAY_BUFFER, gpu->vbo);
    gpu->glBufferSubData(GL_ARRAY_BUFFER, 0, gpu->vtxbuflen * sizeof(GpuVertex_t), gpu->vtxbuf);

    gpu->glVertexAttribPointer(gpu->vtxPos, 4, GL_FLOAT, GL_FALSE, sizeof(GpuVertex_t), offsetof(GpuVertex_t, x));    // x, y, z, w
    gpu->glVertexAttribPointer(gpu->vtxCol, 4, GL_FLOAT, GL_FALSE, sizeof(GpuVertex_t), offsetof(GpuVertex_t, r));    // r, g, b, a

    gpu->glEnableVertexAttribArray(gpu->vtxPos);
    gpu->glEnableVertexAttribArray(gpu->vtxCol);

    // s, t
    int offset = offsetof(GpuVertex_t, tmu);
    for (int i = 0; i < GPU_NUM_TMU; i++) {
        gpu->glVertexAttribPointer(gpu->vtxTex[i], 2, GL_FLOAT, GL_FALSE, sizeof(GpuVertex_t), offset);
        gpu->glEnableVertexAttribArray(gpu->vtxTex[i]);
        offset += sizeof(GpuTmuVertex_t);
    }

    glDrawArrays(gpu->vtxmode, 0, gpu->vtxbuflen);
    gpu->vtxbuflen = 0;
}

static void add_triangle(Gpu* gpu, GpuVertex_t* a, GpuVertex_t* b, GpuVertex_t* c) {
    if (gpu->vtxmode != GL_TRIANGLES || gpu->vtxbuflen > MAX_VTXBUF - 3) {
        flush_draw(gpu);
        gpu->vtxmode = GL_TRIANGLES;
    }

    gpu->vtxbuf[gpu->vtxbuflen++] = *a;
    gpu->vtxbuf[gpu->vtxbuflen++] = *b;
    gpu->vtxbuf[gpu->vtxbuflen++] = *c;
}

static void add_line(Gpu* gpu, GpuVertex_t* a, GpuVertex_t* b) {
    if (gpu->vtxmode != GL_LINES || gpu->vtxbuflen > MAX_VTXBUF - 2) {
        flush_draw(gpu);
        gpu->vtxmode = GL_LINES;
    }

    gpu->vtxbuf[gpu->vtxbuflen++] = *a;
    gpu->vtxbuf[gpu->vtxbuflen++] = *b;
}

static void add_pt(Gpu* gpu, GpuVertex_t* a) {
    if (gpu->vtxmode != GL_POINTS || gpu->vtxbuflen > MAX_VTXBUF - 1) {
        flush_draw(gpu);
        gpu->vtxmode = GL_POINTS;
    }

    gpu->vtxbuf[gpu->vtxbuflen++] = *a;
}

static GLenum convert_blend(int bf) {
    switch (bf) {
        case GPU_BLEND_ZERO: return GL_ZERO;
        case GPU_BLEND_ONE: return GL_ONE;
        case GPU_BLEND_CONST_RGB: return GL_CONSTANT_COLOR;
        case GPU_BLEND_CONST_A: return GL_CONSTANT_ALPHA;
        case GPU_BLEND_ARG0_RGB: return GL_SRC_COLOR;
        case GPU_BLEND_ARG0_A: return GL_SRC_ALPHA;
        case GPU_BLEND_ARG1_RGB: return GL_DST_COLOR;
        case GPU_BLEND_ARG1_A: return GL_DST_ALPHA;
        case GPU_BLEND_CONST_INV_RGB: return GL_ONE_MINUS_CONSTANT_COLOR;
        case GPU_BLEND_CONST_INV_A: return GL_ONE_MINUS_CONSTANT_ALPHA;
        case GPU_BLEND_ARG0_INV_RGB: return GL_ONE_MINUS_SRC_COLOR;
        case GPU_BLEND_ARG0_INV_A: return GL_ONE_MINUS_SRC_ALPHA;
        case GPU_BLEND_ARG1_INV_RGB: return GL_ONE_MINUS_DST_COLOR;
        case GPU_BLEND_ARG1_INV_A: return GL_ONE_MINUS_DST_ALPHA;
    }

    return GL_ZERO;
}

static GLenum convert_cmpfunc(int df) {
    switch (df) {
        case GPU_COMPARE_NEVER: return GL_NEVER;
        case GPU_COMPARE_ALWAYS: return GL_ALWAYS;
        case GPU_COMPARE_LESS: return GL_LESS;
        case GPU_COMPARE_LEQUAL: return GL_LEQUAL;
        case GPU_COMPARE_GREATER: return GL_GREATER;
        case GPU_COMPARE_GEQUAL: return GL_GEQUAL;
        case GPU_COMPARE_EQUAL: return GL_EQUAL;
        case GPU_COMPARE_NOTEQUAL: return GL_NOTEQUAL;
    }

    return GL_NEVER;
}

static void reg_fb_init(Gpu* gpu, int chipsel, int addr, uint32_t val) {
    flush_draw(gpu);

    uint32_t res = GPU_DM_GETRES(val);
    uint32_t dither = GPU_DM_GETDITHER(val);

    switch (res) {
        case GPU_DM_320x240_VGA:
        case GPU_DM_320x240_NTSC:
            gpu->resw = 320;
            gpu->resh = 240;
            SDL_SetWindowSize(gpu->win, 320, 240);
            break;
        case GPU_DM_320x288_PAL:
            gpu->resw = 320;
            gpu->resh = 288;
            SDL_SetWindowSize(gpu->win, 320, 288);
            break;
        case GPU_DM_640x480_VGA:
        case GPU_DM_640x480_NTSC:
            gpu->resw = 640;
            gpu->resh = 480;
            SDL_SetWindowSize(gpu->win, 640, 480);
            break;
        case GPU_DM_640x576_PAL:
            gpu->resw = 640;
            gpu->resh = 576;
            SDL_SetWindowSize(gpu->win, 640, 576);
            break;
    }

    if (dither) glEnable( GL_DITHER );
    else glDisable( GL_DITHER );
}

static void reg_fb_clipx1x2(Gpu* gpu, int chipsel, int addr, uint32_t val) {
    flush_draw(gpu);

    uint32_t x1 = val & 0xFFFF;
    uint32_t x2 = (val >> 16) & 0xFFFF;

    gpu->cx = x1;
    gpu->cw = x2 - x1;

    glScissor(gpu->cx, gpu->cy, gpu->cw, gpu->ch);
}

static void reg_fb_clipy1y2(Gpu* gpu, int chipsel, int addr, uint32_t val) {
    flush_draw(gpu);

    uint32_t y1 = val & 0xFFFF;
    uint32_t y2 = (val >> 16) & 0xFFFF;

    gpu->cy = y1;
    gpu->ch = y2 - y1;

    glScissor(gpu->cx, gpu->cy, gpu->cw, gpu->ch);
}

static void reg_fb_pxConfig(Gpu* gpu, int chipsel, int addr, uint32_t val) {
    flush_draw(gpu);

    gpu->fbcc.rgbFunc = GPU_PXCFG_CCFNRGB(val);
    gpu->fbcc.aFunc = GPU_PXCFG_CCFNA(val);
    gpu->fbcc.rgbFactor = GPU_PXCFG_CCFCRGB(val);
    gpu->fbcc.aFactor = GPU_PXCFG_CCFCA(val);
    gpu->fbcc.rgbInvert = GPU_PXCFG_CCRGBINV(val);
    gpu->fbcc.aInvert = GPU_PXCFG_CCAINV(val);

#if GPU_NUM_TMU > 0
    gpu->tmu_en[0] = GPU_PXCFG_TMU0EN(val);
#endif

#if GPU_NUM_TMU > 1
    gpu->tmu_en[1] = GPU_PXCFG_TMU1EN(val);
#endif

    gpu->fogEn = GPU_PXCFG_FOGEN(val);

    int fb0 = GPU_PXCFG_BLENDSRC(val);
    int fb1 = GPU_PXCFG_BLENDDST(val);

    GLenum blend_src = convert_blend(fb0);
    GLenum blend_dst = convert_blend(fb1);

    glBlendFunc(blend_src, blend_dst);

    if (blend_src == GL_ONE && blend_dst == GL_ZERO) {
        glDisable(GL_BLEND);
    }
    else {
        glEnable(GL_BLEND);
    }
}

static void reg_fb_mode(Gpu* gpu, int chipsel, int addr, uint32_t val) {
    flush_draw(gpu);

    int colormask = GPU_FB_MODE_COLORMASK(val) != 0;
    int depthmask = GPU_FB_MODE_COLORMASK(val) != 0;
    int depthfunc = GPU_FB_MODE_DEPTHFUNC(val);
    int alphafunc = GPU_FB_MODE_ALPHAFUNC(val);
    int alpharef = GPU_FB_MODE_ALPHAREF(val);

    gpu->colorMask = colormask;
    gpu->depthMask = depthmask;
    gpu->alphaCompare = alphafunc;
    gpu->alphaRef = alpharef;

    glColorMask(colormask, colormask, colormask, colormask);
    glDepthMask(depthmask);
    glDepthFunc(convert_cmpfunc(depthfunc));
}

static void reg_fb_depthbias(Gpu* gpu, int chipsel, int addr, uint32_t val) {
    flush_draw(gpu);

    u2f conv = {
        .u = val
    };

    glPolygonOffset(1.0f, conv.f);
    
    if (val) {
        glEnable(GL_POLYGON_OFFSET_FILL);
    }
    else {
        glDisable(GL_POLYGON_OFFSET_FILL);
    }
}

static void reg_fb_fogtable(Gpu* gpu, int chipsel, int addr, uint32_t val) {
    flush_draw(gpu);

    // fog table is 64 bytes, each byte is normalized 0..255 density
    // one 32-bit register write contains four consecutive fog table entries

    int idx = (addr - GPU_REG_FB_FOGTABLE) << 2;

    uint32_t val0 = val & 0xFF;
    uint32_t val1 = (val >> 8) & 0xFF;
    uint32_t val2 = (val >> 16) & 0xFF;
    uint32_t val3 = (val >> 24) & 0xFF;

    gpu->fogtable[idx++] = val0;
    gpu->fogtable[idx++] = val1;
    gpu->fogtable[idx++] = val2;
    gpu->fogtable[idx++] = val3;

    // upload to texture
    glBindTexture(GL_TEXTURE_2D, gpu->fogLUT);
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 64, 1, GL_ALPHA, GL_UNSIGNED_BYTE, gpu->fogtable);
}

static void reg_fb_clearcolor(Gpu* gpu, int chipsel, int addr, uint32_t val) {
    uint32_t b = val & 0xFF;
    uint32_t g = (val >> 8) & 0xFF;
    uint32_t r = (val >> 16) & 0xFF;

    gpu->clearColor[0] = ((float)r) / 255.0f;
    gpu->clearColor[1] = ((float)g) / 255.0f;
    gpu->clearColor[2] = ((float)b) / 255.0f;

    glClearColor(gpu->clearColor[0], gpu->clearColor[1], gpu->clearColor[2], 1.0f);
}

static void reg_fb_cleardepth(Gpu* gpu, int chipsel, int addr, uint32_t val) {
    u2f conv = {
        .u = val
    };

    gpu->clearDepth = conv.f;

    glClearDepth(gpu->clearDepth);
}

static void reg_fb_constcol(Gpu* gpu, int chipsel, int addr, uint32_t val) {
    flush_draw(gpu);

    uint32_t r = val & 0xFF;
    uint32_t g = (val >> 8) & 0xFF;
    uint32_t b = (val >> 16) & 0xFF;
    uint32_t a = (val >> 24) & 0xFF;

    float rf = ((float)r) / 255.0f;
    float gf = ((float)g) / 255.0f;
    float bf = ((float)b) / 255.0f;
    float af = ((float)a) / 255.0f;

    glBlendColor(rf, gf, bf, af);
}

static void reg_fb_fogcolor(Gpu* gpu, int chipsel, int addr, uint32_t val) {
    flush_draw(gpu);

    uint32_t b = val & 0xFF;
    uint32_t g = (val >> 8) & 0xFF;
    uint32_t r = (val >> 16) & 0xFF;

    float rf = ((float)r) / 255.0f;
    float gf = ((float)g) / 255.0f;
    float bf = ((float)b) / 255.0f;

    gpu->fogcolor[0] = rf;
    gpu->fogcolor[1] = gf;
    gpu->fogcolor[2] = bf;
}

static void reg_fb_gamma(Gpu* gpu, int chipsel, int addr, uint32_t val) {
    flush_draw(gpu);

    u2f conv = {
        .u = val
    };

    gpu->gamma = conv.f;
}

static void reg_tmu_texaddr(Gpu* gpu, int chipsel, int addr, uint32_t val) {
    flush_draw(gpu);

    int tmu = chipsel - GPU_CHIP_TMU0;
    if (tmu < 0 || tmu >= GPU_NUM_TMU) return;

    tmu_setaddr(gpu->tmu[tmu], val);
}

static void reg_tmu_tlod(Gpu* gpu, int chipsel, int addr, uint32_t val) {
    flush_draw(gpu);

    int tmu = chipsel - GPU_CHIP_TMU0;
    if (tmu < 0 || tmu >= GPU_NUM_TMU) return;

    int minlod = GPU_TLOD_MIN(val);
    int maxlod = GPU_TLOD_MAX(val);
    int bias = GPU_TLOD_BIAS(val); // Q4.2 fixed point

    tmu_setlod(gpu->tmu[tmu], minlod, maxlod, bias / 4.0f);
}

static void reg_tmu_tmode(Gpu* gpu, int chipsel, int addr, uint32_t val) {
    flush_draw(gpu);

    int tmu = chipsel - GPU_CHIP_TMU0;
    if (tmu < 0 || tmu >= GPU_NUM_TMU) return;

    gpu->tmucc[tmu].rgbFunc = GPU_TMODE_CCFNRGB(val);
    gpu->tmucc[tmu].aFunc = GPU_TMODE_CCFNA(val);
    gpu->tmucc[tmu].rgbFactor = GPU_TMODE_CCFCRGB(val);
    gpu->tmucc[tmu].aFactor = GPU_TMODE_CCFCA(val);
    gpu->tmucc[tmu].rgbInvert = GPU_TMODE_CCRGBINV(val);
    gpu->tmucc[tmu].aInvert = GPU_TMODE_CCAINV(val);
    
    tmu_setclamp(gpu->tmu[tmu], GPU_TMODE_CLAMPS(val), GPU_TMODE_CLAMPT(val));
    tmu_setfilter(gpu->tmu[tmu], GPU_TMODE_MINF(val), GPU_TMODE_MAGF(val));
    tmu_setfmt(gpu->tmu[tmu], GPU_TMODE_TEXFMT(val));
    tmu_setNcc(gpu->tmu[tmu], GPU_TMODE_NCCTBL(val));
}

static void reg_tmu_yiqtable(Gpu* gpu, int chipsel, int addr, uint32_t val) {
    flush_draw(gpu);

    int tmu = chipsel - GPU_CHIP_TMU0;
    if (tmu < 0 || tmu >= GPU_NUM_TMU) return;

    int idx = (addr - GPU_REG_TMU_NCCTABLE0) << 2;
    int tableidx = idx / sizeof(NCCTable);

    void* table = tmu_getncctable(gpu->tmu[tmu], tableidx);
    uint32_t* ptr = (uint32_t*)table;

    *ptr = val;

    tmu_dirtyncctable(gpu->tmu[tmu]);
}

static void reg_tmu_paltable(Gpu* gpu, int chipsel, int addr, uint32_t val) {
    flush_draw(gpu);

    int tmu = chipsel - GPU_CHIP_TMU0;
    if (tmu < 0 || tmu >= GPU_NUM_TMU) return;

    PalTable* pal = tmu_getpaltable(gpu->tmu[tmu]);

    int idx = (addr - GPU_REG_TMU_PALTABLE);
    pal->rgb[idx] = val;

    tmu_dirtypaltable(gpu->tmu[tmu]);
}

Gpu* gpu_create() {
    Gpu* gpu = malloc(sizeof(Gpu));
    assert(gpu != NULL);

    // zero init
    memset(gpu, 0, sizeof(Gpu));

    // hook up reg handlers
    gpu->reghandler[GPU_CHIP_FB][GPU_REG_FB_DM] = reg_fb_init;
    gpu->reghandler[GPU_CHIP_FB][GPU_REG_FB_CLIPX1X2] = reg_fb_clipx1x2;
    gpu->reghandler[GPU_CHIP_FB][GPU_REG_FB_CLIPY1Y2] = reg_fb_clipy1y2;
    gpu->reghandler[GPU_CHIP_FB][GPU_REG_FB_PXCONFIG] = reg_fb_pxConfig;
    gpu->reghandler[GPU_CHIP_FB][GPU_REG_FB_MODE] = reg_fb_mode;
    gpu->reghandler[GPU_CHIP_FB][GPU_REG_FB_DEPTHBIAS] = reg_fb_depthbias;
    for (int i = 0; i < 16; i++) {
        gpu->reghandler[GPU_CHIP_FB][GPU_REG_FB_FOGTABLE + i] = reg_fb_fogtable;
    }
    gpu->reghandler[GPU_CHIP_FB][GPU_REG_FB_CLEARCOL] = reg_fb_clearcolor;
    gpu->reghandler[GPU_CHIP_FB][GPU_REG_FB_CLEARDEPTH] = reg_fb_cleardepth;
    gpu->reghandler[GPU_CHIP_FB][GPU_REG_FB_CONSTCOL] = reg_fb_constcol;
    gpu->reghandler[GPU_CHIP_FB][GPU_REG_FB_FOGCOLOR] = reg_fb_fogcolor;
    gpu->reghandler[GPU_CHIP_FB][GPU_REG_FB_GAMMA] = reg_fb_gamma;

    for (int i = 0; i < 2; i++) {
        gpu->reghandler[GPU_CHIP_TMU0 + i][GPU_REG_TMU_TEXADDR] = reg_tmu_texaddr;
        gpu->reghandler[GPU_CHIP_TMU0 + i][GPU_REG_TMU_TLOD] = reg_tmu_tlod;
        gpu->reghandler[GPU_CHIP_TMU0 + i][GPU_REG_TMU_TMODE] = reg_tmu_tmode;

        for (int j = 0; j < 16; j++) {
            gpu->reghandler[GPU_CHIP_TMU0 + i][GPU_REG_TMU_NCCTABLE0 + j] = reg_tmu_yiqtable;
            gpu->reghandler[GPU_CHIP_TMU0 + i][GPU_REG_TMU_NCCTABLE1 + j] = reg_tmu_yiqtable;
        }

        for (int j = 0; j < 256; j++) {
            gpu->reghandler[GPU_CHIP_TMU0 + i][GPU_REG_TMU_PALTABLE + j] = reg_tmu_paltable;
        }
    }

    // uncomment this if you need to debug with RenderDoc
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);

    // create SDL window
    gpu->win = SDL_CreateWindow("Halberd Emulator", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        640, 480, SDL_WINDOW_OPENGL);
    assert(gpu->win != NULL);

    // create GL context
    gpu->gl = SDL_GL_CreateContext(gpu->win);
    assert(gpu->gl != NULL);
    
    glEnable(GL_SCISSOR_TEST);

    // get GL functions
    FETCH_GL_PROC(glGenBuffers);
    FETCH_GL_PROC(glBindBuffer);
    FETCH_GL_PROC(glBufferData);
    FETCH_GL_PROC(glBufferSubData);
    FETCH_GL_PROC(glDeleteBuffers);
    FETCH_GL_PROC(glVertexAttribPointer);
    FETCH_GL_PROC(glGetAttribLocation);
    FETCH_GL_PROC(glEnableVertexAttribArray);
    FETCH_GL_PROC(glGetUniformLocation);
    FETCH_GL_PROC(glUniform1f);
    FETCH_GL_PROC(glUniform4f);
    FETCH_GL_PROC(glUniform1i);
    FETCH_GL_PROC(glCreateShader);
    FETCH_GL_PROC(glShaderSource);
    FETCH_GL_PROC(glCompileShader);
    FETCH_GL_PROC(glGetShaderiv);
    FETCH_GL_PROC(glGetShaderInfoLog);
    FETCH_GL_PROC(glCreateProgram);
    FETCH_GL_PROC(glAttachShader);
    FETCH_GL_PROC(glLinkProgram);
    FETCH_GL_PROC(glGetProgramiv);
    FETCH_GL_PROC(glGetProgramInfoLog);
    FETCH_GL_PROC(glUseProgram);
    FETCH_GL_PROC(glDeleteShader);
    FETCH_GL_PROC(glDeleteProgram);

    // set up vertex buffer
    gpu->glGenBuffers(1, &gpu->vbo);
    gpu->glBindBuffer(GL_ARRAY_BUFFER, gpu->vbo);
    gpu->glBufferData(GL_ARRAY_BUFFER, MAX_VTXBUF * sizeof(GpuVertex_t), NULL, GL_DYNAMIC_DRAW);

    // load shaders
    size_t vtxlen, fraglen;
    char* vtx_src = loadfile("shaders/fixedfunc.vert", &vtxlen); assert(vtx_src != NULL);
    char* frag_src = loadfile("shaders/fixedfunc.frag", &fraglen); assert(frag_src != NULL);

    gpu->vtxShader = gpu->glCreateShader(GL_VERTEX_SHADER);
    gpu->glShaderSource(gpu->vtxShader, 1, &vtx_src, &vtxlen);
    gpu->glCompileShader(gpu->vtxShader);
    validate_shader(gpu, gpu->vtxShader);

    gpu->fragShader = gpu->glCreateShader(GL_FRAGMENT_SHADER);
    gpu->glShaderSource(gpu->fragShader, 1, &frag_src, &fraglen);
    gpu->glCompileShader(gpu->fragShader);
    validate_shader(gpu, gpu->fragShader);

    gpu->shaderProgram = gpu->glCreateProgram();
    gpu->glAttachShader(gpu->shaderProgram, gpu->vtxShader);
    gpu->glAttachShader(gpu->shaderProgram, gpu->fragShader);
    gpu->glLinkProgram(gpu->shaderProgram);
    validate_program(gpu, gpu->shaderProgram);

    free(vtx_src);
    free(frag_src);

    // create texture for fog table
    glGenTextures(1, &gpu->fogLUT);
    glBindTexture(GL_TEXTURE_2D, gpu->fogLUT);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA8, 64, 1, 0, GL_ALPHA, GL_UNSIGNED_BYTE, NULL);

    gpu->vtxPos = gpu->glGetAttribLocation(gpu->shaderProgram, "apos");
    gpu->vtxCol = gpu->glGetAttribLocation(gpu->shaderProgram, "acol");

    for (int i = 0; i < GPU_NUM_TMU; i++) {
        char buf[32];
        sprintf(buf, "atex%d", i);
        gpu->vtxTex[i] = gpu->glGetAttribLocation(gpu->shaderProgram, buf);

        sprintf(buf, "tmu%d_enable", i);
        gpu->uniformData.tmu_enable[i] = gpu->glGetUniformLocation(gpu->shaderProgram, buf);

        sprintf(buf, "tmu%d_tex", i);
        gpu->uniformData.tmu_tex[i] = gpu->glGetUniformLocation(gpu->shaderProgram, buf);

        sprintf(buf, "tmu%d_rgbFunc", i);
        gpu->uniformData.tmu_rgbFunc[i] = gpu->glGetUniformLocation(gpu->shaderProgram, buf);

        sprintf(buf, "tmu%d_aFunc", i);
        gpu->uniformData.tmu_aFunc[i] = gpu->glGetUniformLocation(gpu->shaderProgram, buf);

        sprintf(buf, "tmu%d_rgbFac", i);
        gpu->uniformData.tmu_rgbFac[i] = gpu->glGetUniformLocation(gpu->shaderProgram, buf);

        sprintf(buf, "tmu%d_aFac", i);
        gpu->uniformData.tmu_aFac[i] = gpu->glGetUniformLocation(gpu->shaderProgram, buf);

        sprintf(buf, "tmu%d_rgbInvert", i);
        gpu->uniformData.tmu_rgbInvert[i] = gpu->glGetUniformLocation(gpu->shaderProgram, buf);

        sprintf(buf, "tmu%d_aInvert", i);
        gpu->uniformData.tmu_aInvert[i] = gpu->glGetUniformLocation(gpu->shaderProgram, buf);
    }

    #define FETCH_UNIFORM(name) gpu->uniformData.name = gpu->glGetUniformLocation(gpu->shaderProgram, #name)
    FETCH_UNIFORM(color_rgbFunc);
    FETCH_UNIFORM(color_aFunc);
    FETCH_UNIFORM(color_rgbFac);
    FETCH_UNIFORM(color_aFac);
    FETCH_UNIFORM(color_rgbInvert);
    FETCH_UNIFORM(color_aInvert);
    FETCH_UNIFORM(alpha_compare);
    FETCH_UNIFORM(alpha_ref);
    FETCH_UNIFORM(fog_lut);
    FETCH_UNIFORM(fog_enable);
    FETCH_UNIFORM(fog_color);
    FETCH_UNIFORM(gamma);
    #undef FETCH_UNIFORM

    printf("Shaders initialized\n");

    // init TMUs
    for (int i = 0; i < GPU_NUM_TMU; i++) {
        gpu->tmu[i] = tmu_create();
        assert(gpu->tmu[i] != NULL);
    }

    // ensure register defaults are applied
    for (int i = 0; i < REG_MAX; i++) {
        gpu_setreg(gpu, GPU_CHIP_FB, i, 0);
        gpu_setreg(gpu, GPU_CHIP_TMU0, i, 0);
        gpu_setreg(gpu, GPU_CHIP_TMU1, i, 0);
    }

    return gpu;
}

void gpu_destroy(Gpu* gpu) {
    for (int i = 0; i < GPU_NUM_TMU; i++) {
        tmu_destroy(gpu->tmu[i]);
    }
    gpu->glDeleteBuffers(1, &gpu->vbo);
    gpu->glDeleteProgram(gpu->shaderProgram);
    gpu->glDeleteShader(gpu->vtxShader);
    gpu->glDeleteShader(gpu->fragShader);
    SDL_GL_DeleteContext(gpu->gl);
    SDL_DestroyWindow(gpu->win);
    free(gpu);
}

uint32_t gpu_getreg(Gpu* gpu, int chipsel, int addr) {
    if (chipsel < 0 || chipsel > GPU_CHIP_TMU1) return 0;
    if (addr < 0 || addr >= REG_MAX) return 0;

    return gpu->reg[chipsel][addr];
}

void gpu_setreg(Gpu* gpu, int chipsel, int addr, uint32_t val) {
    if (chipsel < 0 || chipsel > GPU_CHIP_TMU1) return;
    if (addr < 0 || addr >= REG_MAX) return;

    gpu->reg[chipsel][addr] = val;
    reghandler_fn handler = gpu->reghandler[chipsel][addr];
    if (handler != NULL) {
        handler(gpu, chipsel, addr, val);
    }
}

float gpu_getregf(Gpu* gpu, int chipsel, int addr) {
    u2f u = {
        .u = gpu_getreg(gpu, chipsel, addr)
    };
    return u.f;
}

void gpu_setregf(Gpu* gpu, int chipsel, int addr, float val) {
    u2f u = {
        .f = val
    };
    gpu_setreg(gpu, chipsel, addr, u.u);
}

void gpu_handlecmd(Gpu* gpu, GpuCmd_t* packet) {
    switch (packet->packet_type) {
        case GPU_CMD_SWAPBUF: {
            flush_draw(gpu);
            SDL_GL_SwapWindow(gpu->win);
            break;
        }
        case GPU_CMD_FILLRECT: {
            GLbitfield clearFlags = 0;
            if (gpu->colorMask) {
                clearFlags |= GL_COLOR_BUFFER_BIT;
            }
            if (gpu->depthMask) {
                clearFlags |= GL_DEPTH_BUFFER_BIT;
            }
            glClear(clearFlags);
            break;
        }
        case GPU_CMD_DRAWPT: {
            add_pt(gpu, &packet->param.drawpt.vtx);
            break;
        }
        case GPU_CMD_DRAWLINE: {
            add_line(gpu, &packet->param.drawline.vtx[0], &packet->param.drawline.vtx[1]);
            break;
        }
        case GPU_CMD_DRAWTRI: {
            if (packet->param.drawtri.sign != gpu->triSign) {
                flush_draw(gpu);
                gpu->triSign = packet->param.drawtri.sign;
            }
            add_triangle(gpu, &packet->param.drawtri.vtx[0], &packet->param.drawtri.vtx[1], &packet->param.drawtri.vtx[2]);
            break;
        }
    }
}

void gpu_flushcmd(Gpu* gpu) {
    // process SDL events
    SDL_Event e;
    while (SDL_PollEvent(&e)) {
        if (e.type == SDL_QUIT) {
            // abort
            printf("Exit requested, killing");
            exit(0);
        }
    }
    glFlush();
}

void gpu_readmem(Gpu* gpu, int chipsel, int addr, int len, void* buffer) {
    if (chipsel == GPU_CHIP_TMU0) {
        tmu_read(gpu->tmu[0], buffer, addr, len);
    }
    else if (chipsel == GPU_CHIP_TMU1) {
        tmu_read(gpu->tmu[1], buffer, addr, len);
    }
}

void gpu_writemem(Gpu* gpu, int chipsel, int addr, int len, const void* buffer) {
    if (chipsel == GPU_CHIP_TMU0) {
        tmu_write(gpu->tmu[0], buffer, addr, len);
    }
    else if (chipsel == GPU_CHIP_TMU1) {
        tmu_write(gpu->tmu[1], buffer, addr, len);
    }
}