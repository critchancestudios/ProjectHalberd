#include "crc.h"

#define CRC_POLY 0x82f63b78

// calculate crc32 of given bytes
uint32_t crc32(uint32_t crc, const unsigned char* buf, size_t len) {
    int k;

    crc = ~crc;
    while (len--) {
        crc ^= *buf++;
        for (k = 0; k < 8; k++)
            crc = crc & 1 ? (crc >> 1) ^ CRC_POLY : crc >> 1;
    }
    return ~crc;
}