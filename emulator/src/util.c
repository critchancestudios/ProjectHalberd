#include <stdio.h>

#include "util.h"

void *loadfile(const char *path, size_t* len) {
    FILE *file = fopen(path, "rb");
    if (file == NULL) {
        printf("Failed opening file: %s\n", path);
        return NULL;
    }

    fseek(file, 0, SEEK_END);
    *len = ftell(file);
    fseek(file, 0, SEEK_SET);

    void *data = malloc(*len);
    if (data == NULL) {
        printf("Failed allocating memory: %s\n", path);
        return NULL;
    }

    fread(data, *len, 1, file);
    fclose(file);

    return data;
}

void *loadAllStream(Stream* stream) {
    stream_seek(stream, 0, SEEK_END);
    long len = stream_tell(stream);
    stream_seek(stream, 0, SEEK_SET);

    void *data = malloc(len);
    if (data == NULL) {
        return NULL;
    }

    stream_read(data, len, 1, stream);
    return data;
}