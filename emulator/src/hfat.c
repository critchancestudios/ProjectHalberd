#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>

#include "hfat.h"
#include "crc.h"

// just a ultra simple flat FAT-like fs
// lots of inspiration taken from FAT12, along with PS2 and Gamecube schemes
// nothing really in the way of error recovery - "don't turn your console off" and all that :3

#define FAT_ENTRIES (MC_SECTOR_SIZE / 2)
#define DT_ENTRIES  (MC_SECTOR_SIZE / sizeof(HFAT_DtEntry)) 

#define MAX_SECTORS 4096

#define HDR_MAGIC "HFAT"
#define HDR_CRC_BYTES 24

typedef struct {
    char oem[8];
    uint32_t serial;
    char magic[4];
    uint32_t fmtTime;
    uint16_t dtSectors;
    uint16_t fatSectors;
    uint32_t hdrChecksum;
    uint32_t dtChecksum;
    uint32_t fatChecksum;
} HFAT_Header;

struct _Memcard {
    int valid;
    Stream* fs;
};

uint32_t gen_serial() {
    struct timespec ts;
    if (timespec_get(&ts, TIME_UTC) != TIME_UTC) {
        return 0;
    }

    time_t t = (time_t)ts.tv_sec;
    struct tm tm = *localtime(&t);

    int cs = ts.tv_nsec / 10000000;

    uint32_t mon = tm.tm_mon & 0xFF;
    uint32_t day = tm.tm_mday & 0xFF;
    uint32_t sec = tm.tm_sec & 0xFF;
    uint32_t csec = cs & 0xFF;
    
    uint32_t hr = tm.tm_hour & 0xFF;
    uint32_t min = tm.tm_min & 0xFF;
    uint32_t yr = tm.tm_year & 0xFFFF;

    uint32_t a = (mon << 8) | day;
    uint32_t b = (sec << 8) | csec;

    uint32_t c = (hr << 8) | min;
    uint32_t d = yr;

    uint32_t ab = a ^ b;
    uint32_t cd = c ^ d;

    return (ab << 16) | cd;
}

// read data from a sector into memory
static void read_sector(Stream* fs, int sector, void* buffer, size_t size) {
    if (sector < 0 || sector >= MAX_SECTORS) return;

    stream_lock(fs);
    stream_seek(fs, sector * MC_SECTOR_SIZE, SEEK_SET);
    stream_read(buffer, size, 1, fs);
    stream_unlock(fs);
}

// write data to a sector on disk
static void write_sector(Stream* fs, int sector, const void* buffer, size_t size) {
    if (sector < 0 || sector >= MAX_SECTORS) return;
    
    stream_lock(fs);
    stream_seek(fs, sector * MC_SECTOR_SIZE, SEEK_SET);
    stream_write(buffer, size, 1, fs);
    stream_unlock(fs);
}

// calculate crc32 of contents of given sector range on disk
static uint32_t crc32_sectors(Stream* fs, uint32_t crc, int sectorStart, int sectorCount) {
    char sectorbuf[MC_SECTOR_SIZE];

    for (int i = 0; i < sectorCount; i++) {
        read_sector(fs, sectorStart + i, sectorbuf, MC_SECTOR_SIZE);
        crc = crc32(crc, sectorbuf, MC_SECTOR_SIZE);
    }

    return crc;
}

// given a single sector of the FAT, look for and return offset of first unallocated entry
static int allocate_from_fat_sector(uint16_t* fat, int startIdx) {
    for (int i = startIdx; i < FAT_ENTRIES; i++) {
        if (fat[i] == 0) {
            return i;
        }
    }

    return -1;
}

// given a single sector of the DT, look for and return offset of first unallocated entry
static int allocate_from_dt_sector(HFAT_DtEntry* dt) {
    for (int i = 0; i < DT_ENTRIES; i++) {
        if (dt[i].filename[0] == 0) {
            return i;
        }
    }

    return -1;
}

// allocate single entry within DT
// returns index of entry within DT, or -1 if failed
static int allocate_dt_entry(Memcard* mc, const HFAT_Header* header) {
    // scan through DT looking for a free entry
    int dir_start = 1;

    HFAT_DtEntry dir_sector[DT_ENTRIES];

    for (int i = 0; i < header->dtSectors; i++) {
        // scan dt sector for a free entry
        read_sector(mc->fs, dir_start + i, &dir_sector, MC_SECTOR_SIZE);

        int idx = allocate_from_dt_sector(dir_sector);
        if (idx != -1) {
            return idx + (i * DT_ENTRIES);
        }
    }

    return -1;
}

// allocate single data sector within FAT
// returns sector number, or -1 if failed
static int allocate_data_sector(Memcard* mc, const HFAT_Header* header, int startidx) {
    // scan through the FAT looking for a free sector
    int fat_start = 1 + header->dtSectors;
    int data_start = fat_start + header->fatSectors;

    uint16_t fat_sector[FAT_ENTRIES];

    int first_sec = startidx / FAT_ENTRIES;
    int fat_offs = startidx % FAT_ENTRIES;

    for (int i = first_sec; i < header->fatSectors; i++) {
        // scan FAT sector for a free entry
        read_sector(mc->fs, fat_start + i, &fat_sector, MC_SECTOR_SIZE);

        int idx = allocate_from_fat_sector(fat_sector, fat_offs);
        fat_offs = 0;

        if (idx != -1) {
            return data_start + idx + (i * FAT_ENTRIES);
        }
    }

    return -1;
}

// allocate a number of data sectors within FAT, writing to sector number array
// returns number of sectors allocated
static int allocate_data_sectors(Memcard* mc, const HFAT_Header* header, int* sectors, int count) {
    int alloc_cnt = 0;
    int start_idx = 0;

    int fat_start = 1 + header->dtSectors;
    int data_start = fat_start + header->fatSectors;

    for (int i = 0; i < count; i++) {
        int sec = allocate_data_sector(mc, header, start_idx);
        if (sec == -1) break;

        sectors[i] = sec;
        alloc_cnt++;

        // start search at next fat entry so it doesn't keep flagging the same entry (which we haven't updated yet)
        // if this would go past end of FAT, just end our search instead
        start_idx = (sec - data_start) + 1;

        if (start_idx >= header->fatSectors) {
            break;
        }
    }

    return alloc_cnt;
}

// create a new file (updates FAT, DT, and header checksums)
// returns new index into directory table on success, -1 on failure
int memcard_create_file(Memcard* mc, const char* filename, uint32_t length, uint32_t metaOffset) {
    // ensure file does not already exist
    if (memcard_find_file(mc, filename) != -1) {
        errno = EEXIST;
        return -1;
    }

    HFAT_Header header;
    read_sector(mc->fs, 0, &header, sizeof(HFAT_Header));

    int dt_start = 1;
    int fat_start = dt_start + header.dtSectors;

    uint8_t secbuf[MC_SECTOR_SIZE];

    int prev_fat = -1;
    uint16_t* fatbuf = (uint16_t*)secbuf;
    HFAT_DtEntry* dtbuf = (HFAT_DtEntry*)secbuf;

    // allocate entry in dt
    int dir_ent = allocate_dt_entry(mc, &header);
    if (dir_ent == -1) {
        errno = ENOSPC;
        return -1;
    }

    // allocate sectors for file data
    int* secid = malloc(sizeof(int) * length);

    if (allocate_data_sectors(mc, &header, secid, length) != length) {
        free(secid);
        errno = ENOSPC;
        return -1;
    }

    // link up fat sectors
    for (int i = 0; i < length; i++) {
        // convert sector number into offset within FAT
        int fat_start = 1 + header.dtSectors;
        int data_start = fat_start + header.fatSectors;
        int fat_idx = secid[i] - data_start;

        int fat_sec = fat_start + (fat_idx / FAT_ENTRIES);
        int fat_offs = fat_idx % FAT_ENTRIES;

        // 0xFFFF indicates last entry of chain
        uint16_t link = 0xFFFF;
        if (i < length - 1) {
            link = (uint16_t)secid[i + 1];
        }

        if (fat_sec != prev_fat) {
            if (prev_fat != -1) {
                // flush old fat sector to disk
                write_sector(mc->fs, prev_fat, fatbuf, MC_SECTOR_SIZE);
            }
            // read in new fat sector
            prev_fat = fat_sec;
            read_sector(mc->fs, fat_sec, fatbuf, MC_SECTOR_SIZE);
        }

        fatbuf[fat_offs] = link;
    }

    uint16_t file_head = secid[0];
    free(secid);

    // flush fat sector to disk
    if (prev_fat != -1) {
        write_sector(mc->fs, prev_fat, fatbuf, MC_SECTOR_SIZE);
    }

    // record new file info
    HFAT_DtEntry file_info;
    strncpy(file_info.filename, filename, 20);
    file_info.dataLength = length;
    file_info.metaOffset = metaOffset;
    file_info.dataStart = file_head;
    file_info.timestamp = (uint32_t)time(NULL);

    // read in dt sector, write new entry, & flush to disk
    int dtsec = dt_start + (dir_ent / DT_ENTRIES);
    read_sector(mc->fs, dtsec, dtbuf, MC_SECTOR_SIZE);
    memcpy(&dtbuf[dir_ent % DT_ENTRIES], &file_info, sizeof(HFAT_DtEntry));
    write_sector(mc->fs, dtsec, dtbuf, MC_SECTOR_SIZE);

    // finally, recompute checksums for dirtable & fat sectors
    header.dtChecksum = crc32_sectors(mc->fs, 0, dt_start, header.dtSectors);
    header.fatChecksum = crc32_sectors(mc->fs, 0, fat_start, header.fatSectors);

    write_sector(mc->fs, 0, &header, sizeof(HFAT_Header));

    // flush memcard
    stream_flush(mc->fs);

    // return index into directory table
    return dir_ent;
}

// delete a file from the memory card, given its index in the DT
void memcard_delete_file(Memcard* mc, int dirEnt) {
    HFAT_Header header;
    read_sector(mc->fs, 0, &header, sizeof(HFAT_Header));

    int dt_start = 1;
    int fat_start = 1 + header.dtSectors;

    int dt_sec = 1 + (dirEnt / DT_ENTRIES);
    int dt_offs = dirEnt % DT_ENTRIES;

    int prev_fat = -1;

    uint8_t secbuf[MC_SECTOR_SIZE];
    uint16_t* fatbuf = (uint16_t*)secbuf;
    HFAT_DtEntry* dtbuf = (HFAT_DtEntry*)secbuf;

    // erase entry from dt
    read_sector(mc->fs, dt_sec, secbuf, MC_SECTOR_SIZE);
    HFAT_DtEntry ent = dtbuf[dt_offs];
    memset(&dtbuf[dt_offs], 0, sizeof(HFAT_DtEntry));
    write_sector(mc->fs, dt_sec, secbuf, MC_SECTOR_SIZE);

    // clear out FAT chain
    int cur_sec = ent.dataStart;
    for (int i = 0; i < ent.dataLength; i++) {
        // convert sector number into offset within FAT
        int data_start = fat_start + header.fatSectors;
        int fat_idx = cur_sec - data_start;

        int fat_sec = fat_start + (fat_idx / FAT_ENTRIES);
        int fat_offs = fat_idx % FAT_ENTRIES;

        if (fat_sec != prev_fat) {
            if (prev_fat != -1) {
                // flush sector
                write_sector(mc->fs, prev_fat, secbuf, MC_SECTOR_SIZE);
            }
            prev_fat = fat_sec;
            read_sector(mc->fs, fat_sec, secbuf, MC_SECTOR_SIZE);
        }

        uint16_t link = fatbuf[fat_offs];
        fatbuf[fat_offs] = 0;

        if (link == 0xFFFF) {
            if (i < ent.dataLength - 1) {
                printf("Encountered unexpected end-of-chain while erasing file. FAT may be corrupt\n");
            }
            break;
        }
        else if (link == 0) {
            printf("Encountered unallocated block while erasing file. FAT may be corrupt\n");
            break;
        }
        else {
            cur_sec = link;
        }
    }

    if (prev_fat != -1) {
        // flush sector
        write_sector(mc->fs, prev_fat, secbuf, MC_SECTOR_SIZE);
    }

    // finally, recompute checksums for dirtable & fat sectors
    header.dtChecksum = crc32_sectors(mc->fs, 0, dt_start, header.dtSectors);
    header.fatChecksum = crc32_sectors(mc->fs, 0, fat_start, header.fatSectors);

    write_sector(mc->fs, 0, &header, sizeof(HFAT_Header));

    // flush memcard
    stream_flush(mc->fs);
}

// get the information about a file given its dt index
void memcard_getinfo(Memcard* mc, int dirEnt, HFAT_DtEntry* info) {
    HFAT_Header header;
    read_sector(mc->fs, 0, &header, sizeof(HFAT_Header));

    int dt_start = 1;
    int fat_start = 1 + header.dtSectors;

    int dt_sec = 1 + (dirEnt / DT_ENTRIES);
    int dt_offs = dirEnt % DT_ENTRIES;

    int prev_fat = -1;

    uint8_t secbuf[MC_SECTOR_SIZE];
    uint16_t* fatbuf = (uint16_t*)secbuf;
    HFAT_DtEntry* dtbuf = (HFAT_DtEntry*)secbuf;

    // get entry from dt
    read_sector(mc->fs, dt_sec, secbuf, MC_SECTOR_SIZE);
    memcpy(info, &dtbuf[dt_offs], sizeof(HFAT_DtEntry));
}

// look for a file on disk
// returns index into DT if found, -1 otherwise
int memcard_find_file(Memcard* mc, const char* filename) {
    HFAT_Header header;
    read_sector(mc->fs, 0, &header, sizeof(HFAT_Header));

    int dir_start = 1;

    HFAT_DtEntry dir_sector[DT_ENTRIES];

    for (int i = 0; i < header.dtSectors; i++) {
        read_sector(mc->fs, dir_start + i, &dir_sector, MC_SECTOR_SIZE);

        for (int j = 0; j < DT_ENTRIES; j++) {
            if (strncmp(dir_sector[j].filename, filename, 20) == 0) {
                // found it
                return j + (i * DT_ENTRIES);
            }
        }
    }

    return -1;
}

// given a current sector number, look up the next sector from the FAT
// returns a sector number, or -1 if this is the last sector in the chain (or if the next link is corrupt)
int memcard_next_sector(Memcard* mc, int cur_sector) {
    HFAT_Header header;
    read_sector(mc->fs, 0, &header, sizeof(HFAT_Header));

    // convert sector number into offset within FAT
    int fat_start = 1 + header.dtSectors;
    int data_start = fat_start + header.fatSectors;
    int fat_idx = cur_sector - data_start;

    int fat_sec = fat_start + (fat_idx / FAT_ENTRIES);
    int fat_offs = fat_idx % FAT_ENTRIES;

    // look up value in FAT
    uint16_t fatbuf[FAT_ENTRIES];
    read_sector(mc->fs, fat_sec, fatbuf, MC_SECTOR_SIZE);

    uint16_t link = fatbuf[fat_offs];

    if (link == 0xFFFF) {
        return -1;
    }
    else if (link == 0) {
        // uh oh.
        printf("Encountered unallocated block while traversing file chain. FAT may be corrupt\n");
        return -1;
    }
    else {
        return link;
    }
}

Memcard* init_memcard(Stream* fs) {
    Memcard* mc = malloc(sizeof(Memcard));
    if (mc == NULL) return NULL;

    mc->valid = 0;
    mc->fs = fs;
    return mc;
}

void destroy_memcard(Memcard* mc) {
    stream_destroy(mc->fs);
    free(mc);
}

// returns 1 if the memory card has been validated, 0 otherwise
int memcard_valid(Memcard* mc) {
    return mc->valid;
}

int check_memcard(Memcard* mc) {
    HFAT_Header header;
    read_sector(mc->fs, 0, &header, sizeof(HFAT_Header));

    mc->valid = 0;

    if (strncmp(header.magic, HDR_MAGIC, 4)) {
        return MC_CHECK_NOFS;
    }

    // compute header CRC32
    uint32_t hdr_crc = crc32(0, &header, HDR_CRC_BYTES);

    if (hdr_crc != header.hdrChecksum) {
        return MC_CHECK_CORRUPT;
    }

    // compute crc for DT
    int dt_start = 1;
    uint32_t dt_crc = crc32_sectors(mc->fs, 0, dt_start, header.dtSectors);

    if (dt_crc != header.dtChecksum) {
        return MC_CHECK_CORRUPT;
    }

    // compute crc for FAT
    int fat_start = 1 + header.dtSectors;
    uint32_t fat_crc = crc32_sectors(mc->fs, 0, fat_start, header.fatSectors);

    if (fat_crc != header.fatChecksum) {
        return MC_CHECK_CORRUPT;
    }

    // filesystem ok!
    mc->valid = 1;
    return MC_CHECK_OK;
}

void format_memcard(Memcard* mc) {
    // format a 2MiB memory card
    char buff[MC_SECTOR_SIZE];
    memset(buff, 0, MC_SECTOR_SIZE);
    
    HFAT_Header header;

    // preserve existing OEM+serial
    read_sector(mc->fs, 0, &header, 12);

    // overwrite other fields
    memcpy(&header.magic, HDR_MAGIC, 4);
    header.fmtTime = (uint32_t)time(NULL);
    header.dtSectors = 16;
    header.fatSectors = 16;
    header.hdrChecksum = crc32(0, &header, HDR_CRC_BYTES);
    header.fatChecksum = 0;
    header.dtChecksum = 0;

    for (int i = 0; i < header.dtSectors; i++) {
        header.dtChecksum = crc32(header.dtChecksum, buff, MC_SECTOR_SIZE);
    }

    for (int i = 0; i < header.fatSectors; i++) {
        header.fatChecksum = crc32(header.fatChecksum, buff, MC_SECTOR_SIZE);
    }

    write_sector(mc->fs, 0, &header, sizeof(HFAT_Header));

    int dt_start = 1;
    int fat_start = dt_start + header.dtSectors;
    int data_start = fat_start + header.fatSectors;

    // write dt & fat sectors
    for (int i = 0; i < header.dtSectors; i++) {
        write_sector(mc->fs, dt_start + i, buff, MC_SECTOR_SIZE);
    }

    for (int i = 0; i < header.fatSectors; i++) {
        write_sector(mc->fs, fat_start + i, buff, MC_SECTOR_SIZE);
    }

    // write remaining data sectors
    for (int i = data_start; i < 4096; i++) {
        write_sector(mc->fs, i, buff, MC_SECTOR_SIZE);
    }

    mc->valid = 1;
}

void memcard_read_sector(Memcard* mc, int sector, void* buffer, size_t size) {
    read_sector(mc->fs, sector, buffer, size);
}

void memcard_write_sector(Memcard* mc, int sector, const void* buffer, size_t size) {
    write_sector(mc->fs, sector, buffer, size);
}

void memcard_flush(Memcard* mc) {
    stream_flush(mc->fs);
}