#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <assert.h>
#include <time.h>

#include <unicorn/unicorn.h>
#include <SDL.h>

#include "mem.h"
#include "elf.h"
#include "vm.h"
#include "stream.h"
#include "hfat.h"
#include "hdfs.h"
#include "util.h"
#include "gpu.h"

#define MAX_MEMCARD         2

#define UC_FETCH_MEM(name, ptr, idx, type) uc_mem_read(rt->uc, ptr + ((idx) * sizeof(type)), &name, sizeof(type))

struct HalberdRuntime {
    void* buf;
    int bufsize;
    Memcard* memcards[MAX_MEMCARD];
    Disc* disc;
    Gpu* gpu;
    void *mem;
    uc_engine *uc;
    int entryPoint;
    int heapStart;
    int heapLength;
};

typedef void (*syscall_handler_t)(HalberdRuntime *rt);

static void getstr(HalberdRuntime *rt, int ptr, char* buf, int len) {
    char c;
    do
    {
        uc_mem_read(rt->uc, ptr++, &c, 1);
        *buf++ = c;
    } while (c != 0 & len-- > 0);
}

static void setstr(HalberdRuntime *rt, int ptr, const char* buf, int len) {
    char c;
    do
    {
        c = *buf;
        uc_mem_write(rt->uc, ptr++, buf++, 1);
    } while (c != 0 & len-- > 0);
}

static void syscall_exit(HalberdRuntime *rt) {
    uc_emu_stop(rt->uc);
}

static void syscall_heapinfo(HalberdRuntime *rt) {
    // void heapinfo(int* heapstart, int* heaplen);
    int heapstart_ptr, heaplen_ptr;
    uc_reg_read(rt->uc, UC_ARM_REG_R0, &heapstart_ptr);
    uc_reg_read(rt->uc, UC_ARM_REG_R1, &heaplen_ptr);

    uc_mem_write(rt->uc, heapstart_ptr, &rt->heapStart, 4);
    uc_mem_write(rt->uc, heaplen_ptr, &rt->heapLength, 4);
}

static void syscall_clock(HalberdRuntime *rt) {
    // int clock();

    int us = (clock() * 1000) / CLOCKS_PER_SEC;
    uc_reg_write(rt->uc, UC_ARM_REG_R0, &us);
}

static void syscall_time(HalberdRuntime *rt) {
    // int time();

    // note: this returns local time, NOT utc!!

    time_t gmt, rawtime = time(NULL);

    // get offset in seconds
    struct tm* ptm;
    ptm = gmtime(&rawtime);
    ptm->tm_isdst = -1;
    gmt = mktime(ptm);

    int diff = difftime(rawtime, gmt);

    int ret = (int)rawtime + diff;
    uc_reg_write(rt->uc, UC_ARM_REG_R0, &ret);
}

static void syscall_gpu_getreg(HalberdRuntime* rt) {
    // int gpu_getreg(int chipsel, int addr);
    int chipsel, addr;
    uc_reg_read(rt->uc, UC_ARM_REG_R0, &chipsel);
    uc_reg_read(rt->uc, UC_ARM_REG_R1, &addr);

    uint32_t val = gpu_getreg(rt->gpu, chipsel, addr);
    uc_reg_write(rt->uc, UC_ARM_REG_R0, &val);
}

static void syscall_gpu_setreg(HalberdRuntime* rt) {
    // void gpu_setreg(int chipsel, int addr, int val);
    int chipsel, addr, val;
    uc_reg_read(rt->uc, UC_ARM_REG_R0, &chipsel);
    uc_reg_read(rt->uc, UC_ARM_REG_R1, &addr);
    uc_reg_read(rt->uc, UC_ARM_REG_R2, &val);

    gpu_setreg(rt->gpu, chipsel, addr, val);
}

static void syscall_gpu_getregf(HalberdRuntime* rt) {
    // float gpu_getregf(int chipsel, int addr);
    int chipsel, addr;
    uc_reg_read(rt->uc, UC_ARM_REG_R0, &chipsel);
    uc_reg_read(rt->uc, UC_ARM_REG_R1, &addr);

    float val = gpu_getregf(rt->gpu, chipsel, addr);
    uc_reg_write(rt->uc, UC_ARM_REG_S0, &val);
}

static void syscall_gpu_setregf(HalberdRuntime* rt) {
    // void gpu_setreg(int chipsel, int addr, float val);
    int chipsel, addr;
    float val;
    uc_reg_read(rt->uc, UC_ARM_REG_R0, &chipsel);
    uc_reg_read(rt->uc, UC_ARM_REG_R1, &addr);
    uc_reg_read(rt->uc, UC_ARM_REG_S0, &val);

    gpu_setregf(rt->gpu, chipsel, addr, val);
}

static void syscall_gpu_cmd(HalberdRuntime* rt) {
    // void gpu_cmd(GpuCmd_t* cmd, int size);
    int cmdptr, size;
    uc_reg_read(rt->uc, UC_ARM_REG_R0, &cmdptr);
    uc_reg_read(rt->uc, UC_ARM_REG_R1, &size);

    GpuCmd_t cmd;
    uc_mem_read(rt->uc, cmdptr, &cmd, size);

    gpu_handlecmd(rt->gpu, &cmd);
}

static void syscall_gpu_flushcmd(HalberdRuntime* rt) {
    // void gpu_flushcmd();
    gpu_flushcmd(rt->gpu);
}

static void syscall_gpu_readmem(HalberdRuntime* rt) {
    // void gpu_readmem(int chipsel, int addr, int len, void* buffer);
    int chipsel, addr, len, bufferPtr;
    uc_reg_read(rt->uc, UC_ARM_REG_R0, &chipsel);
    uc_reg_read(rt->uc, UC_ARM_REG_R1, &addr);
    uc_reg_read(rt->uc, UC_ARM_REG_R2, &len);
    uc_reg_read(rt->uc, UC_ARM_REG_R3, &bufferPtr);

    if (rt->bufsize < len) {
        rt->buf = realloc(rt->buf, len);
        rt->bufsize = len;
    }

    gpu_readmem(rt->gpu, chipsel, addr, len, rt->buf);
    uc_mem_write(rt->uc, bufferPtr, rt->buf, len);
}

static void syscall_gpu_writemem(HalberdRuntime* rt) {
    // void gpu_writemem(int chipsel, int addr, int len, const void* buffer);
    int chipsel, addr, len, bufferPtr;
    uc_reg_read(rt->uc, UC_ARM_REG_R0, &chipsel);
    uc_reg_read(rt->uc, UC_ARM_REG_R1, &addr);
    uc_reg_read(rt->uc, UC_ARM_REG_R2, &len);
    uc_reg_read(rt->uc, UC_ARM_REG_R3, &bufferPtr);

    if (rt->bufsize < len) {
        rt->buf = realloc(rt->buf, len);
        rt->bufsize = len;
    }

    uc_mem_read(rt->uc, bufferPtr, rt->buf, len);
    gpu_writemem(rt->gpu, chipsel, addr, len, rt->buf);
}

static void syscall_disc_present(HalberdRuntime* rt) {
    // int disc_present();
    int present = rt->disc != NULL;
    uc_reg_write(rt->uc, UC_ARM_REG_R0, &present);
}

static void syscall_disc_opentray(HalberdRuntime* rt) {
    // int disc_opentray();
    if (rt->disc != NULL) {
        destroy_disc(rt->disc);
        rt->disc = NULL;
    }

    int ret = 1;
    uc_reg_write(rt->uc, UC_ARM_REG_R0, &ret);
}

static void syscall_disc_closetray(HalberdRuntime* rt) {
    // int disc_closetray();
    int ret = 1;
    uc_reg_write(rt->uc, UC_ARM_REG_R0, &ret);
}

static void syscall_disc_read(HalberdRuntime* rt) {
    // void disc_read(int lba, int len, void* buffer);
    int lba, len, bufferPtr;
    uc_reg_read(rt->uc, UC_ARM_REG_R0, &lba);
    uc_reg_read(rt->uc, UC_ARM_REG_R1, &len);
    uc_reg_read(rt->uc, UC_ARM_REG_R2, &bufferPtr);

    if (rt->bufsize < len) {
        rt->buf = realloc(rt->buf, len);
        rt->bufsize = len;
    }

    void* dst = rt->buf;
    int rem = len;

    while (rem > 0) {
        int copy_bytes = len;
        if (copy_bytes > DVD_SECTOR_SIZE) copy_bytes = DVD_SECTOR_SIZE;
        disc_read_sector(rt->disc, lba, dst, copy_bytes);
        dst += copy_bytes;
        rem -= copy_bytes;
        lba++;
    }

    uc_mem_write(rt->uc, bufferPtr, rt->buf, len);
}

static void syscall_uart_putc(HalberdRuntime* rt) {
    // void uart_putc(char c);
    int c;
    uc_reg_read(rt->uc, UC_ARM_REG_R0, &c);

    putc(c, stdout);

    if (c == '\n') {
        fflush(stdout);
    }
}

static void syscall_uart_flush(HalberdRuntime* rt) {
    // void uart_flush();
    fflush(stdout);
}

static void syscall_memc_present(HalberdRuntime* rt) {
    // int memc_present(int mc);
    int mc;
    uc_reg_read(rt->uc, UC_ARM_REG_R0, &mc);

    int present = 0;
    if (mc >= 0 && mc < MAX_MEMCARD) {
        present = rt->memcards[mc] != NULL;
    }

    uc_reg_write(rt->uc, UC_ARM_REG_R0, &present);
}

static void syscall_memc_read(HalberdRuntime* rt) {
    // void memc_read(int mc, int lba, int length, void* buffer);
    int mc, lba, len, bufferPtr;
    uc_reg_read(rt->uc, UC_ARM_REG_R0, &mc);
    uc_reg_read(rt->uc, UC_ARM_REG_R1, &lba);
    uc_reg_read(rt->uc, UC_ARM_REG_R2, &len);
    uc_reg_read(rt->uc, UC_ARM_REG_R3, &bufferPtr);

    if (mc >= 0 && mc < MAX_MEMCARD) {
        if (rt->bufsize < len) {
            rt->buf = realloc(rt->buf, len);
            rt->bufsize = len;
        }

        void* dst = rt->buf;
        int rem = len;

        while (rem > 0) {
            int copy_bytes = len;
            if (copy_bytes > MC_SECTOR_SIZE) copy_bytes = MC_SECTOR_SIZE;
            memcard_read_sector(rt->memcards[mc], lba, dst, copy_bytes);
            dst += copy_bytes;
            rem -= copy_bytes;
            lba++;
        }

        uc_mem_write(rt->uc, bufferPtr, rt->buf, len);
    }
}

static void syscall_memc_write(HalberdRuntime* rt) {
    // void memc_write(int mc, int lba, int length, const void* buffer);
    int mc, lba, len, bufferPtr;
    uc_reg_read(rt->uc, UC_ARM_REG_R0, &mc);
    uc_reg_read(rt->uc, UC_ARM_REG_R1, &lba);
    uc_reg_read(rt->uc, UC_ARM_REG_R2, &len);
    uc_reg_read(rt->uc, UC_ARM_REG_R3, &bufferPtr);

    if (mc >= 0 && mc < MAX_MEMCARD) {
        if (rt->bufsize < len) {
            rt->buf = realloc(rt->buf, len);
            rt->bufsize = len;
        }

        uc_mem_read(rt->uc, bufferPtr, rt->buf, len);

        void* src = rt->buf;
        int rem = len;

        while (rem > 0) {
            int copy_bytes = len;
            if (copy_bytes > MC_SECTOR_SIZE) copy_bytes = MC_SECTOR_SIZE;
            memcard_write_sector(rt->memcards[mc], lba, src, copy_bytes);
            src += copy_bytes;
            rem -= copy_bytes;
            lba++;
        }
    }
}

static syscall_handler_t handlers[] = {
    syscall_exit,
    syscall_heapinfo,
    syscall_clock,
    syscall_time,

    syscall_gpu_getreg,
    syscall_gpu_setreg,
    syscall_gpu_getregf,
    syscall_gpu_setregf,
    syscall_gpu_cmd,
    syscall_gpu_flushcmd,
    syscall_gpu_readmem,
    syscall_gpu_writemem,

    syscall_disc_present,
    syscall_disc_opentray,
    syscall_disc_closetray,
    syscall_disc_read,

    syscall_uart_putc,
    syscall_uart_flush,

    syscall_memc_present,
    syscall_memc_read,
    syscall_memc_write,
};

#define HANDLER_COUNT (sizeof(handlers) / sizeof(syscall_handler_t))

static void hook_intr(uc_engine* uc, uint32_t intno, void* userdata) {
    if (intno == 2) {
        int pc;
        uc_reg_read(uc, UC_ARM_REG_PC, &pc);

        // back up pc by four bytes to get addr of svc instruction
        pc -= 4;

        // grab 24-bit immediate from the instruction
        uint32_t svc_insr;
        uc_mem_read(uc, pc, &svc_insr, 4);

        int svc_num = svc_insr & 0xFFFFFF;

        // trigger svc handler
        if (svc_num >= 0 && svc_num < HANDLER_COUNT) {
            if (handlers[svc_num] != NULL) {
                handlers[svc_num]((HalberdRuntime *)userdata);
            }
        }
    }
}

static void hook_mem_invalid(uc_engine* uc, uc_mem_type type, uint64_t addr,
    int size, int64_t value, void* userdata) {
    switch (type) {
    default:
        printf("Invalid memory access at 0x%" PRIx64 "\n", addr);
        return;
    case UC_MEM_READ_UNMAPPED:
        printf("Read from unmapped memory at 0x%" PRIx64
               ", data size = %u\n",
               addr, size);
        return;
    case UC_MEM_WRITE_UNMAPPED:
        printf("Write to unmapped memory at 0x%" PRIx64
               ", data size = %u, data value = 0x%" PRIx64 "\n",
               addr, size, value);
        return;
    case UC_MEM_FETCH_PROT:
        printf("Fetch from non-executable memory at 0x%" PRIx64 "\n",
               addr);
        return;
    case UC_MEM_WRITE_PROT:
        printf("Write to protected memory at 0x%" PRIx64
               ", data size = %u, data value = 0x%" PRIx64 "\n",
               addr, size, value);
        return;
    case UC_MEM_READ_PROT:
        printf("Read from protected memory at 0x%" PRIx64
               ", data size = %u\n",
               addr, size);
        return;
    }
}

HalberdRuntime *halberd_create() {
    HalberdRuntime *rt = malloc(sizeof(HalberdRuntime));
    if (rt == NULL) {
        printf("Failed allocating runtime\n");
        return NULL;
    }

    rt->buf = NULL;
    rt->bufsize = 0;
    rt->disc = NULL;

    // clear memcard slots
    memset(rt->memcards, 0, sizeof(Memcard*) * MAX_MEMCARD);

    // allocate bigass slab of memory upfront
    rt->mem = malloc(MEM_SIZE);
    if (rt->mem == NULL) {
        printf("Failed allocating memory\n");
        free(rt);
        return NULL;
    }

    // init GPU
    rt->gpu = gpu_create();
    if (rt->gpu == NULL) {
        printf("Failed creating GPU\n");
        free(rt);
        return NULL;
    }

    uc_err err;
    uc_hook trace1, trace2;

    // Initialize emulator in ARM mode
    err = uc_open(UC_ARCH_ARM, UC_MODE_ARM, &rt->uc);
    if (err)
    {
        printf("Failed on uc_open() with error returned: %u (%s)\n", err,
               uc_strerror(err));
        free(rt->mem);
        gpu_destroy(rt->gpu);
        free(rt);
        return NULL;
    }

    uc_ctl(rt->uc, UC_CTL_CPU_MODEL, UC_CPU_ARM_926);

    // enable access to VFP coprocessor
    uint32_t fpexc;
    uc_reg_read(rt->uc, UC_ARM_REG_FPEXC, &fpexc);
    fpexc |= (1<<30);
    uc_reg_write(rt->uc, UC_ARM_REG_FPEXC, &fpexc);

    // set hook for svc calls
    uc_hook_add(rt->uc, &trace1, UC_HOOK_INTR, hook_intr, rt, 1, 0);
    uc_hook_add(rt->uc, &trace2, UC_HOOK_MEM_INVALID, hook_mem_invalid, rt, 1, 0);

    printf("Runtime created\n");
    return rt;
}

void halberd_destroy(HalberdRuntime *rt) {
    // destroy GPU
    gpu_destroy(rt->gpu);

    // if any memcards are open, close them
    for (int i = 0; i < MAX_MEMCARD; i++) {
        if (rt->memcards[i] != NULL) {
            destroy_memcard(rt->memcards[i]);
        }
    }

    // if disc is open, close it
    if (rt->disc != NULL) {
        destroy_disc(rt->disc);
    }

    uc_close(rt->uc);
    free(rt->mem);
    free(rt);
}

int halberd_loadelf(HalberdRuntime *rt, void *elf) {
    return elf_load(elf, rt->mem, MEM_SIZE, STACK_ADDR, STACK_SIZE, rt->uc, &rt->entryPoint, &rt->heapStart, &rt->heapLength);
}

int halberd_loadElfFromDisc(HalberdRuntime *rt, const char* path) {
    if (rt->disc == NULL) {
        return 0;
    }

    Stream* elf_file = disc_open(rt->disc, path, "rb");

    if (elf_file == NULL) {
        return 0;
    }

    void* elf_data = loadAllStream(elf_file);
    stream_destroy(elf_file);

    if (elf_data == NULL) {
        return 0;
    }

    int ret = halberd_loadelf(rt, elf_data);
    free(elf_data);

    return ret;
}

int halberd_start(HalberdRuntime *rt) {
    return uc_emu_start(rt->uc, rt->entryPoint, 0, 0, 0);
}

void halberd_loadMemcard(HalberdRuntime *rt, const char* path, int slot) {
    if (slot < 0 || slot >= MAX_MEMCARD) {
        printf("Invalid memory card slot\n");
        return;
    }

    Stream* mc_file = stream_fopen(path, "rb+");

    if (mc_file == NULL) {
        printf("Creating new memory card file: %s (in slot %d)\n", path, slot);

        // file does not exist, create a new one
        mc_file = stream_fopen(path, "w+");

        // init with some fictional oem+serial data... :)
        stream_write("CCS LTD.", 8, 1, mc_file);
        uint32_t serial = gen_serial();
        stream_write(&serial, 4, 1, mc_file);
    }
    else {
        printf("Opened memory card file: %s (in slot %d)\n", path, slot);
    }

    Memcard* mc = init_memcard(mc_file);

    if (rt->memcards[slot] != NULL) {
        destroy_memcard(rt->memcards[slot]);
    }

    rt->memcards[slot] = mc;
}

void halberd_loadDisc(HalberdRuntime *rt, const char* path) {
    Stream* disc_file = stream_fopen(path, "rb");

    if (disc_file == NULL) {
        printf("Could not load disc image: %s\n", path);
        return;
    }

    Disc* disc = init_disc(disc_file);

    if (disc == NULL) {
        printf("Failed loading disc image\n");
        return;
    }

    printf("Opened disc image: %s\n", path);

    if (rt->disc != NULL) {
        destroy_disc(rt->disc);
    }

    rt->disc = disc;
}

Disc* halberd_getDisc(HalberdRuntime *rt) {
    return rt->disc;
}