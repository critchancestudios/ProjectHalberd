#version 100
#extension GL_EXT_shader_texture_lod : require

precision mediump float;

#define COMPARE_NEVER           0
#define COMPARE_ALWAYS          1
#define COMPARE_LESS            2
#define COMPARE_LEQUAL          3
#define COMPARE_GREATER         4
#define COMPARE_GEQUAL          5
#define COMPARE_EQUAL           6
#define COMPARE_NOTEQUAL        7

#define COMBINE_FUNC_ZERO                                       0
#define COMBINE_FUNC_LOCAL                                      1
#define COMBINE_FUNC_LOCAL_ALPHA                                2
#define COMBINE_FUNC_SCALE_OTHER                                3
#define COMBINE_FUNC_BLEND_OTHER                                COMBINE_FUNC_SCALE_OTHER
#define COMBINE_FUNC_SCALE_OTHER_ADD_LOCAL                      4
#define COMBINE_FUNC_SCALE_OTHER_ADD_LOCAL_ALPHA                5
#define COMBINE_FUNC_SCALE_OTHER_MINUS_LOCAL                    6
#define COMBINE_FUNC_SCALE_OTHER_MINUS_LOCAL_ADD_LOCAL          7
#define COMBINE_FUNC_BLEND                                      COMBINE_FUNC_SCALE_OTHER_MINUS_LOCAL_ADD_LOCAL
#define COMBINE_FUNC_SCALE_OTHER_MINUS_LOCAL_ADD_LOCAL_ALPHA    8
#define COMBINE_FUNC_SCALE_MINUS_LOCAL_ADD_LOCAL                9
#define COMBINE_FUNC_BLEND_LOCAL                                10
#define COMBINE_FUNC_SCALE_MINUS_LOCAL_ADD_LOCAL_ALPHA          11

#define COMBINE_FACTOR_ZERO                                     0
#define COMBINE_FACTOR_LOCAL                                    1
#define COMBINE_FACTOR_OTHER_ALPHA                              2
#define COMBINE_FACTOR_LOCAL_ALPHA                              3
#define COMBINE_FACTOR_ONE                                      4
#define COMBINE_FACTOR_ONE_MINUS_LOCAL                          5
#define COMBINE_FACTOR_ONE_MINUS_OTHER_ALPHA                    6
#define COMBINE_FACTOR_ONE_MINUS_LOCAL_ALPHA                    7

// TMU combine settings
uniform bool        tmu0_enable;
uniform int         tmu0_rgbFunc;
uniform int         tmu0_rgbFac;
uniform int         tmu0_aFunc;
uniform int         tmu0_aFac;
uniform bool        tmu0_rgbInvert;
uniform bool        tmu0_aInvert;

uniform bool        tmu1_enable;
uniform int         tmu1_rgbFunc;
uniform int         tmu1_rgbFac;
uniform int         tmu1_aFunc;
uniform int         tmu1_aFac;
uniform bool        tmu1_rgbInvert;
uniform bool        tmu1_aInvert;

// color combine settings
uniform int         color_rgbFunc;
uniform int         color_rgbFac;
uniform int         color_aFunc;
uniform int         color_aFac;
uniform bool        color_rgbInvert;
uniform bool        color_aInvert;

// alpha compare
uniform int         alpha_compare;
uniform float       alpha_ref;

// misc
uniform float       gamma;

// textures
uniform sampler2D   tmu0_tex;
uniform vec4        tmu0_texres;
uniform bool        tmu0_filter;
uniform sampler2D   tmu1_tex;
uniform vec4        tmu1_texres;
uniform bool        tmu1_filter;

// fog
uniform sampler2D fog_lut;
uniform bool fog_enable;
uniform vec4 fog_color;

// vertex attributes
varying float       oow;
varying vec4        vcol;
varying vec2        vst0;
varying vec2        vst1;

vec3 getscale(vec4 other, vec4 local, int fac) {
    vec3 scale = vec3(0.0, 0.0, 0.0);
    if (fac == COMBINE_FACTOR_LOCAL) {
        scale = local.rgb;
    }
    else if (fac == COMBINE_FACTOR_OTHER_ALPHA) {
        scale = other.aaa;
    }
    else if (fac == COMBINE_FACTOR_LOCAL_ALPHA) {
        scale = local.aaa;
    }
    else if (fac == COMBINE_FACTOR_ONE) {
        scale = vec3(1.0, 1.0, 1.0);
    }
    else if (fac == COMBINE_FACTOR_ONE_MINUS_LOCAL) {
        scale = 1.0 - local.rgb;
    }
    else if (fac == COMBINE_FACTOR_ONE_MINUS_OTHER_ALPHA) {
        scale = 1.0 - other.aaa;
    }
    else if (fac == COMBINE_FACTOR_ONE_MINUS_LOCAL_ALPHA) {
        scale = 1.0 - local.aaa;
    }
    return scale;
}

vec4 combine(vec4 other, vec4 local, int rgbFunc, int rgbFac, int aFunc, int aFac,
    bool rgbInvert, bool aInvert)
{
    vec3 scale = getscale(other, local, rgbFac);
    float ascale = getscale(other.aaaa, local.aaaa, aFac).r;
    vec4 combine = vec4(0.0, 0.0, 0.0, 0.0);
    
    if (rgbFunc == COMBINE_FUNC_LOCAL) {
        combine.rgb = local.rgb;
    }
    else if (rgbFunc == COMBINE_FUNC_LOCAL_ALPHA) {
        combine.rgb = local.aaa;
    }
    else if (rgbFunc == COMBINE_FUNC_SCALE_OTHER) {
        combine.rgb = other.rgb * scale;
    }
    else if (rgbFunc == COMBINE_FUNC_SCALE_OTHER_ADD_LOCAL) {
        combine.rgb = (other.rgb * scale) + local.rgb;
    }
    else if (rgbFunc == COMBINE_FUNC_SCALE_OTHER_ADD_LOCAL_ALPHA) {
        combine.rgb = (other.rgb * scale) + local.aaa;
    }
    else if (rgbFunc == COMBINE_FUNC_SCALE_OTHER_MINUS_LOCAL) {
        combine.rgb = scale * (other.rgb - local.rgb);
    }
    else if (rgbFunc == COMBINE_FUNC_SCALE_OTHER_MINUS_LOCAL_ADD_LOCAL) {
        combine.rgb = scale * (other.rgb - local.rgb) + local.rgb;
    }
    else if (rgbFunc == COMBINE_FUNC_SCALE_OTHER_MINUS_LOCAL_ADD_LOCAL_ALPHA) {
        combine.rgb = scale * (other.rgb - local.rgb) + local.aaa;
    }
    else if (rgbFunc == COMBINE_FUNC_SCALE_MINUS_LOCAL_ADD_LOCAL) {
        combine.rgb = (scale * -local.rgb) + local.rgb;
    }
    else if (rgbFunc == COMBINE_FUNC_SCALE_MINUS_LOCAL_ADD_LOCAL_ALPHA) {
        combine.rgb = (scale * -local.rgb) + local.aaa;
    }
    
    if (aFunc == COMBINE_FUNC_LOCAL) {
        combine.a = local.a;
    }
    else if (aFunc == COMBINE_FUNC_LOCAL_ALPHA) {
        combine.a = local.a;
    }
    else if (aFunc == COMBINE_FUNC_SCALE_OTHER) {
        combine.a = other.a * ascale;
    }
    else if (aFunc == COMBINE_FUNC_SCALE_OTHER_ADD_LOCAL) {
        combine.a = (other.a * ascale) + local.a;
    }
    else if (aFunc == COMBINE_FUNC_SCALE_OTHER_ADD_LOCAL_ALPHA) {
        combine.a = (other.a * ascale) + local.a;
    }
    else if (aFunc == COMBINE_FUNC_SCALE_OTHER_MINUS_LOCAL) {
        combine.a = ascale * (other.a - local.a);
    }
    else if (aFunc == COMBINE_FUNC_SCALE_OTHER_MINUS_LOCAL_ADD_LOCAL) {
        combine.a = ascale * (other.a - local.a) + local.a;
    }
    else if (aFunc == COMBINE_FUNC_SCALE_OTHER_MINUS_LOCAL_ADD_LOCAL_ALPHA) {
        combine.a = ascale * (other.a - local.a) + local.a;
    }
    else if (aFunc == COMBINE_FUNC_SCALE_MINUS_LOCAL_ADD_LOCAL) {
        combine.a = (ascale * -local.a) + local.a;
    }
    else if (aFunc == COMBINE_FUNC_SCALE_MINUS_LOCAL_ADD_LOCAL_ALPHA) {
        combine.a = (ascale * -local.a) + local.a;
    }
    
    if (rgbInvert) {
        combine.rgb = 1.0 - combine.rgb;
    }
    
    if (aInvert) {
        combine.a = 1.0 - combine.a;
    }
    
    return clamp(combine, 0.0, 1.0);
}

bool compare(float a, float b, int func) {
    if (func == COMPARE_NEVER) {
        return false;
    }
    else if (func == COMPARE_ALWAYS) {
        return true;
    }
    else if (func == COMPARE_LESS) {
        return a < b;
    }
    else if (func == COMPARE_LEQUAL) {
        return a <= b;
    }
    else if (func == COMPARE_GREATER) {
        return a > b;
    }
    else if (func == COMPARE_GEQUAL) {
        return a >= b;
    }
    else if (func == COMPARE_EQUAL) {
        return a == b;
    }
    else if (func == COMPARE_NOTEQUAL) {
        return a != b;
    }

    return false;
}

// 3DFX COLOR COMBINE NOTES
// the color combine algorithm takes into account a "local" color and an "upstream/other" color
// TMU0, TMU1, and FB all have their own color combine settings.
// Each of these units is connected to another unit as its "upstream" color source:
// FB's upstream source is TMU0, and in turn TMU0's upstream source is TMU1.
// TMU1's upstream source, on the other hand, is nothing. You can give it color combine settings, but I think "other" is either zero or undefined?

void main() {
    float w = 1.0 / oow;
    
    // multiply by w to undo w division performed by vertex shader
    vec2 st0 = vst0 * w;
    vec2 st1 = vst1 * w;

    // TODO: does tmu enable settings turn off the combine operation too?
    vec4 tmu0 = tmu0_enable ? texture2D(tmu0_tex, st0).bgra : vec4(0.0, 0.0, 0.0, 0.0);
    vec4 tmu1 = tmu1_enable ? texture2D(tmu1_tex, st1).bgra : vec4(0.0, 0.0, 0.0, 0.0);
    vec4 color = vcol * w;

    vec4 outcol = combine(vec4(0.0, 0.0, 0.0, 0.0), tmu1, tmu1_rgbFunc, tmu1_rgbFac, tmu1_aFunc, tmu1_aFac, tmu1_rgbInvert, tmu1_aInvert);
    outcol      = combine(outcol, tmu0, tmu0_rgbFunc, tmu0_rgbFac, tmu0_aFunc, tmu0_aFac, tmu0_rgbInvert, tmu0_aInvert);
    outcol      = combine(outcol, color, color_rgbFunc, color_rgbFac, color_aFunc, color_aFac, color_rgbInvert, color_aInvert);

    if (!compare(outcol.a, alpha_ref, alpha_compare)) {
        discard;
    }

    if (fog_enable) {
        float fogz = log2(w) * 0.0625; // basically un-doing the 2^(i/4) calculation of fog table, then divide by 64 to normalize
        float fog_density = texture2D(fog_lut, vec2(fogz, 0.0)).a;

        outcol.rgb = mix(outcol.rgb, fog_color.rgb, fog_density);
    }

    gl_FragColor = vec4(pow(outcol.rgb, vec3(1.0 / gamma)), outcol.a);
}