#version 100

precision mediump float;

attribute vec4 apos;
attribute vec4 acol;
attribute vec2 atex0;
attribute vec2 atex1;

varying float oow;
varying vec4 vcol;
varying vec2 vst0;
varying vec2 vst1;

void main() {
    // perform our own 1/w & pass w to fragment shader to undo 1/w
    // note: tex0 & tex1 coords are already 1/w on the CPU

    // fun implementation note: z/w will range from 0 to 1, but GLES is hardcoded to expect -1 to 1
    // so we need to remap it.

    vec4 proj = vec4(apos.xyz * apos.w, 1.0);
    proj.z = proj.z * 2.0 - 1.0;

    oow = apos.w;
    gl_Position = proj;
    vcol = acol * apos.w;
    vst0 = atex0;
    vst1 = atex1;
}