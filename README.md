# ProjectHalberd

A new kind of fantasy console, modelled after a fictional historically-accurate console of the early 2000s and designed to run on a Raspberry Pi Zero 2 or equivalent hardware. A successor to the [Dreambox](https://dreambox3d.dev/) project.

## Fantasy Hardware Specs

Project Halberd is designed to emulate a "canonical" fictional hardware design, period accurate to hardware that would've been available (or at least announced and in development) from 1998 to 2000. Each component should be researched
to ensure Halberd strictly limits itself to the hardware's actual feature set.

The fictional specs are as follows:

- CPU
  - ARM9E-based (ARMv5TE+VFP instruction set, 300MHz)
  - Codename: "Muse"
- GPU
  - Voodoo-inspired graphics accelerator (Glide subset, dual TMU, 4MB FB memory, 8MB memory per TMU - total 20MB vram)
  - Codename: "Vision"
- Audio
  - Custom audio inspired by PS2 (64-voice pcm+adpcm synthesizer, EAX reverb + configurable biquad filter, OpenAL-like wrapper API)
  - Codename: "Harmony"
- Storage
  - DVD drive (2X speed, single layer, 4.7GB maximum, custom "HDFS" filesystem)
  - A pair of removeable 2MiB "memory cards" (custom FAT-like filesystem, technically supports up to 32MiB)
- Misc
  - 32MB RAM
  
## Emulation

Of course all of this will instead be emulated. The goal is twofold: the emulator should be cross-platform, running on PC Windows + Linux, as well as ARM Linux (supporting RPi or equivalent targets). I would also like to provide a minimal "just enough Linux" installation with Halberd pre-installed, allowing Halberd on an RPi to function as if it were a real game console.

- Executables will simply be standard executable ELF files located in the root directory of the disc filesystem. Unicorn Emulator will provide the emulation layer, utilizing runtime JIT compilation to ensure high speed. APIs will be provided as system calls, via the "svc" instruction.
- A custom low-level GPU API is provided which allows registers to be read & written, texture memory uploaded, as well as various rendering commands to be submitted, with a feature set inspired by Voodoo hardware (things like dual TMUs, color combine settings, etc will be emulated in GLSL). A small Glide subset layer will be provided which wraps around this low level API.
- A simple audio API will be provided which allows sample memory to be uploaded, and registers to be read & written to schedule voice playback, change reverb settings, etc. An OpenAL-like layer will wrap this low-level API.
- Game ISOs and memory card files are presented as simple block devices, where individual sectors can be read & written. A POSIX-inspired layer will wrap this to provide support for high level filesystem access.

Things I have not yet figured out:

- How to handle things like selecting game ISOs to run, pairing BT accessories, etc (without a desktop, this will likely have to be handled by the custom shell - perhaps via a "meta" menu that can be opened?)
- Should socket API be IPv4 only? Maybe "IPv6" behind the scenes, but generate fake IPv4 "wrapper", almost like a NAT, for authenticity? Or is it OK to just be anachronistic here?
- Whether the Voodoo's 2D acceleration should be emulated. I think originally you'd use DirectDraw or whatever to interact with it, but that's MS only. Maybe a custom API (like SDL_Render) for 2D drawing, emulated in OpenGL? According to the programming guide, it supports clipped screen-to-screen blit, host-to-screen blit, rect fill, line, polyline, and polyfill. Allowed src formats are 1bpp monochrome, 8bpp palette, 16bpp RGB, 24bpp RGB, 32bpp RGB, YUYV 4:2:2, and UYVY 4:2:2. Allowed dst formats are 8bpp, 16bpp, 24bpp, and 32bpp. Do all of these features need to be emulated, or is just a useful subset OK? (screen-to-screen might end up being a huge PITA for example, maybe OK to skip?)

## Dependencies/Prerequisites

- SDL2 (`sudo apt install libsdl2-dev`)
- [Unicorn Engine](https://github.com/unicorn-engine/unicorn) (build from source, then `sudo make install`)

## Directories

- emulator
  - The main emulator application
- mkhdfs
  - A Rust CLI utility for converting folders into HDFS disc images
- test_cpu_program
  - Used as a sandbox for creating ELF executables for testing
- research
  - A collection of research docs (reference manuals, programming guides, etc) related to the fictional hardware specs, for comparing Halberd's feature set against
