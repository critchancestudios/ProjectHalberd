use std::{ffi::CString, fs::{read_dir, File}, io::Write, path::{Path, PathBuf}, vec};
use byteorder::{LittleEndian, WriteBytesExt};

use clap::Parser;

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    #[arg(short, long)]
    publisher: String,
    #[arg(short, long)]
    game_name: String,
    #[arg(short, long)]
    output: PathBuf,
    #[arg(index = 1)]
    input: PathBuf,
}

struct DtableEntry {
    filename: [u8; 52],
    flags: u32,
    data_start: u32,
    data_length: u32
}

const DTABLE_ENTRY_SIZE: usize = 64;
const DVD_SECTOR_SIZE: usize = 2048;

const HDR_SIZE: usize = 76;
const HDR_PAD: usize = DVD_SECTOR_SIZE - HDR_SIZE;

const CRC_POLY: u32 = 0x82f63b78;

fn crc32(crc: u32, buf: &[u8]) -> u32 {
    let mut crc = !crc;

    for b in buf {
        crc = crc ^ (*b as u32);
        for _ in 0..8 {
            crc = if (crc & 1) != 0 {
                (crc >> 1) ^ CRC_POLY
            }
            else {
                crc >> 1
            };
        }
    }

    !crc
}

fn str_to_byte_array<const LEN: usize>(str: String) -> Result<[u8; LEN],()> {
    let cstr = CString::new(str).unwrap();
    let cstr_bytes = cstr.as_bytes();

    let mut outp: [u8; LEN] = [0; LEN];

    if cstr_bytes.len() <= LEN {
        outp[0..cstr_bytes.len()].copy_from_slice(cstr_bytes);
        Ok(outp)
    }
    else {
        Err(())
    }
}

fn write_file<P: AsRef<Path> + Clone, W: Write>(path: P, out: &mut W) -> (usize, usize) {
    // copy input file bytes to output
    let file_bytes = std::fs::read(path).unwrap();
    out.write_all(&file_bytes).unwrap();

    // pad to next sector boundary
    let sectors = usize::div_ceil(file_bytes.len(), DVD_SECTOR_SIZE);
    let total_bytes = sectors * DVD_SECTOR_SIZE;
    let padding = total_bytes - file_bytes.len();

    let padbuf: Vec<u8> = vec![0; padding];
    out.write_all(&padbuf).unwrap();

    // return sectors written
    return (sectors, sectors);
}

fn write_dir<P: AsRef<Path> + Clone, W: Write>(path: P, sector_offset: usize, out: &mut W) -> (usize, usize) {
    // first count the number of entries in the directory so we know how big the directory table needs to be,
    // then iterate again to actually start writing the files

    let path2 = path.clone();

    let count = match read_dir(path) {
        Ok(input) => input,
        Err(e) => {
            panic!("Error opening input directory: {:?}", e)
        }
    }.count();

    let dtable_bytes = count * DTABLE_ENTRY_SIZE;
    let dtable_sectors = usize::div_ceil(dtable_bytes, DVD_SECTOR_SIZE);

    let mut cur_offs = sector_offset + dtable_sectors;

    let mut dt: Vec<DtableEntry> = vec![];
    let mut content_buf: Vec<u8> = vec![];

    let r = match read_dir(path2) {
        Ok(input) => input,
        Err(e) => {
            panic!("Error opening input directory: {:?}", e)
        }
    };

    for entry in r {
        match entry {
            Ok(entry) => {
                let md = entry.metadata().unwrap();
                println!("Processing entry: {:?}", entry.file_name());

                let filename: [u8; 52] = match str_to_byte_array(entry.file_name().into_string().unwrap()) {
                    Ok(str) => str,
                    Err(()) => {
                        panic!("Filename {:?} is too long", entry.file_name());
                    }
                };

                let data_start = cur_offs;
                let flags: u32 =
                    if md.is_dir()          { 1 }
                    else if md.is_file()    { 0 }
                    else                    { panic!("Unsupported entity in directory: {:?} (not a file or folder)", entry.file_name()) };

                let data_length =
                    if flags == 1           { write_dir(entry.path(), cur_offs, &mut content_buf) }
                    else                    { write_file(entry.path(), &mut content_buf) };

                cur_offs += data_length.0;

                dt.push(DtableEntry {
                    filename: filename,
                    flags: flags,
                    data_start: data_start as u32,
                    data_length: data_length.1 as u32
                });
            }
            Err(e) => {
                println!("Open error: {:?}", e);
            }
        }
    }

    // write dirtable
    for ent in dt {
        out.write_all(&ent.filename).unwrap();
        out.write_u32::<LittleEndian>(ent.flags).unwrap();
        out.write_u32::<LittleEndian>(ent.data_start).unwrap();
        out.write_u32::<LittleEndian>(ent.data_length).unwrap();
    }

    // pad dirtable to whole sector size
    if dtable_bytes % DVD_SECTOR_SIZE != 0 {
        let dirtable_rem = DVD_SECTOR_SIZE - (dtable_bytes % DVD_SECTOR_SIZE);
        let b: Vec<u8> = vec![0; dirtable_rem];
        out.write_all(&b).unwrap();
    }

    // write dir contents
    out.write_all(&content_buf).unwrap();

    // return number of written sectors
    return (cur_offs - sector_offset, dtable_sectors);
}

fn main() {
    let args = Args::parse();

    // open output file
    let mut outf = match File::create(args.output) {
        Ok(f) => f,
        Err(e) => panic!("Failed opening output file: {:?}", e)
    };

    let mut buf: Vec<u8> = vec![];
    let (_, root_sectors) = write_dir(args.input, 1, &mut buf);

    // write file header
    let mut hdr_buf: Vec<u8> = vec![];
    hdr_buf.write_all(b"HDFS").unwrap();

    let publisher: [u8; 32] = match str_to_byte_array(args.publisher.clone()) {
        Ok(str) => str,
        Err(()) => {
            panic!("Publisher field is too long (max 32 bytes): {}", args.publisher);
        }
    };

    let game_name: [u8; 32] = match str_to_byte_array(args.game_name.clone()) {
        Ok(str) => str,
        Err(()) => {
            panic!("Game name field is too long (max 32 bytes): {}", args.game_name);
        }
    };

    hdr_buf.write_all(&publisher).unwrap();
    hdr_buf.write_all(&game_name).unwrap();
    hdr_buf.write_u32::<LittleEndian>(root_sectors as u32).unwrap();

    // calculate header checksum
    let hdr_checksum = crc32(0, &hdr_buf);

    hdr_buf.write_u32::<LittleEndian>(hdr_checksum).unwrap();

    // pad to sector boundary
    let pad: [u8; HDR_PAD] = [0; HDR_PAD];
    hdr_buf.write_all(&pad).unwrap();

    // sick, now just write everything out to file and we're done!
    outf.write_all(&hdr_buf).unwrap();
    outf.write_all(&buf).unwrap();

    println!("Disk image written ({} bytes)", hdr_buf.len() + buf.len());
}
